<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParacaidistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paracaidistas', function (Blueprint $table) {
            $table->id();

            $table->integer('licencia');
            $table->enum('categoria',['AI', 'A','B','C'])->default('A');
            $table->integer('cant_saltos');
            $table->string('tamaño_velamen');
            //relaciones
            $table->unsignedBigInteger('persona_id');
            $table->foreign('persona_id')->references('id')->on('personas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paracaidistas');
    }
}
