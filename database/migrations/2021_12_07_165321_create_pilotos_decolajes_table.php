<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePilotosDecolajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pilotos_decolajes', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('piloto_id');
            $table->foreign('piloto_id')->references('id')->on('pilotos');

            $table->unsignedBigInteger('decolaje_id');
            $table->foreign('decolaje_id')->references('id')->on('decolajes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pilotos_decolajes');
    }
}
