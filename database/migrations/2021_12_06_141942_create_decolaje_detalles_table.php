<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDecolajeDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decolaje_detalles', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('decolaje_id');
            $table->foreign('decolaje_id')->references('id')->on('decolajes');

            $table->unsignedBigInteger('paracaidista_id');
            $table->foreign('paracaidista_id')->references('id')->on('paracaidistas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decolaje_detalles');
    }
}
