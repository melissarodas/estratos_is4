<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDecolajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decolajes', function (Blueprint $table) {
            $table->id();

            $table->date('fecha_despegue');
            $table->date('fecha_aterrizaje');

            $table->enum('estado', ['PENDIENTE','APROBADO','DESAPROBADO'])->default('PENDIENTE');

            // $table->unsignedBigInteger('piloto_id');
            // $table->foreign('piloto_id')->references('id')->on('pilotos');

            $table->unsignedBigInteger('evento_id');
            $table->foreign('evento_id')->references('id')->on('eventos');

            $table->unsignedBigInteger('avioneta_id');
            $table->foreign('avioneta_id')->references('id')->on('avionetas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decolajes');
    }
}
