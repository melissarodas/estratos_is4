<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCondicionesClimaticasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condiciones_climaticas', function (Blueprint $table) {
            $table->id();
            $table->integer('altitud');
            $table->integer('temperatura');
            $table->integer('direccion_viento')->nullable();
            $table->integer('velocidad_viento')->nullable();

            $table->unsignedBigInteger('decolaje_id');
            $table->foreign('decolaje_id')->references('id')->on('decolajes');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condiciones_climaticas');
    }
}
