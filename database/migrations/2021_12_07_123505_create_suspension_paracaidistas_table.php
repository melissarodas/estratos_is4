<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuspensionParacaidistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suspension_paracaidistas', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('paracaidista_id');
            $table->foreign('paracaidista_id')->references('id')->on('paracaidistas');
            $table->string('motivo');
            $table->date('fecha');
            $table->integer('duracion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suspension_paracaidistas');
    }
}
