<?php

use App\User;
use Illuminate\Database\Seeder;
Use Caffeinated\Shinobi\Models\Role;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        
        $user = User::create([
            'name'      => 'Administrador',
            'email'     => 'admin@gmail.com',
            'password'  =>  bcrypt('123456789'),
        ]);

        $user->roles()->sync(1);
    }
}
