<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //https://spatie.be/docs/laravel-permission/v4/basic-usage/basic-usage
        $admin          = Role::create(['name' => 'Admin']);
        $supervisor     = Role::create(['name' => 'Supervisor']);

        Permission::create(['name' => 'home', 'description' => 'Ver el dashboard'])->syncRoles([$admin,$supervisor]);

       //PERMISOS PARA PERMISOS
       Permission::create(['name' => 'permisos',
            'description' => 'Acceso el modulo de permisos'])->syncRoles([$admin]);
        Permission::create(['name' => 'permisos.index',
            'description' => 'Ver el listado de permisos'])->syncRoles([$admin]);
        Permission::create(['name' => 'permisos.create',
            'description' => 'Crear permisos'])->syncRoles([$admin]);
        Permission::create(['name' => 'permisos.edit',
            'description' => 'Editar permisos'])->syncRoles([$admin]);
        Permission::create(['name' => 'permisos.destroy',
            'description' => 'Eliminar permisos'])->syncRoles([$admin]);

     //PERMISOS PARA ROLES
     Permission::create(['name' => 'roles',
        'description' => 'Acceso el modulo de roles'])->syncRoles([$admin]);
     Permission::create(['name' => 'roles.index',
        'description' => 'Ver el listado de roles'])->syncRoles([$admin]);
    Permission::create(['name' => 'roles.create',
        'description' => 'Crear roles'])->syncRoles([$admin]);
    Permission::create(['name' => 'roles.edit',
        'description' => 'Editar roles'])->syncRoles([$admin]);
    Permission::create(['name' => 'roles.destroy',
        'description' => 'Eliminar roles'])->syncRoles([$admin]);

  //PERMISOS PARA USUARIOS
    Permission::create(['name' => 'usuarios',
        'description' => 'Acceso el modulo de usuarios'])->syncRoles([$admin]);
    Permission::create(['name' => 'usuarios.index',
        'description' => 'Ver el listado de usuarios'])->syncRoles([$admin]);
    Permission::create(['name' => 'usuarios.create',
        'description' => 'Crear usuarios'])->syncRoles([$admin]);
    Permission::create(['name' => 'usuarios.edit',
        'description' => 'Editar usuarios'])->syncRoles([$admin]);
    Permission::create(['name' => 'usuarios.destroy',
        'description' => 'Eliminar usuarios'])->syncRoles([$admin]);

    //===================================
    //PERMISOS PARA PERSONAS
    Permission::create(['name' => 'personas',
    'description' => 'Acceso el modulo de personas'])->syncRoles([$admin]);
    Permission::create(['name' => 'personas.index',
    'description' => 'Ver el listado de personas'])->syncRoles([$admin]);
    Permission::create(['name' => 'personas.create',
    'description' => 'Crear personas'])->syncRoles([$admin]);
    Permission::create(['name' => 'personas.edit',
    'description' => 'Editar personas'])->syncRoles([$admin]);
    Permission::create(['name' => 'personas.destroy',
    'description' => 'Eliminar personas'])->syncRoles([$admin]);
    Permission::create(['name' => 'personas.show',
    'description' => 'Ver personas'])->syncRoles([$admin]);


    //PERMISOS PARA PILOTOS
    Permission::create(['name' => 'pilotos',
    'description' => 'Acceso el modulo de pilotos'])->syncRoles([$admin]);
    Permission::create(['name' => 'pilotos.index',
    'description' => 'Ver el listado de pilotos'])->syncRoles([$admin]);
    Permission::create(['name' => 'pilotos.create',
    'description' => 'Crear pilotos'])->syncRoles([$admin]);
    Permission::create(['name' => 'pilotos.edit',
    'description' => 'Editar pilotos'])->syncRoles([$admin]);
    Permission::create(['name' => 'pilotos.destroy',
    'description' => 'Eliminar pilotos'])->syncRoles([$admin]);

    //PERMISOS PARA PARACAIDISTAS
    Permission::create(['name' => 'paracaidistas',
    'description' => 'Acceso el modulo de paracaidistas'])->syncRoles([$admin]);
    Permission::create(['name' => 'paracaidistas.index',
    'description' => 'Ver el listado de paracaidistas'])->syncRoles([$admin]);
    Permission::create(['name' => 'paracaidistas.create',
    'description' => 'Crear paracaidistas'])->syncRoles([$admin]);
    Permission::create(['name' => 'paracaidistas.edit',
    'description' => 'Editar paracaidistas'])->syncRoles([$admin]);
    Permission::create(['name' => 'paracaidistas.destroy',
    'description' => 'Eliminar paracaidistas'])->syncRoles([$admin]);


    //PERMISOS PARA ZONAS
    Permission::create(['name' => 'zonas',
    'description' => 'Acceso el modulo de zonas'])->syncRoles([$admin]);
    Permission::create(['name' => 'zonas.index',
    'description' => 'Ver el listado de zonas'])->syncRoles([$admin]);
    Permission::create(['name' => 'zonas.create',
    'description' => 'Crear zonas'])->syncRoles([$admin]);
    Permission::create(['name' => 'zonas.edit',
    'description' => 'Editar zonas'])->syncRoles([$admin]);
    Permission::create(['name' => 'zonas.destroy',
    'description' => 'Eliminar zonas'])->syncRoles([$admin]);



    //PERMISOS PARA EVENTOS
    Permission::create(['name' => 'eventos',
    'description' => 'Acceso el modulo de eventos'])->syncRoles([$admin]);
    Permission::create(['name' => 'eventos.index',
    'description' => 'Ver el listado de eventos'])->syncRoles([$admin]);
    Permission::create(['name' => 'eventos.create',
    'description' => 'Crear eventos'])->syncRoles([$admin]);
    Permission::create(['name' => 'eventos.edit',
    'description' => 'Editar eventos'])->syncRoles([$admin]);
    Permission::create(['name' => 'eventos.destroy',
    'description' => 'Eliminar eventos'])->syncRoles([$admin]);

    //PERMISOS PARA AVIONETAS
    Permission::create(['name' => 'avionetas',
    'description' => 'Acceso el modulo de avionetas'])->syncRoles([$admin]);
    Permission::create(['name' => 'avionetas.index',
    'description' => 'Ver el listado de avionetas'])->syncRoles([$admin]);
    Permission::create(['name' => 'avionetas.create',
    'description' => 'Crear avionetas'])->syncRoles([$admin]);
    Permission::create(['name' => 'avionetas.edit',
    'description' => 'Editar avionetas'])->syncRoles([$admin]);
    Permission::create(['name' => 'avionetas.destroy',
    'description' => 'Eliminar avionetas'])->syncRoles([$admin]);



    //PERMISOS PARA ESCUELAS
    Permission::create(['name' => 'escuelas',
    'description' => 'Acceso el modulo de escuelas'])->syncRoles([$admin]);
    Permission::create(['name' => 'escuelas.index',
    'description' => 'Ver el listado de escuelas'])->syncRoles([$admin]);
    Permission::create(['name' => 'escuelas.create',
    'description' => 'Crear escuelas'])->syncRoles([$admin]);
    Permission::create(['name' => 'escuelas.edit',
    'description' => 'Editar escuelas'])->syncRoles([$admin]);
    Permission::create(['name' => 'escuelas.destroy',
    'description' => 'Eliminar escuelas'])->syncRoles([$admin]);




    //PERMISOS PARA DECOLAJES
    Permission::create(['name' => 'decolajes',
    'description' => 'Acceso el modulo de decolajes'])->syncRoles([$admin]);
    Permission::create(['name' => 'decolajes.index',
    'description' => 'Ver el listado de decolajes'])->syncRoles([$admin]);
    Permission::create(['name' => 'decolajes.create',
    'description' => 'Crear decolajes'])->syncRoles([$admin]);
    Permission::create(['name' => 'decolajes.edit',
    'description' => 'Editar decolajes'])->syncRoles([$admin]);
    Permission::create(['name' => 'decolajes.destroy',
    'description' => 'Eliminar decolajes'])->syncRoles([$admin]);
    Permission::create(['name' => 'decolajes.aprobar',
    'description' => 'Aprobar decolajes'])->syncRoles([$admin]);
    Permission::create(['name' => 'decolajes.show',
    'description' => 'Ver decolajes'])->syncRoles([$admin]);


    //PERMISOS PARA SUSPENCIONES
    Permission::create(['name' => 'suspensiones',
    'description' => 'Acceso el modulo de suspensiones'])->syncRoles([$admin]);
    Permission::create(['name' => 'suspensiones.index',
    'description' => 'Ver el listado de suspensiones'])->syncRoles([$admin]);
    Permission::create(['name' => 'suspensiones.create',
    'description' => 'Crear suspensiones'])->syncRoles([$admin]);
    Permission::create(['name' => 'suspensiones.edit',
    'description' => 'Editar suspensiones'])->syncRoles([$admin]);
    Permission::create(['name' => 'suspensiones.destroy',
    'description' => 'Eliminar suspensiones'])->syncRoles([$admin]);


    //PERMISOS PARA TIPOS DE TITULACIONES
    Permission::create(['name' => 'titulaciontipos',
    'description' => 'Acceso el modulo de tipos titulaciones'])->syncRoles([$admin]);
    Permission::create(['name' => 'titulaciontipos.index',
    'description' => 'Ver el listado de tipos titulaciones'])->syncRoles([$admin]);
    Permission::create(['name' => 'titulaciontipos.create',
    'description' => 'Crear tipos titulaciones'])->syncRoles([$admin]);
    Permission::create(['name' => 'titulaciontipos.edit',
    'description' => 'Editar tipos titulaciones'])->syncRoles([$admin]);
    Permission::create(['name' => 'titulaciontipos.destroy',
    'description' => 'Eliminar tipos titulaciones'])->syncRoles([$admin]);

    //PERMISOS PARA TIPOS DE PROPIETARIOS
    Permission::create(['name' => 'propietarios',
    'description' => 'Acceso el modulo de tipos propietarios de escuelas'])->syncRoles([$admin]);
    Permission::create(['name' => 'propietarios.index',
    'description' => 'Ver el listado de tipos propietarios de escuelas'])->syncRoles([$admin]);
    Permission::create(['name' => 'propietarios.create',
    'description' => 'Crear tipos propietarios de escuelas'])->syncRoles([$admin]);
    Permission::create(['name' => 'propietarios.edit',
    'description' => 'Editar tipos propietarios de escuelas'])->syncRoles([$admin]);
    Permission::create(['name' => 'propietarios.destroy',
    'description' => 'Eliminar tipos propietarios de escuelas'])->syncRoles([$admin]);


    //PERMISOS PARA CONDICIONES CLIMATICAS
    Permission::create(['name' => 'climaticas',
    'description' => 'Acceso el modulo de Condiciones climaticas'])->syncRoles([$admin]);
    Permission::create(['name' => 'climaticas.index',
    'description' => 'Ver el listado de Condiciones climaticas'])->syncRoles([$admin]);
    Permission::create(['name' => 'climaticas.create',
    'description' => 'Crear Condiciones climaticas'])->syncRoles([$admin]);
    Permission::create(['name' => 'climaticas.edit',
    'description' => 'Editar Condiciones climaticas'])->syncRoles([$admin]);
    Permission::create(['name' => 'climaticas.destroy',
    'description' => 'Eliminar Condiciones climaticas'])->syncRoles([$admin]);
    Permission::create(['name' => 'climaticas.show',
    'description' => 'Ver climaticas'])->syncRoles([$admin]);





    }





}
