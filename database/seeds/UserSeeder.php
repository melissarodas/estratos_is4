<?php

use Illuminate\Database\Seeder;
use App\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrador',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456789'),
            'imagen' => 'administrador.jpg',

        ])->assignRole('Admin');

        User::create([
            'name' => 'Melissa Rodas',
            'email' => 'melissa@gmail.com',
            'password' => bcrypt('123456789'),
            'imagen' => 'usuario7.jpg'

        ]);


    }
}
