

INSERT INTO public.personas (id, nombre, tipo_persona, cedula, direccion, telefono, correo, fecha_nacimiento, nacionalidad, created_at, updated_at) 
VALUES (1, 'Luis Diaz', 'PERSONA FISICA', '3443434', '2 de febrero casi mariscarl estigarribia', '0987987983', 'luisdiaz@gmail.com', '1992-12-07', 'Paraguaya', '2021-12-07 16:16:24', '2021-12-07 16:16:24');
INSERT INTO public.personas (id, nombre, tipo_persona, cedula, direccion, telefono, correo, fecha_nacimiento, nacionalidad, created_at, updated_at) 
VALUES (2, 'Jorge Gauto', 'PERSONA FISICA', '3762348', '2 de febrero casi mariscarl estigarribia', '0987654321', 'jorge@gmail.com', '1991-05-20', 'Paraguaya', '2021-12-07 16:17:21', '2021-12-07 16:17:21');
INSERT INTO public.personas (id, nombre, tipo_persona, cedula, direccion, telefono, correo, fecha_nacimiento, nacionalidad, created_at, updated_at) 
VALUES (3, 'Melissa Rodas', 'PERSONA FISICA', '3771711', 'capiata 123', '0987123753', 'melissa@gmail.com', '1992-06-10', 'Paraguaya', '2021-12-07 16:17:21', '2021-12-07 16:17:21');
INSERT INTO public.personas (id, nombre, tipo_persona, cedula, direccion, telefono, correo, fecha_nacimiento, nacionalidad, created_at, updated_at) 
VALUES (4, 'Juan Gonzalez', 'PERSONA FISICA', '3652879', 'asuncion 123', '0987123012', 'juan@gmail.com', '1992-09-01', 'Paraguaya', '2021-12-07 16:17:21', '2021-12-07 16:17:21');

INSERT INTO public.paracaidistas (id, licencia, categoria, cant_saltos, tamaño_velamen, persona_id, created_at, updated_at) 
VALUES (1, 1122334, 'B', 40, '50', 1, '2021-12-07 16:20:36', '2021-12-07 16:20:36');
INSERT INTO public.paracaidistas (id, licencia, categoria, cant_saltos, tamaño_velamen, persona_id, created_at, updated_at) 
VALUES (2, 223344, 'A', 20, '20', 2, '2021-12-07 16:20:36', '2021-12-07 16:20:36');
INSERT INTO public.paracaidistas (id, licencia, categoria, cant_saltos, tamaño_velamen, persona_id, created_at, updated_at) 
VALUES (3, 4455667, 'C', 42, '31', 3, '2021-12-07 16:20:36', '2021-12-07 16:20:36');

INSERT INTO public.pilotos (id, licencia, vencimiento, persona_id, created_at, updated_at) 
VALUES (1, 131313, '2021-12-30', 4, '2021-12-07 16:19:01', '2021-12-07 16:19:01');
INSERT INTO public.pilotos (id, licencia, vencimiento, persona_id, created_at, updated_at) 
VALUES (2, 141414, '2021-12-30', 3, '2021-12-07 16:19:01', '2021-12-07 16:19:01');
INSERT INTO public.pilotos (id, licencia, vencimiento, persona_id, created_at, updated_at) 
VALUES (3, 151515, '2021-12-30', 2, '2021-12-07 16:19:01', '2021-12-07 16:19:01');

INSERT INTO public.titulacion_tipos (id, nombre, created_at, updated_at) VALUES (1, 'tipo 1', '2021-12-07 14:17:38', '2021-12-07 14:17:38');
INSERT INTO public.titulacion_tipos (id, nombre, created_at, updated_at) VALUES (2, 'tipo 2', '2021-12-07 14:17:38', '2021-12-07 14:17:38');

INSERT INTO public.avionetas (id, modelo, marca, matricula, capacidad, created_at, updated_at) VALUES (1, 'modelo 1', 'marca 1', 'matricula1', 60, '2021-12-07 17:27:18', '2021-12-07 17:27:18');
INSERT INTO public.avionetas (id, modelo, marca, matricula, capacidad, created_at, updated_at) VALUES (2, 'modelo 2', 'marca 2', 'matricula2', 45, '2021-12-07 17:27:18', '2021-12-07 17:27:18');

INSERT INTO public.zonas (id, nombre, latitud, longitud, ciudad, created_at, updated_at) VALUES (1, 'zona 1', '-24.664320', '-56,444671', 'ciudad 1', '2021-12-07 17:29:16', '2021-12-07 17:29:16');
INSERT INTO public.zonas (id, nombre, latitud, longitud, ciudad, created_at, updated_at) VALUES (2, 'zona 2', '-24.664320', '-56,444671', 'ciudad 2', '2021-12-07 17:29:16', '2021-12-07 17:29:16');

INSERT INTO public.eventos (id, nombre, fecha, organizador_id, rta_id, zona_id, created_at, updated_at) VALUES (1, 'evento 1', '2021-12-23', 2, 3, 1, '2021-12-07 17:32:15', '2021-12-07 17:32:15');
INSERT INTO public.eventos (id, nombre, fecha, organizador_id, rta_id, zona_id, created_at, updated_at) VALUES (2, 'evento 2', '2021-12-23', 1, 2, 1, '2021-12-07 17:32:15', '2021-12-07 17:32:15');
