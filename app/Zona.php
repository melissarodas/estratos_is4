<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    protected $fillable = ['nombre','latitud','longitud','ciudad'];

    public function eventos()
    {
        return $this->hasMany(Evento::class);
    }


}
