<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piloto extends Model
{
    protected $fillable = ['licencia', 'vencimiento','persona_id'];

    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }


}
