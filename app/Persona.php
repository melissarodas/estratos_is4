<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $fillable = ['nombre','tipo_persona','cedula','direccion','telefono','correo','fecha_nacimiento','nacionalidad'];


    public function pilotos(){
        return $this->hasMany(Piloto::class);
    }
    public function paracaidistas(){
        return $this->hasMany(Paracaidista::class);
    }



}
