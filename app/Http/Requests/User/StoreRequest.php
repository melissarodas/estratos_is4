<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' =>'required|string|max:50',
            'email' =>'required|email|max:255|unique:users'
        ];
    }

    public function messages()
    {
        return[
            'name.required' =>'El campo "nombre" es obligatorio.',
            'name.string' =>'Campo "nombre": el valor no es correcto.',
            'name.max' =>'Solo se permite 50 caracteres.',

            'email.required' =>'El campo "email" es requerido.',
            'email.email' =>'El tipo no es correcto.',
            'email.max' =>'Solo se permite 255 caracteres.',
            'email.unique' =>'El valor debe ser único.',

        ];
    }
}
