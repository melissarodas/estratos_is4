<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $email = $this->route('user')->email ?? null;

        return [
            'name' =>'required|string|max:50',
            'email' =>['required',Rule::unique('users')->ignore($email)],

        ];
    }

    public function messages()
    {
        return[
            'name.required' =>'El campo "name" es requerido.',
            'name.string' =>'El valor de "name" no es correcto.',
            'name.max' =>'El campo "name" solo permite 50 caracteres.',

            'email.required' =>'El campo "email" es requerido.',
            'email.unique' =>'El valor del campo "email" debe ser único.',
        ];
    }
}
