<?php

namespace App\Http\Requests\TipoTitulacion;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' =>'required|max:255',
        ];
    }

    public function messages()
    {
        return[
            'nombre.required' =>'El campo licencia es requerido.',
            'nombre.max' =>'Solo se permite hasta 255 caracteres.',
        ];
    }
}
