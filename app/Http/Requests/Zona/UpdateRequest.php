<?php

namespace App\Http\Requests\Zona;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' =>'required|unique:zonas,nombre,'.$this->route('zona')->id.'|max:11',
            'ciudad'   =>'required|string',
            'latitud' =>'required|max:100|min:6',
            'longitud' =>'required|max:100|min:6',

        ];
    }

    public function messages()
    {
        return[
            'nombre.required' =>'El campo nombre es requerido.',
            'nombre.max' =>'Solo se permite hasta 11 caracteres.',
            'nombre.unique' =>'El nombre de la zona ya se encuentra registrado.',

            'ciudad.required' =>'El campo ciudad es requerido.',
            'ciudad.string' =>'El tipo de dato no es correcto',

            'latitud.required' =>'El campo latitud es requerido.',
            'latitud.max' =>'Solo se permite hasta 100 caracteres.',
            'latitud.min' =>'Ingrese como minimo 6 caracteres.',

            'longitud.required' =>'El campo longitud es requerido.',
            'longitud.max' =>'Solo se permite hasta 100 caracteres.',
            'longitud.min' =>'Ingrese como minimo 6 caracteres.',


        ];
    }
}
