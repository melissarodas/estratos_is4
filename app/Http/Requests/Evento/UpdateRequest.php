<?php

namespace App\Http\Requests\Evento;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' =>'required|max:255',
        ];
    }

    public function messages()
    {
        return[
            'nombre.required' =>'El campo nombre es requerido.',
            'nombre.max' =>'Solo se permite hasta 255 caracteres.',
        ];
    }
}
