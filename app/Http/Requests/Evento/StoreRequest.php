<?php

namespace App\Http\Requests\Evento;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre'    =>'required|max:255',
            'organizador_id'  =>'required',
            'rta_id'  =>'required',
            'zona_id'  =>'required',


        ];
    }

    public function messages()
    {
        return[
            'nombre.required' =>'El campo nombre es requerido.',
            'nombre.max' =>'Solo se permite hasta 255 caracteres.',
            'organizador_id.required' =>'El campo ORGANIZADOR es requerido.',
            'rta_id.required' =>'El campo RTA es requerido.',
            'zona_id.required' =>'El campo Zona es requerido.',

        ];
    }
}
