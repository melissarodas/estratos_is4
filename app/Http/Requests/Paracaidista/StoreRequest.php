<?php

namespace App\Http\Requests\Paracaidista;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'licencia'        =>'required|max:11|min:6|unique:paracaidistas',
            'categoria'       =>'required|string',
            'cant_saltos'     =>'required|max:3',
            'persona_id'      =>'required',
            'tamaño_velamen'  =>'required',


        ];
    }

    public function messages()
    {
        return[
            'licencia.required' =>'El campo licencia es requerido.',
            'licencia.max' =>'Solo se permite hasta 11 caracteres.',
            'licencia.min' =>'Se requiere como minimo 6 caracteres.',
            'licencia.unique' =>'El numero de licencia ya se encuentra registrado.',

            'categoria.required' =>'El campo categoria es requerido.',
            'categoria.string' =>'El tipo de dato no es correcto',

            'cant_saltos.required' =>'El campo Cantidad de saltos es requerido.',
            'cant_saltos.max' =>'Solo se permite hasta 11 caracteres.',
            'persona_id.required' =>'El campo de persona es requerido.',
            'tamaño_velamen.required' =>'El campo de tamaño del velamen es requerido.',

        ];
    }
}
