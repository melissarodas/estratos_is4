<?php

namespace App\Http\Requests\Persona;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'cedula'        =>'required|max:9|min:6|unique:personas',
            'nombre'        =>'required|string|max:255',
            'telefono'      =>'required',
            'nacionalidad'  =>'required|string|max:255',
            'correo'        =>'required|email|max:255|unique:personas'
        ];

    }

    public function messages()
    {
        return[
            'cedula.required'   =>'El campo cedula es requerido.',
            'cedula.max'        =>'Solo se permite hasta 9 caracteres.',
            'cedula.min'        =>'Se requiere como minimo 6 caracteres.',
            'cedula.unique'     =>'El numero de cedula ya existe.',

            'nombre.required'   =>'El campo nombre es requerido.',
            'nombre.string'     =>'Solo se permiten letras',
            'nombre.max'        =>'Solo se permite hasta 255 caracteres.',

            'nacionalidad.required'   =>'El campo nacionalidad es requerido.',
            'nacionalidad.string'     =>'Solo se permiten letras',
            'nacionalidad.max'        =>'Solo se permite hasta 255 caracteres.',

            'correo.required'   =>'El campo "email" es requerido.',
            'correo.email'      =>'El tipo no es correcto.',
            'correo.max'        =>'Solo se permite 255 caracteres.',
            'correo.unique'     =>'El valor debe ser único.',

            'telefono.required' =>'El campo "telefono" es requerido.',
            
        ];
    }
}
