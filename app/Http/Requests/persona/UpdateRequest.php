<?php

namespace App\Http\Requests\Persona;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $correo = $this->route('personas')->correo ?? null;

        return [
            'cedula'        =>'required|max:9|min:6',
            'nombre'        =>'required|string',
            'telefono'      =>'required',
            'correo'        =>'required|unique:personas,correo,'.$this->route('persona')->id,

        ];
    }

    public function messages()
    {
        return[
            'cedula.required'   =>'El campo cedula es requerido.',
            'cedula.max'        =>'Solo se permite hasta 9 caracteres.',
            'cedula.min'        =>'Se requiere como minimo 6 caracteres.',
            'cedula.unique'     =>'El numero de cedula ya existe.',

            'nombre.required'   =>'El campo nombre es requerido.',
            'nombre.string'     =>'Solo se permiten letras',
            'nombre.max'        =>'Solo se permite hasta 255 caracteres.',

            'correo.required'   =>'El campo "email" es requerido.',

            'telefono.required' =>'El campo "telefono" es requerido.',

        ];
    }
}
