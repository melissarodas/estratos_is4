<?php

namespace App\Http\Requests\Propietario;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'escuela_id'  =>'required',
        ];
    }

    public function messages()
    {
        return[
            'escuela_id.required' =>'El campo ESCUELA es requerido.',

        ];
    }
}
