<?php

namespace App\Http\Requests\Suspension;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'motivo'            =>'required|max:255',
            'paracaidista_id'   =>'required',
            'fecha'             =>'required',
            'duracion'          =>'required',

        ];
    }

    public function messages()
    {
        return[
            'motivo.required' =>'El campo motivo es requerido.',
            'motivo.max' =>'Solo se permite hasta 255 caracteres.',

            'paracaidista_id.required' =>'El campo ORGANIZADOR es requerido.',

            'fecha.required' =>'El campo fecha es requerido.',
            'duracion.required' =>'El campo Duracion es requerido.',

        ];
    }
}
