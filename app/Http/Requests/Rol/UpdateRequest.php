<?php

namespace App\Http\Requests\Rol;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' =>'required|string|max:50',
        ];
    }

    public function messages()
    {
        return[
            'name.required' =>'El campo "name" es requerido.',
            'name.string' =>'El valor de "name" no es correcto.',
            'name.max' =>'El campo "name" solo permite 50 caracteres.',

        ];
    }
}
