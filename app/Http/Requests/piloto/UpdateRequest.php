<?php

namespace App\Http\Requests\Piloto;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'licencia' =>'required|min:6|unique:pilotos,licencia,'.$this->route('piloto')->id.'|max:11',
        ];
    }

    public function messages()
    {
        return[
            'licencia.required' =>'El campo licencia es requerido.',
            'licencia.max' =>'Solo se permite hasta 11 caracteres.',
            'licencia.min' =>'Se requiere como minimo 6 caracteres.',
            'licencia.unique' =>'El numero de licencia ya se encuentra registrado.',
        ];
    }
}
