<?php

namespace App\Http\Requests\Piloto;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'licencia' =>'required|max:11|min:6|unique:pilotos',
            'persona_id'  =>'required',

        ];
    }

    public function messages()
    {
        return[
            'licencia.required' =>'El campo licencia es requerido.',
            'licencia.max' =>'Solo se permite hasta 11 caracteres.',
            'licencia.min' =>'Se requiere como minimo 6 caracteres.',
            'licencia.unique' =>'El numero de licencia ya se encuentra registrado.',
            'persona_id.required' =>'El campo de persona es requerido.',

        ];
    }
}
