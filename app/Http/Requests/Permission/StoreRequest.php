<?php

namespace App\Http\Requests\Permission;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' =>'required|string|max:50',
        ];
    }

    public function messages()
    {
        return[
            'name.required' =>'El campo "nombre" es obligatorio.',
            'name.string' =>'Campo "nombre": el valor no es correcto.',
            'name.max' =>'Solo se permite 50 caracteres.',

        ];
    }
}
