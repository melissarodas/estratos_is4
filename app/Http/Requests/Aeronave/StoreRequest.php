<?php

namespace App\Http\Requests\Aeronave;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'modelo'    =>'required|string|max:100',
            'matricula' =>'required|string|max:100|unique:avionetas',
            'capacidad' =>'required',
            'marca'    =>'required|string|max:100',

        ];
    }

    public function messages()
    {
        return[
            'modelo.required'   =>'El campo modelo es requerido.',
            'modelo.max'        =>'Solo se permite hasta 100 caracteres.',
            'modelo.string'     =>'El valor no es correcto.',

            'matricula.required'   =>'El campo matricula es requerido.',
            'matricula.max'        =>'Solo se permite hasta 100 caracteres.',
            'matricula.string'     =>'El valor no es correcto.',
            'matricula.unique'    =>'El numero de matricula ya se encuentra registrada.',

            'capacidad.required'   =>'El campo capacidad es requerido.',

            'marca.required'   =>'El campo marca es requerido.',
            'marca.max'        =>'Solo se permite hasta 100 caracteres.',
            'marca.string'     =>'El valor no es correcto.',
        ];
    }
}
