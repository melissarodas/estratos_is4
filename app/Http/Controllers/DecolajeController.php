<?php

namespace App\Http\Controllers;

use App\Piloto;
use App\Avioneta;
use App\Decolaje;
use App\Evento;
use App\Paracaidista;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DecolajeController extends Controller
{

    public function index()
    {
        $pilotos = Piloto::get();
        $avionetas = Avioneta::get();
        $decolajes = Decolaje::get();

        return view('admin.decolaje.index',compact('pilotos','avionetas','decolajes'));
    }

    public function create()
    {
       // $pilotos = Piloto::get();
       $pilotos = \DB::table('pilotos')->where('pilotos.vencimiento','>=', Carbon::now())->get();
       //dd($pilotos);
        $avionetas = Avioneta::get();
        $paracaidistas = Paracaidista::get();
        $eventos = Evento::get();

        return view('admin.decolaje.create',compact('pilotos','avionetas','paracaidistas','eventos'));
    }

    public function store(Request $request)
    {
        //dd($request);
        DB::beginTransaction();
        try{
            \Log::info('A');

            $decolaje = Decolaje::create($request->all()+[]);
            \Log::info('B');
            foreach ($request->paracaidista_id as $key => $paracaidista){
                $resultado[] = array(
                    "paracaidista_id" => $request->paracaidista_id[$key],
                );
            }
            \Log::info('B');
            $decolaje->decolajeDetalle()->createMany($resultado);
            \Log::info('C');

            ///Insertando pilotos x decolaje
            foreach ($request->piloto_id as $key => $piloto){
                $resultadoPiloto[] = array(
                    "piloto_id"     => $request->piloto_id[$key],
                    "decolaje_id"   => $decolaje->id,

                );
            }

            \Log::info('Insertando pilotos al decolaje');
            \Log::info($resultadoPiloto);

            $decolaje->decolajePiloto()->createMany($resultadoPiloto);
            \Log::info('f');

            


            DB::commit();
            return redirect()->route('decolajes.index')->with('guardar','ok');
        }catch (\Exception $e){
            DB::rollBack();
            \Log::critical($e->getMessage());
            \Log::info('Error al registrar el decolaje');
            return redirect()->route('decolajes.index')->with('error','ok');
        }

    }

 
    public function show(Decolaje $decolaje)
    {
        $pilotos = Piloto::get();
        $avionetas = Avioneta::get();
        $decolajeDetalles = $decolaje->decolajeDetalle;
        $decolajePilotos= $decolaje->decolajePiloto;
        //dd($decolajePilotos);


        return view('admin.decolaje.show',compact('pilotos','avionetas','decolaje','decolajeDetalles','decolajePilotos'));
    }

   
    public function edit(Decolaje $decolaje)
    {
        //
    }

   
    public function update(Request $request, Decolaje $decolaje)
    {
        //
    }

    
    public function destroy(Decolaje $decolaje)
    {
        //
    }

    public function pdf(Decolaje $decolaje)
    {
        // $decolajesDetalles = $decolaje->decolajeDetalle;
    
  

        // $pdf = PDF::loadView('admin.venta.pdf', compact('subtotal','decolajesDetalles','venta', 'empresa','total','total_general'));
        // return $pdf->download('Reporte_de_venta_'.$venta->id.'.pdf');
    }

    public function change_status( Decolaje $decolaje)
    {
        if($decolaje->estado == 'PENDIENTE'){
            $decolaje->update(['estado'=>'APROBADO']);
            return redirect()->back()->with('aprobado','ok');
        }else{
            $decolaje->update(['estado'=>'DESAPROBADO']);
            return redirect()->back()->with('desaprobado','ok');
        }
    }








}
