<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

use App\Http\Requests\Permission\StoreRequest;
use App\Http\Requests\Permission\UpdateRequest;

class PermissionController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:permisos.index')->only('index');
         $this->middleware('can:permisos.create')->only('create','store');
         $this->middleware('can:permisos.edit')->only('edit','update');
         $this->middleware('can:permisos.destroy')->only('destroy');
    }
    public function index()
    {
        $permisos = Permission::all();
        return view('admin.permission.index', compact('permisos'));
    }


    public function create()
    {
        return view('admin.permission.create');
    }


    public function store(StoreRequest $request)
    {
        // $request->validate([
        //     'name' => 'required'
        // ]);

        $permission = Permission::create($request->all());
        return redirect()->route('permissions.index', $permission)->with('guardar','ok');
    }


    public function show($id)
    {
        //
    }


    public function edit(Permission $permission)
    {

        return view('admin.permission.edit', compact('permission'));
    }


    public function update(UpdateRequest $request, Permission $permission)
    {
        // $request->validate([
        //     'name' => 'required'
        // ]);

        $permission->update($request->all());
        return redirect()->route('permissions.edit', $permission)->with('info', 'El permiso se actualizó con éxito.');
    }


    public function destroy(Permission $permission)
    {
        $permission->delete();
        return redirect()->route('permissions.index', $permission)->with('eliminar','ok');
    }
}
