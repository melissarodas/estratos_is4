<?php

namespace App\Http\Controllers;

use App\Paracaidista;
use Illuminate\Http\Request;
use App\SuspensionParacaidista;
use Yajra\DataTables\DataTables;
use App\Http\Requests\Suspension\StoreRequest;
use App\Http\Requests\Suspension\UpdateRequest;

class SuspensionParacaidistaController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:suspensiones.index')->only('index');
         $this->middleware('can:suspensiones.create')->only('create','store');
         $this->middleware('can:suspensiones.edit')->only('edit','update');
         $this->middleware('can:suspensiones.destroy')->only('destroy');
    }

    public function index(Request $request)
    {
        $paracaidistas = Paracaidista::get();
        if($request->ajax()){

            $data = \DB::table('suspension_paracaidistas')
            ->join('paracaidistas','paracaidistas.id','=','suspension_paracaidistas.paracaidista_id')
            ->join('personas','personas.id','=','paracaidistas.persona_id')
            ->select('suspension_paracaidistas.id','suspension_paracaidistas.motivo','suspension_paracaidistas.fecha','suspension_paracaidistas.duracion','personas.nombre as paracaidista')
            ->get();

            \Log::info($data);

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Eliminar" title ="Eliminar" class="btn btn-danger btn-sm float-right mr-3 eliminarSuspension"> <i class="far fa-trash-alt"></i></a>';
                        $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Editar" title ="Editar" class="edit btn btn-primary btn-sm float-right mr-3 editarSuspension"><i class="far fa-edit"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            //LOGUEO DE ACCION
            \Log::info('Cargando datos al index de suspensiones:');
            \Log::info($paracaidistas->toArray());

        return view('admin.suspension.index',compact('paracaidistas'));
    }


    public function create()
    {
        //
    }


    public function store(StoreRequest $request)
    {

        try{
           
            $suspension = SuspensionParacaidista::updateOrCreate(
                ['id'               => $request->suspension_id],
                ['paracaidista_id'  => $request->paracaidista_id,
                'motivo'            => $request->motivo,
                'duracion'          => $request->duracion,
                'fecha'             => $request->fecha]);
                                               
                
            //LOGUEO DE ACCION
            \Log::info('Suspension creada correctamente:');
            \Log::info($suspension->toArray());

            return response()->json(['success'=>'La suspension se ha agregado correctamente.']);

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear la suspension');
            return redirect()->route('suspensionParacaidistas.index')->with('guardar','ok');

        }
    }


    public function show(SuspensionParacaidista $suspension)
    {
        //
    }


    public function edit($id)
    {
        $suspension = SuspensionParacaidista::find($id);
        //LOGUEO DE ACCION
        \Log::info('Obteniendo datos para la vista EDIT');
        \Log::info($suspension->toArray());
        return response()->json($suspension);
    }


    public function update(UpdateRequest $request, SuspensionParacaidista $suspensionParacaidista)
    {
        //
    }


    public function destroy($id)
    {

        try{
            SuspensionParacaidista::find($id)->delete();
            //LOGUEO DE ACCION
            \Log::info('Registro de suspension ha sido eliminado correctamente');
            return response()->json(['success'=>'La suspensión ha sido eliminada correctamente.']);
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar la suspensión');
            return redirect()->route('suspensionParacaidistas.index')->with('eliminar','error');
        }
    }

  
}
