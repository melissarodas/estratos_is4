<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:usuarios.index')->only('index');
         $this->middleware('can:usuarios.create')->only('create','store');
         $this->middleware('can:usuarios.edit')->only('edit','update');
         $this->middleware('can:usuarios.destroy')->only('destroy');
    }

    public function index()
    {
        $users = User::get();
        return view('admin.user.index',compact('users'));
    }


    public function create()
    {
        $roles =  Role::get();
        return view('admin.user.create',compact('roles'));
    }


    public function store(StoreRequest $request)
    {
        if($request->hasFile('foto')){
            $file = $request->file('foto');
            $image_name = time().'_'.$file->getClientOriginalName();
            $file->move(public_path("/imagen/usuarios"),$image_name);
        }

        $user = User::create($request->all()+[
             'imagen' =>  $image_name,
         ]);
        $user->update(['password'=> Hash::make($request->password)]);
        $user->roles()->sync($request->get('roles'));
        return redirect()->route('users.index')->with('guardar','ok');
    }


    public function show(User $user)
    {
        return view('admin.user.show',compact('user'));

    }


    public function edit(User $user)
    {
        $roles =  Role::all();
        return view('admin.user.edit',compact('user','roles'));
    }


    public function update(Request $request, User $user)
    {

        Validator::make($request->all(), [
            'email' => [
                'required',
                    Rule::unique('users')->ignore($user->id),
            ],
        ]);


        if($request->hasFile('picture')){
            $file = $request->file('picture');
            $image_name = time().'_'.$file->getClientOriginalName();
            $file->move(public_path("/imagen/usuarios"),$image_name);
        }
        //if ($user->id == 1) {

           //return redirect()->route('users.index');
        //}else{

            // $user->update($request->all());
            // $user->roles()->sync($request->get('roles'));
            // return redirect()->route('users.index');
            $user->update($request->all()+[
                'imagen' =>  $image_name,
            ]);

            $user->roles()->sync($request->roles);

            return redirect()->route('users.edit',$user)->with('info','Se asignaron los roles correctamente.');
        //}
    }



    public function destroy(User $user)
    {
         // $user->delete();
        // return back();
        if ($user->id == 1) {
            return back();
        } else {
            $user->delete();
            return back()->with('eliminar','ok');
        }
    }
}
