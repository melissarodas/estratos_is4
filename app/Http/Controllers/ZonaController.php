<?php

namespace App\Http\Controllers;

use App\Zona;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Requests\Zona\StoreRequest;
use App\Http\Requests\Zona\UpdateRequest;

class ZonaController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:zonas.index')->only('index');
         $this->middleware('can:zonas.create')->only('create','store');
         $this->middleware('can:zonas.edit')->only('edit','update');
         $this->middleware('can:zonas.destroy')->only('destroy');
    }

    public function index(Request $request)
    {
        $zonas = Zona::latest()->get();
        if($request->ajax()){

            $data = \DB::table('zonas')
            ->select('zonas.id','zonas.nombre','zonas.latitud','zonas.longitud','zonas.ciudad')
            ->get();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Eliminar" title ="Eliminar" class="btn btn-danger btn-sm float-right mr-3 eliminarZona"> <i class="far fa-trash-alt"></i></a>';
                        $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Editar" title ="Editar" class="edit btn btn-primary btn-sm float-right mr-3 editarZona"><i class="far fa-edit"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            //LOGUEO DE ACCION
            \Log::info('Cargando datos al index de zonas:');
            \Log::info($zonas->toArray());

        return view('admin.zona.index',compact('zonas'));
    }


    public function create()
    {
        //
    }


    public function store(StoreRequest $request)
    {
        try{
           
            $zona = Zona::updateOrCreate(
                ['id'       => $request->zona_id],
                ['nombre'   => $request->nombre,
                'ciudad'    => $request->ciudad,
                'latitud'   => $request->latitud,
                'longitud'  => $request->longitud]);

            //LOGUEO DE ACCION
            \Log::info('Zona creada correctamente:');
            \Log::info($zona->toArray());

            return response()->json(['success'=>'La zona se ha agregado correctamente.']);

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear la zona');
            return redirect()->route('zonas.index')->with('guardar','ok');

        }

    }


    public function show(Zona $zona)
    {
        //
    }


    public function edit($id)
    {
        $zona = Zona::find($id);
        //LOGUEO DE ACCION
        \Log::info('Obteniendo datos para la vista EDIT');
        \Log::info($zona->toArray());
        return response()->json($zona);    }


    public function update(UpdateRequest $request, Zona $zona)
    {
        //
    }


    public function destroy($id)
    {
        try{
            Zona::find($id)->delete();
            //LOGUEO DE ACCION
            \Log::info('Registro de zona ha sido eliminado correctamente');
            return response()->json(['success'=>'La Zona ha sido eliminada correctamente.']);
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar la zona');
            return redirect()->route('zonas.index')->with('eliminar','error');
        }

    }
}
