<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


use App\Http\Requests\Rol\StoreRequest;
use App\Http\Requests\Rol\UpdateRequest;


class RoleController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:roles.index')->only('index');
         $this->middleware('can:roles.create')->only('create','store');
         $this->middleware('can:roles.edit')->only('edit','update');
         $this->middleware('can:roles.destroy')->only('destroy');
    }

    public function index()
    {
        $roles = Role::all();
        return view('admin.role.index', compact('roles'));
    }


    public function create()
    {
        $permissions = Permission::all();
        return view('admin.role.create' ,compact('permissions'));

    }

    public function store(StoreRequest $request)
    {
        // $request->validate([
        //     'name' => 'required'
        // ]);
        $role = Role::create($request->all());
        $role->permissions()->sync($request->permissions);
        return redirect()->route('roles.index', $role)->with('info', 'El rol se creó con éxito.');

    }

    public function show(Role $role)
    {
        $permissions = Permission::all();

        return view('admin.role.show', compact('role','permissions'));
    }

    public function edit(Role $role)
    {
        $permissions = Permission::all();

        return view('admin.role.edit', compact('role','permissions'));
    }

    public function update(UpdateRequest $request, Role $role)
    {
        // $request->validate([
        //     'name' => 'required'
        // ]);

        $role->update($request->all());
        $role->permissions()->sync($request->permissions);
        return redirect()->route('roles.edit', $role)->with('info', 'El rol se actualizó con éxito.');
    }

    public function destroy(Role $role)
    {
        $role->delete();
        return redirect()->route('roles.index', $role)->with('eliminar', 'ok');

    }
}
