<?php

namespace App\Http\Controllers;

use App\Evento;
use App\Persona;
use App\Zona;
use Illuminate\Http\Request;
use App\Http\Requests\Evento\StoreRequest;
use App\Http\Requests\Evento\UpdateRequest;

class EventoController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:eventos.index')->only('index');
         $this->middleware('can:eventos.create')->only('create','store');
         $this->middleware('can:eventos.edit')->only('edit','update');
         $this->middleware('can:eventos.destroy')->only('destroy');
    }

    public function index()
    {
        $eventos = Evento::get();
        $zonas = Zona::get();
        $personas = Persona::get();

<<<<<<< HEAD
        if($request->ajax()){

            $data = \DB::table('eventos')
            ->join('personas','personas.id','=','eventos.organizador_id')
            //->join('personas','personas.id','=','eventos.rta_id')
            ->join('zonas','zonas.id','=','eventos.zona_id')
            ->select('eventos.id','eventos.nombre','eventos.fecha','personas.nombre as organizador', 'zonas.nombre as zona')
            ->get();

            
            \Log::info($data);
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Eliminar" title ="Eliminar" class="btn btn-danger btn-sm float-right mr-3 eliminarEvento"> <i class="far fa-trash-alt"></i></a>';
                        $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Editar" title ="Editar" class="edit btn btn-primary btn-sm float-right mr-3 editarEvento"><i class="far fa-edit"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            //LOGUEO DE ACCION
            \Log::info('Cargando datos al index de eventos:');
            //\Log::info($data->toArray());

        return view('admin.evento.index',compact('eventos','zonas','personas'));



=======
        return view('admin.evento.index',compact('eventos','zonas','personas'));
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
    }


    public function create()
    {
      //
    }


    public function store(StoreRequest $request)
    {

        try{
            $evento = Evento::updateOrCreate(
                ['id'             => $request->evento_id],
                ['nombre'         => $request->nombre,
                'fecha'           => $request->fecha,
                'lugar'           => $request->lugar,
                'organizador_id'  => $request->organizador_id,
                'rta_id'          => $request->rta_id,
                'zona_id'         => $request->zona_id,
            ]);


            //LOGUEO DE ACCION
            \Log::info('Evento creada correctamente:');
            \Log::info($evento->toArray());
            
            return response()->json(['success'=>'El evento se ha agregado correctamente.']);

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear el evento');
            return redirect()->route('eventos.index')->with('error','ok');
        }
    }


    public function show(Evento $evento)
    {
        //
    }


    public function edit($id)
    {
<<<<<<< HEAD
        $evento = Evento::find($id);

        //LOGUEO DE ACCION
        \Log::info('Obteniendo datos para la vista EDIT');
        \Log::info($evento);
       return response()->json($evento);
=======
        $zonas = Zona::get();
        $personas = Persona::get();
        $organizadores = Persona::get();

        return view('admin.evento.edit',compact('evento','zonas','personas','organizadores'));
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
    }


    public function update(UpdateRequest $request, Evento $evento)
    {
        //
    }


    public function destroy($id)
    {

        try{
            Evento::find($id)->delete();
            
            //LOGUEO DE ACCION
            \Log::info('Registro de evento eliminado correctamente');

            return response()->json(['success'=>'El evento ha sido eliminado correctamente.']);
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar el evento');
            return redirect()->route('eventos.index')->with('eliminar','error');
        }
    }

     /*
        AJAX request
    */
    public function getEventos(Request $request){

        $search = $request->search;

        if($search == ''){
            $eventos = Evento::orderby('nombre','asc')->select('id','nombre')->limit(5)->get();
        }else{
            $eventos = Evento::orderby('nombre','asc')->select('id','nombre')->where('nombre', 'like', '%' .$search . '%')->limit(5)->get();
        }

        $response = array();
        foreach($eventos as $evento){
            $response[] = array(
                    "id"=>$evento->id,
                    "text"=>$evento->nombre
            );
        }

        return response()->json($response);
    }
}
