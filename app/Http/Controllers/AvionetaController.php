<?php

namespace App\Http\Controllers;

use App\Avioneta;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Aeronave\StoreRequest;
use App\Http\Requests\Aeronave\UpdateRequest;

class AvionetaController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:avionetas.index')->only('index');
         $this->middleware('can:avionetas.create')->only('create','store');
         $this->middleware('can:avionetas.edit')->only('edit','update');
         $this->middleware('can:avionetas.destroy')->only('destroy');
    }
    public function index(Request $request)
    {
        $avionetas = Avioneta::latest()->get();
        if($request->ajax()){

            $data = Avioneta::latest()->get();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Eliminar" title ="Eliminar" class="btn btn-danger btn-sm float-right mr-1 eliminarAvioneta"> <i class="far fa-trash-alt"></i></a>';
                        $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Editar" title ="Editar" class="edit btn btn-primary btn-sm float-right mr-1 editarAvioneta"><i class="far fa-edit"></i></a>';

                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        //LOGUEO DE ACCION
        \Log::info('Cargando datos al index de avionetas:');
        \Log::info($avionetas->toArray());

        return view('admin.avioneta.index',compact('avionetas'));

    }


    public function create()
    {
        //
    }


    public function store(StoreRequest $request)
    {
        try{
            $avioneta = Avioneta::updateOrCreate(
                ['id'               => $request->avioneta_id],
                ['modelo'           => $request->modelo,
                'marca'             => $request->marca,
                'matricula'         => $request->matricula,
                'capacidad'         => $request->capacidad ]);

            //LOGUEO DE ACCION
            \Log::info('Avioneta creada correctamente:');
            \Log::info($avioneta->toArray());

            return response()->json(['success'=>'La avioneta se ha agregado correctamente.']);

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear la avioneta');
            return redirect()->route('avionetas.index')->with('error','ok');
        }
    }

    public function show(Avioneta $avion)
    {
        //
    }


    public function edit($id)
    {
        $avioneta = Avioneta::find($id);

        //LOGUEO DE ACCION
        \Log::info('Obteniendo datos para la vista EDIT');
        \Log::info($avioneta);
       return response()->json($avioneta);

    }


    public function update(UpdateRequest $request, Avioneta $avioneta)
    {
        //
    }


    public function destroy($id)
    {
        try{
            Avioneta::find($id)->delete();

            //LOGUEO DE ACCION
            \Log::info('Registro de avioneta eliminado correctamente');

            return response()->json(['success'=>'La avioneta ha sido eliminada correctamente.']);
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar la avioneta');
            return redirect()->route('avionetas.index')->with('eliminar','error');
        }
    }

     /*
        AJAX request
    */
    public function getAvionetas(Request $request){

        $search = $request->search;

        if($search == ''){
            $avionetas = Avioneta::orderby('matricula','asc')->select('id','matricula')->limit(5)->get();
        }else{
            $avionetas = Avioneta::orderby('matricula','asc')->select('id','matricula')->where('matricula', 'like', '%' .$search . '%')->limit(5)->get();
        }

        $response = array();
        foreach($avionetas as $avioneta){
            $response[] = array(
                    "id"=>$avioneta->id,
                    "text"=>$avioneta->matricula
            );
        }

        return response()->json($response);
    }
}
