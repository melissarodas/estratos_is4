<?php

namespace App\Http\Controllers;

use App\Persona;
use Illuminate\Http\Request;
<<<<<<< HEAD
use App\Http\Requests\Persona\StoreRequest;
use App\Http\Requests\Persona\UpdateRequest;
use Yajra\DataTables\DataTables;
=======
use App\Http\Requests\persona\StoreRequest;
use App\Http\Requests\persona\UpdateRequest;
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96

class PersonaController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:personas.index')->only('index');
         $this->middleware('can:personas.create')->only('create','store');
         $this->middleware('can:personas.edit')->only('edit','update');
         $this->middleware('can:personas.destroy')->only('destroy');
         $this->middleware('can:personas.show')->only('show');

    }

    public function index()
    {
        $personas = Persona::get();
        return view('admin.persona.index',compact('personas'));
    }


    public function create()
    {
        return view('admin.persona.create');
    }


    public function store(StoreRequest $request)
    {
        try{
            Persona::create($request->all());
            \Log::info('Persona creada correctamente');
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear la persona');
        }
        return redirect()->route('personas.index')->with('guardar','ok');
    }


    public function show(Persona $persona)
    {
        return view('admin.persona.show',compact('persona'));
    }


    public function edit(Persona $persona)
    {
        return view('admin.persona.edit',compact('persona'));
    }


    public function update(UpdateRequest $request, Persona $persona)
    {
        try{
            $persona->update($request->all());
            \Log::info('Persona actualizada correctamente');
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al actualizar la persona');
        }
        return redirect()->route('personas.index')->with('actualizar','ok');
    }


    public function destroy(Persona $persona)
    {
        try{
            $persona->delete();
            \Log::info('Persona eliminada correctamente');
            return redirect()->route('personas.index')->with('eliminar','ok');

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar la persona');
            return redirect()->route('personas.index')->with('eliminar','error');

        }
    }
}
