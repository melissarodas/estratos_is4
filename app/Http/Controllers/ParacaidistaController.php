<?php

namespace App\Http\Controllers;

use App\Escuela;
use App\Paracaidista;
use App\Persona;
use Illuminate\Http\Request;
use App\Http\Requests\Paracaidista\StoreRequest;
use App\Http\Requests\Paracaidista\UpdateRequest;

class ParacaidistaController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:paracaidistas.index')->only('index');
         $this->middleware('can:paracaidistas.create')->only('create','store');
         $this->middleware('can:paracaidistas.edit')->only('edit','update');
         $this->middleware('can:paracaidistas.destroy')->only('destroy');
    }

    public function index()
    {
        $paracaidistas = Paracaidista::get();
        $personas = Persona::get();
        return view('admin.paracaidista.index',compact('personas','paracaidistas'));
    }


    public function create()
    {
        $personas = Persona::get();
        return view('admin.paracaidista.create',compact('personas'));
    }


    public function store(StoreRequest $request)
    {
        try{
            Paracaidista::create($request->all());
            \Log::info('Piloto creado correctamente');
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear el piloto');
        }
        return redirect()->route('paracaidistas.index')->with('guardar','ok');
    }


    public function show(Paracaidista $paracaidista)
    {
        return view('admin.paracaidista.show',compact('paracaidista'));
    }


    public function edit(Paracaidista $paracaidista)
    {
        $personas = Persona::get();
        return view('admin.paracaidista.edit',compact('paracaidista','personas'));
    }


    public function update(UpdateRequest $request, Paracaidista $paracaidista)
    {

        try{
            $paracaidista->update($request->all());
            \Log::info('Piloto actualizado correctamente');
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al actualizar el piloto');
        }
        return redirect()->route('paracaidistas.index')->with('actualizar','ok');
    }


    public function destroy(Paracaidista $paracaidista)
    {
        try{
            $paracaidista->delete();
            \Log::info('Piloto eliminado correctamente');
            return redirect()->route('paracaidistas.index')->with('eliminar','ok');

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar el piloto');
            return redirect()->route('paracaidistas.index')->with('eliminar','error');

        }
    }

    /*
        AJAX request
    */
    public function getParacaidistas(Request $request){

        $search = $request->search;

        if($search == ''){
            $paracaidistas = Paracaidista::orderby('licencia','asc')->select('id','licencia')->limit(5)->get();
        }else{
            $paracaidistas = Paracaidista::orderby('licencia','asc')->select('id','licencia')->where('licencia', 'like', '%' .$search . '%')->limit(5)->get();
        }

        $response = array();
        foreach($paracaidistas as $paracaidista){
        $response[] = array(
                "id"=>$paracaidista->id,
                "text"=>$paracaidista->licencia
        );
        }

        return response()->json($response);
    }
}
