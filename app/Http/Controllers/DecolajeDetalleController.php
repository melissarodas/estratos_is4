<?php

namespace App\Http\Controllers;

use App\DecolajeDetalle;
use Illuminate\Http\Request;

class DecolajeDetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DecolajeDetalle  $decolajeDetalle
     * @return \Illuminate\Http\Response
     */
    public function show(DecolajeDetalle $decolajeDetalle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DecolajeDetalle  $decolajeDetalle
     * @return \Illuminate\Http\Response
     */
    public function edit(DecolajeDetalle $decolajeDetalle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DecolajeDetalle  $decolajeDetalle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DecolajeDetalle $decolajeDetalle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DecolajeDetalle  $decolajeDetalle
     * @return \Illuminate\Http\Response
     */
    public function destroy(DecolajeDetalle $decolajeDetalle)
    {
        //
    }
}
