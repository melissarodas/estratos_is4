<?php

namespace App\Http\Controllers;

use App\Escuela;
use App\PropietariosEscuela;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Requests\Propietario\StoreRequest;

class PropietariosEscuelaController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:propietarios.index')->only('index');
         $this->middleware('can:propietarios.create')->only('create','store');
         $this->middleware('can:propietarios.edit')->only('edit','update');
         $this->middleware('can:propietarios.destroy')->only('destroy');
    }

    public function index(Request $request)
    {
        $escuelas = Escuela::get();
        $propietarios = PropietariosEscuela::latest()->get();

        if($request->ajax()){

            $data = \DB::table('propietarios_escuelas')
            ->join('escuelas','escuelas.id','=','propietarios_escuelas.escuela_id')
            ->select('propietarios_escuelas.id','escuelas.licencia as licencia')
            ->get();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Eliminar" title ="Eliminar" class="btn btn-danger btn-sm float-right mr-3 eliminarPropietario"> <i class="far fa-trash-alt"></i></a>';
                        $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Editar" title ="Editar" class="edit btn btn-primary btn-sm float-right mr-3 editarPropietario"><i class="far fa-edit"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            //LOGUEO DE ACCION
            \Log::info('Cargando datos al index de propietarios:');
            \Log::info($propietarios->toArray());

            return view('admin.propietario.index',compact('propietarios','escuelas'));



    }

    public function create()
    {
        //
    }

   
    public function store(StoreRequest $request)
    {
        try{
            $propietario = PropietariosEscuela::updateOrCreate(
                ['id'         => $request->propietario_id],
                ['escuela_id'   => $request->escuela_id]);

            //LOGUEO DE ACCION
            \Log::info('propietario creado correctamente:');
            \Log::info($propietario->toArray());

            return response()->json(['success'=>'El propietario se ha agregado correctamente.']);

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear el propietario');
            return redirect()->route('propietarios.index')->with('guardar','ok');

        }

    }

    
    public function show(PropietariosEscuela $propietariosEscuela)
    {
        //
    }

    
<<<<<<< HEAD
    public function edit($id )
    {        
        $propietario = PropietariosEscuela::find($id);
       //LOGUEO DE ACCION
       \Log::info('Obteniendo datos para la vista EDIT');
       \Log::info($propietario->toArray());
       return response()->json($propietario);

        
=======
    public function edit(PropietariosEscuela $propietariosEscuela)
    {        
        $escuelas = Escuela::get();
        return view('admin.propietario.edit',compact('escuelas','propietariosEscuela'));
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
    }

    
    public function update(Request $request, PropietariosEscuela $propietariosEscuela)
    {
<<<<<<< HEAD
       //
    }

    
    public function destroy($id)
    {
        try{
            PropietariosEscuela::find($id)->delete();
=======
        try{
            $propietariosEscuela->update($request->all());
            \Log::info('Propietario actualizado correctamente');
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al actualizar el propietario');
        }
        return redirect()->route('propietarios.index')->with('actualizar','ok');
    }

    
    public function destroy(PropietariosEscuela $propietariosEscuela)
    {
        try{
            $propietariosEscuela->delete();
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
            \Log::info('Propietario eliminado correctamente');
            return response()->json(['success'=>'El propietario ha sido eliminada correctamente.']);

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar el propietario');
            return redirect()->route('propietarios.index')->with('eliminar','error');
        }
    }
}
