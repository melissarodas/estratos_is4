<?php

namespace App\Http\Controllers;

use App\Escuela;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Requests\Escuela\StoreRequest;
use App\Http\Requests\Escuela\UpdateRequest;

class EscuelaController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:escuelas.index')->only('index');
         $this->middleware('can:escuelas.create')->only('create','store');
         $this->middleware('can:escuelas.edit')->only('edit','update');
         $this->middleware('can:escuelas.destroy')->only('destroy');
    }

    public function index(Request $request)
    {
        $escuelas = Escuela::latest()->get();
        if($request->ajax()){

            $data = Escuela::latest()->get();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Eliminar" title ="Eliminar" class="btn btn-danger btn-sm float-right mr-1 eliminarEscuela"> <i class="far fa-trash-alt"></i></a>';
                        $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Editar" title ="Editar" class="edit btn btn-primary btn-sm float-right mr-1 editarEscuela"><i class="far fa-edit"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        //LOGUEO DE ACCION
        \Log::info('Cargando datos al index de escuelas:');
        \Log::info($escuelas->toArray());

        return view('admin.escuela.index',compact('escuelas'));

    }


    public function create()
    {
        //
    }


    public function store(StoreRequest $request)
    {
        
        try{
            $escuela = Escuela::updateOrCreate(
                ['id'              => $request->escuela_id],
                ['licencia'        => $request->licencia,
                'estado'           => $request->estado,]);

            //LOGUEO DE ACCION
            \Log::info('Escuela creada correctamente:');
            \Log::info($escuela->toArray());
            
            return response()->json(['success'=>'La escuela se ha agregado correctamente.']);

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear la escuela');
            return redirect()->route('escuelas.index')->with('error','ok');
        }
    }


    public function show(Escuela $escuela)
    {
        //
    }


    public function edit($id)
    {
        $escuela = Escuela::find($id);

        //LOGUEO DE ACCION
        \Log::info('Obteniendo datos para la vista EDIT');
        \Log::info($escuela);
       return response()->json($escuela);    
    }


    public function update(UpdateRequest $request, Escuela $escuela)
    {
      //
    }


    public function destroy($id)
    {
        try{
            Escuela::find($id)->delete();
            
            //LOGUEO DE ACCION
            \Log::info('Registro de escuela eliminada correctamente');

            return response()->json(['success'=>'La escuela ha sido eliminada correctamente.']);
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar la escuela');
            return redirect()->route('escuelas.index')->with('eliminar','error');
        }
    }
}
