<?php

namespace App\Http\Controllers;

use App\Piloto;
use App\Persona;
use Carbon\Carbon;
use Illuminate\Http\Request;
<<<<<<< HEAD
use Yajra\DataTables\DataTables;
use App\Http\Requests\Piloto\StoreRequest;
use App\Http\Requests\Piloto\UpdateRequest;
=======
use App\Http\Requests\piloto\StoreRequest;
use App\Http\Requests\piloto\UpdateRequest;
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96

class PilotoController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:pilotos.index')->only('index');
         $this->middleware('can:pilotos.create')->only('create','store');
         $this->middleware('can:pilotos.edit')->only('edit','update');
         $this->middleware('can:pilotos.destroy')->only('destroy');
    }

    public function index()
    {
        $pilotos = Piloto::get();
        $personas = Persona::get();

        return view('admin.piloto.index',compact('pilotos','personas'));
    }


    public function create()
    {
        $personas = Persona::get();
        return view('admin.piloto.create', compact('personas'));
    }


    public function store(StoreRequest $request)
    {
        try{
            Piloto::create($request->all());
            \Log::info('Piloto creado correctamente');
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear el piloto');
        }
        return redirect()->route('pilotos.index')->with('guardar','ok');
    }


    public function show(Piloto $piloto)
    {
        return view('admin.piloto.show',compact('piloto'));
    }


    public function edit(Piloto $piloto)
    {
        $personas = Persona::get();
        return view('admin.piloto.edit',compact('piloto','personas'));
    }


    public function update(UpdateRequest $request, Piloto $piloto)
    {
        try{
            $piloto->update($request->all());
            \Log::info('Piloto actualizada correctamente');
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al actualizar el piloto');
        }
        return redirect()->route('pilotos.index')->with('actualizar','ok');
    }


    public function destroy(Piloto $piloto)
    {

        try{
            $piloto->delete();
            \Log::info('Piloto eliminada correctamente');
            return redirect()->route('pilotos.index')->with('eliminar','ok');

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar el piloto');
            return redirect()->route('pilotos.index')->with('eliminar','error');

        }
    }

    /*
        AJAX request
    */
    public function getPilotos(Request $request){

        $search = $request->search;

        if($search == ''){
            $pilotos = Piloto::orderby('licencia','asc')->select('id','licencia')->where('vencimiento','>=', Carbon::now())->limit(5)->get();
        }else{
            $pilotos = Piloto::orderby('licencia','asc')->select('id','licencia')->where('licencia', 'like', '%' .$search . '%')->where('vencimiento','>=', Carbon::now())->limit(5)->get();
        }

        $response = array();
        foreach($pilotos as $piloto){
        $response[] = array(
                "id"=>$piloto->id,
                "text"=>$piloto->licencia
        );
        }

        return response()->json($response);
    }
}
