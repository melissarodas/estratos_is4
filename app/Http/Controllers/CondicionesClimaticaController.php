<?php

namespace App\Http\Controllers;

use App\Decolaje;
use Illuminate\Http\Request;
use App\CondicionesClimatica;
use Yajra\DataTables\DataTables;

class CondicionesClimaticaController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:climaticas.index')->only('index');
         $this->middleware('can:climaticas.create')->only('create','store');
         $this->middleware('can:climaticas.edit')->only('edit','update');
         $this->middleware('can:climaticas.destroy')->only('destroy');
    }

    public function index(Request $request)
    {
        $decolajes = Decolaje::get();
        $climaticas = CondicionesClimatica::latest()->get();

        if($request->ajax()){

            $data = \DB::table('condiciones_climaticas')
            ->join('decolajes','decolajes.id','=','condiciones_climaticas.decolaje_id')
            ->join('eventos','eventos.id', '=','decolajes.evento_id')
            ->select('condiciones_climaticas.id','condiciones_climaticas.altitud','condiciones_climaticas.temperatura',
                     'condiciones_climaticas.direccion_viento', 'condiciones_climaticas.velocidad_viento','eventos.nombre as eventodecolaje')
            ->get();


            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Eliminar" title ="Eliminar" class="btn btn-danger btn-sm float-right mr-3 eliminarClimatica"> <i class="far fa-trash-alt"></i></a>';
                        $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Editar" title ="Editar" class="edit btn btn-primary btn-sm float-right mr-3 editarClimatica"><i class="far fa-edit"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
            //LOGUEO DE ACCION
            \Log::info('Cargando datos al index de condiciones climaticas:');
            //\Log::info($data->toArray());

        return view('admin.climatica.index',compact('decolajes','climaticas'));

    }

    public function create()
    {
       //
    }

   
    public function store(Request $request)
    {
       
        try{
            $clima = CondicionesClimatica::updateOrCreate(
                ['id'              => $request->clima_id],
                ['altitud'         => $request->altitud,
                'temperatura'      => $request->temperatura,
                'direccion_viento' => $request->direccion_viento,
                'velocidad_viento' => $request->velocidad_viento,
                'decolaje_id'      => $request->decolaje_id,
            ]);


            //LOGUEO DE ACCION
            \Log::info('Condicion cliamtica creada correctamente:');
            \Log::info($clima->toArray());
            
            return response()->json(['success'=>'La condicion climatica se ha agregado correctamente.']);

        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear la condicion climatica');
            return redirect()->route('climaticas.index')->with('error','ok');
        }
    }



   

   
    public function show(CondicionesClimatica $condicionesClimatica)
    {
        //
    }

   
    public function edit($id)
    {
        $climatica = CondicionesClimatica::find($id);
       //LOGUEO DE ACCION
       \Log::info('Obteniendo datos para la vista EDIT');
       \Log::info($climatica);
        return response()->json($climatica);    

    }

   
    public function update(Request $request, CondicionesClimatica $climatica)
    {
        $climatica->update($request->all());
        return redirect()->route('climaticas.index')->with('actualizar','ok');
    }

   
    public function destroy($id)
    {
        try{
            CondicionesClimatica::find($id)->delete();
            
            //LOGUEO DE ACCION
            \Log::info('Registro de condicion climatica eliminado correctamente');

            return response()->json(['success'=>'La condicion climatica ha sido eliminado correctamente.']);
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar la condicion climatica');
            return redirect()->route('climaticas.index')->with('eliminar','error');
        }
    }
}
