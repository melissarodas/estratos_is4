<?php

namespace App\Http\Controllers;

use App\TitulacionTipo;
use Illuminate\Http\Request;
use App\Http\Requests\TipoTitulacion\StoreRequest;
use App\Http\Requests\TipoTitulacion\UpdateRequest;

class TitulacionTipoController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:titulaciontipos.index')->only('index');
         $this->middleware('can:titulaciontipos.create')->only('create','store');
         $this->middleware('can:titulaciontipos.edit')->only('edit','update');
         $this->middleware('can:titulaciontipos.destroy')->only('destroy');
    }

    public function index()
    {
<<<<<<< HEAD
        $titulacionTipos = TitulacionTipo::latest()->get();
        if($request->ajax()){
            $data = TitulacionTipo::latest()->get();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Eliminar" title ="Eliminar" class="btn btn-danger btn-sm float-right mr-3 eliminarTipo"> <i class="far fa-trash-alt"></i></a>';
                        $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Editar" title ="Editar" class="edit btn btn-primary btn-sm float-right mr-3 editarTipo"><i class="far fa-edit"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        //LOGUEO DE ACCION
        \Log::info('Cargando datos al index de Tipos de titulaciones:');
        \Log::info($titulacionTipos->toArray());

=======
        $titulacionTipos = TitulacionTipo::get();
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
        return view('admin.tipoTitulacion.index',compact('titulacionTipos'));
    }

  
    public function create()
    {
<<<<<<< HEAD
      //
    }

    public function store(StoreRequest $request)
    {
        try{  
            $tipo = TitulacionTipo::updateOrCreate(['id' => $request->tipo_id],['nombre'   => $request->nombre]);
            //LOGUEO DE ACCION
            \Log::info('Tipo de titulacion creada correctamente:');
            \Log::info($tipo->toArray());

            return response()->json(['success'=>'El Tipo de titulación se ha agregado correctamenteee.']);
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear el tipo de titulacion');
            return redirect()->route('tipostitulaciones.index')->with('guardar','ok');

        }
=======
        return view('admin.tipoTitulacion.create');

    }

    
    public function store(StoreRequest $request)
    {
        TitulacionTipo::create($request->all());
        return redirect()->route('pruebas.index')->with('guardar','ok');
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
    }

   
    public function show(TitulacionTipo $titulacionTipo)
    {
        //
    }

    
    public function edit(TitulacionTipo $titulacionTipo)
    {
<<<<<<< HEAD
        $tipoTitulacion = TitulacionTipo::find($id);
         //LOGUEO DE ACCION
         \Log::info('Obteniendo datos para la vista EDIT');
         \Log::info($tipoTitulacion->toArray());
        return response()->json($tipoTitulacion);
=======
        return view('admin.tipoTitulacion.edit',compact('titulacionTipo'));

>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
    }

   
    public function update(UpdateRequest $request, TitulacionTipo $titulacionTipo)
    {
        $titulacionTipo->update($request->all());
        return redirect()->route('pruebas.index')->with('actualizar','ok');
    }

    
    public function destroy(TitulacionTipo $titulacionTipo)
    {
<<<<<<< HEAD
        try{
            TitulacionTipo::find($id)->delete();
             //LOGUEO DE ACCION
             \Log::info('Registro del tipo de titulacion eliminado correctamente');
            return response()->json(['success'=>'El Tipo de titulación ha sido eliminado correctamente.']);
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar el tipo de titulaciones');
            return redirect()->route('tipostitulaciones.index')->with('eliminar','error');
        }
=======
        $titulacionTipo->delete();
        return redirect()->route('pruebas.index')->with('eliminar','ok');
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
    }
}
