<?php

namespace App\Http\Controllers;

use App\Titulacion;
use App\Paracaidista;
use App\TitulacionTipo;
use Illuminate\Http\Request;
use App\Http\Requests\Titulacion\StoreRequest;
use App\Http\Requests\Titulacion\UpdateRequest;
use Yajra\DataTables\DataTables;

class TitulacionController extends Controller
{
    public function __construct()
    {
         $this->middleware('can:titulaciones.index')->only('index');
         $this->middleware('can:titulaciones.create')->only('create','store');
         $this->middleware('can:titulaciones.edit')->only('edit','update');
         $this->middleware('can:titulaciones.destroy')->only('destroy');
    }
    
    public function index(Request $request)
    {
        $tipos = TitulacionTipo::get();
        $paracaidistas = Paracaidista::get();
        $titulaciones = Titulacion::latest()->get();

        if($request->ajax()){

            $data = \DB::table('titulacions')
            ->join('paracaidistas','paracaidistas.id', '=', 'titulacions.paracaidista_id' )
            ->join('personas','personas.id','=','paracaidistas.persona_id')
            ->join('titulacion_tipos','titulacion_tipos.id','=','titulacions.tipo_id')
            ->select('titulacions.id','personas.nombre as paracaidista','titulacion_tipos.nombre as tipotitulacion','titulacions.licencia','titulacions.fecha_aprobacion','titulacions.documento')
            ->get();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Eliminar" title ="Eliminar" class="btn btn-danger btn-sm float-right mr-3 eliminarTipo"> <i class="far fa-trash-alt"></i></a>';
                        $btn = $btn.'<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Editar" title ="Editar" class="edit btn btn-primary btn-sm float-right mr-3 editarTipo"><i class="far fa-edit"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
                    
        }
            //LOGUEO DE ACCION
            \Log::info('Cargando datos al index de titulaciones:');
            \Log::info($titulaciones->toArray());

        return view('admin.titulacion.index',compact('titulaciones','paracaidistas','tipos'));
    }

    
    public function create()
    {
        //
    }

    
    public function store(StoreRequest $request)
    {
        try{
            $tipo = Titulacion::updateOrCreate(
                ['id' => $request->titulacion_id],
                ['tipo_id'          => $request->tipo_id,
                'paracaidista_id'   => $request->paracaidista_id,
                'licencia'          => $request->licencia,
                'fecha_aprobacion'  => $request->fecha_aprobacion,
                'documento'         => $request->documento]);

                //LOGUEO DE ACCION
                \Log::info('Titulacion creado correctamente:');
                \Log::info($tipo->toArray());
    
                return response()->json(['success'=>'La titulación se ha agregado correctamente.']);
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al crear la titulacion');
            return redirect()->route('titulaciones.index')->with('guardar','ok');

        }
    }

   
    public function show(Titulacion $titulacion)
    {
        //
    }

  
    public function edit($id)
    {
        $titulacione = Titulacion::find($id);
        //LOGUEO DE ACCION
        \Log::info('Obteniendo datos para la vista EDIT');
        \Log::info($titulacione->toArray());
        return response()->json($titulacione);
    }

   
    public function update(UpdateRequest $request, Titulacion $titulacione)
    {
        //
    }


    public function destroy($id)
    {
        try{
            Titulacion::find($id)->delete();
             //LOGUEO DE ACCION
             \Log::info('Registro de titulacion eliminado correctamente');
            return response()->json(['success'=>'La titulación ha sido eliminada correctamente.']);
        }catch (\Exception $e){
            \Log::critical($e->getMessage());
            \Log::info('Error al eliminar la titulacion');
            return redirect()->route('titulaciones.index')->with('eliminar','error');
        }
    }
}
