<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Decolaje extends Model
{
    //protected $fillable = ['fecha_despegue','fecha_aterrizaje','estado','piloto_id','evento_id','avioneta_id'];
    protected $fillable = ['fecha_despegue','fecha_aterrizaje','estado','evento_id','avioneta_id'];

    
    public function piloto()
    {
        return $this->belongsTo(Piloto::class);
    }

    public function evento()
    {
        return $this->belongsTo(Evento::class);
    }

    public function avioneta()
    {
        return $this->belongsTo(Avioneta::class);
    }

    public function decolajeDetalle()
    {
        return $this->hasMany(DecolajeDetalle::class);
    }

    public function decolajePiloto()
    {
        return $this->hasMany(DecolajePiloto::class);
    }
    
}
