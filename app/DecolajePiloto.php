<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DecolajePiloto extends Model
{
    protected $table = 'pilotos_decolajes';

    protected $fillable = ['piloto_id','decolaje_id'];

    
    public function piloto()
    {
        return $this->belongsTo(Piloto::class);
    }

}
