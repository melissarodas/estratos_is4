<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DecolajeDetalle extends Model
{
    protected $fillable = ['paracaidista_id'];

    public function paracaidista()
    {
        return $this->belongsTo(Paracaidista::class);
    }
}
