<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CondicionesClimatica extends Model
{
    protected $fillable = ['altitud','temperatura','direccion_viento','velocidad_viento','decolaje_id'];

    public function decolaje()
    {
        return $this->belongsTo(Decolaje::class);
    }

}
