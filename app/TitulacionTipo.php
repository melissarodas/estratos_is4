<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TitulacionTipo extends Model
{
    protected $fillable = ['nombre'];

}
