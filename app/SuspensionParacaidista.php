<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuspensionParacaidista extends Model
{
    protected $fillable = ['paracaidista_id', 'motivo','fecha','duracion'];

    
    public function paracaidista()
    {
        return $this->belongsTo(Paracaidista::class);
    }


}
