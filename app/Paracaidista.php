<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paracaidista extends Model
{
    protected $fillable = ['persona_id','licencia','categoria','cant_saltos','tamaño_velamen'];


    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }
}
