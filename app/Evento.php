<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $fillable = ['nombre', 'fecha','organizador_id','rta_id','zona_id'];

    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }

    public function zona()
    {
        return $this->belongsTo(Zona::class);
    }

    public function rta()
    {
        return $this->belongsTo(Persona::class);
    }
    public function organizador()
    {
        return $this->belongsTo(Persona::class);
    }

}
