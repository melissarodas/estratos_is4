<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avioneta extends Model
{
    protected $fillable = ['modelo','marca','matricula','capacidad'];

}
