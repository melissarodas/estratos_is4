<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropietariosEscuela extends Model
{
    protected $fillable = ['escuela_id'];

    public function escuela()
    {
        return $this->belongsTo(Escuela::class);
    }

}
