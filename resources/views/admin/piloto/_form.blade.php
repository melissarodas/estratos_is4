<div class="form-row">

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="licencia">Licencia</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="number" name="licencia" id="licencia" class="form-control" placeholder="" aria-describedby="helpId" >
            </div>
            @if ($errors->has('licencia'))
                <small class="form-text text-danger">{{ $errors->first('licencia')}}</small>
            @endif
        </div>
    </div>


    <div class="form-group col-md-4">
        <label for="vencimiento">Vencimiento de la licencia</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3"><i class="fa fa-calendar menu-icon"></i></span>
            </div>
            <input type="date" name="vencimiento" id="vencimiento" class="form-control"  value="<?php echo date("Y-m-d");?>" >
        </div>
    </div>


    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="persona_id">Asociar Persona</label>
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                </div>
                <select class="form-control" name="persona_id" id="persona_id">
                    <option value="" disabled selected>Selecccione una persona</option>
                    @foreach ($personas as $persona)
                    <option value="{{$persona->id}}">{{$persona->nombre}}</option>
                    @endforeach
                </select>
            </div>
            @if ($errors->has('persona_id'))
                <small class="form-text text-danger">{{ $errors->first('persona_id')}}</small>
            @endif
        </div>
    </div>

</div>
<div class="clearfix"></div>
