@extends('adminlte::page')
@section('title','Registro de Piloto')
@section('css')
@stop
@section('content_header')
@stop
@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Registrar de Piloto</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('pilotos.index')}}">Pilotos</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Registro de Piloto</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Nueva Piloto</h3>
                    </div>

                    {!! Form::open(['route'=>'pilotos.store', 'method'=>'POST']) !!}
                        <div class="card-body">
                            @include('admin.piloto._form')
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Registrar</button>
                            <a href="{{route('pilotos.index')}}" class="btn btn-light">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>

@stop
@section('js')
@stop
