@extends('adminlte::page')
@section('title', 'Pilotos')
@section('css')
<<<<<<< HEAD

<style>
    #pilotos tbody td:eq(0) {
        text-align: left;
    }
    #pilotos tbody td {
        text-align: center;
    }
</style>
=======
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Pilotos</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">
        <div class="card-header text-white bg-secondary" style="display: flex">
            <div class="col-sm-6">
                <h2>Listado de Pilotos</h2>
            </div>
            <div class="col-sm-6 text-right">
                @can('pilotos.create')
                    <a href="{{ route('pilotos.create') }}" class="nav-link">
                        <span class="btn btn-info">+ Registrar Piloto</span>
                    </a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table id="pilotos" class="table table-hover text-nowrap">
                        <thead>
                            <tr class="table-active">
                                <th style="text-align: center;">Id</th>
                                <th style="text-align: center;">Licencia</th>
                                <th style="text-align: center;">Vencimiento de licencia</th>
                                <th style="text-align: center;">Piloto</th>
                                <th style="text-align: center;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pilotos as $piloto)
                                <tr>
                                    <th scope="row">{{ $piloto->id }}</th>
                                    <td style="text-align: center;">{{ $piloto->licencia }}</td>
                                    <td style="text-align: center;">{{ $piloto->vencimiento }}</td>
                                    @foreach ($personas as $persona)
                                        @if ($piloto->persona_id == $persona->id)
                                            <td style="text-align: center;">{{ $persona->nombre }}</td>
                                        @endif
                                    @endforeach
                           
                                    <td style="text-align: center;">
                                        @csrf
                                        {!! Form::open(['route' => ['pilotos.destroy', $piloto], 'method' => 'DELETE', 'class' => 'formulario-eliminar']) !!}
                                        @can('pilotos.edit')
                                            <a class="btn btn-sm btn-warning" href="{{ route('pilotos.edit', $piloto) }}" title="Editar">
                                                <i class="far fa-edit"></i>
                                            </a>
                                        @endcan
                                        @can('pilotos.destroy')
                                            <button class="btn btn-sm btn-danger" type="submit" title="Eliminar">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                        @endcan
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')

<<<<<<< HEAD
<script type="text/javascript">

    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        var table = $('.data-table').DataTable({
            responsive:true,
            autoWidth: false,
=======
    <script>
        $(document).ready(function() {
            $('#pilotos').DataTable({
                responsive: true,
                autoWidth: false,
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
                "language": {
                    "lengthMenu": "Mostrar " +
                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="-1">All</option>
                                        </select>` +
                        " registros por página",
                    "zeroRecords": "No se encontró ningún registro.",
                    "info": "Mostrando la página  _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registro disponible.",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
        });
    </script>

    {{-- Recepcion de sessions para los estados de insertar actualizar y eliminar desplegando el sweeatalert --}}
    @if (session('guardar') == 'ok')
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido guardado existosamente.',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    @if (session('actualizar') == 'ok')
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido actualizado existosamente.',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    {{-- Rececpcion de status del metodo destroy para la confirmacion del alert de eliminado --}}
    @if (session('eliminar') == 'ok')
        <script>
            Swal.fire(
                'Eliminado!',
                'El registro ha sido eliminado correctamente.',
                'success'
            )
        </script>
    @endif
    @if (session('eliminar') == 'error')
    <script>
        Swal.fire(
            'Error!',
            'No se puede eliminar el registro por que cuenta con dependencias. Verifique.',
            'error'
        )
    </script>
    @endif

    <script>
        $('.formulario-eliminar').submit(function(e) {
            e.preventDefault();

            Swal.fire({
                title: 'Estas seguro?',
                text: "¡No podrás revertir esto!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, bórralo!'
            }).then((result) => {
                if (result.isConfirmed) {

                    this.submit();
                }
            })

        });
    </script>
@stop
