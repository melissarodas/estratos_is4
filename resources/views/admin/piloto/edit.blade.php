@extends('adminlte::page')
@section('title','Editar de Piloto')
@section('css')
@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Editar Piloto: <i>{{$piloto->nombre}}</i></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('pilotos.index')}}">Pilotos</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editar Piloto</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- TITULO DEL CARD -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Editar Piloto</h3>
                    </div>
                    {!! Form::model($piloto, ['route'=>['pilotos.update',$piloto], 'method'=>'PUT']) !!}
                        <div class="card-body">
                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="licencia">Licencia</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="licencia" id="licencia" class="form-control" placeholder="" aria-describedby="helpId" value="{{$piloto->licencia}}">
                                        </div>
                                        @if ($errors->has('licencia'))
                                            <small class="form-text text-danger">{{ $errors->first('licencia')}}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="vencimiento">Vencimiento de la licencia</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon3"><i class="fa fa-calendar menu-icon"></i></span>
                                        </div>
                                        <input type="date" name="vencimiento" id="vencimiento" class="form-control"  value="<?php echo date("Y-m-d");?>" >
                                    </div>
                                </div>


                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="persona_id">Piloto</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                                            </div>
                                            <select  class="form-control" name="persona_id" id="persona_id">
                                                <option value="" disabled selected>Selecccione una persona</option>
                                                @foreach ($personas as $persona)
                                                @if (old('id', $piloto->persona->id) == $persona->id)
                                                    <option value="{{ $persona->id }}" selected>
                                                        {{ $persona->nombre }}</option>
                                                @else
                                                    <option value="{{ $persona->id }}">{{ $persona->nombre }}
                                                    </option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                            <a href="{{route('pilotos.index')}}" class="btn btn-light">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
@stop
