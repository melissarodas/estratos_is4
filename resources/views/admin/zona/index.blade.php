@extends('adminlte::page')
@section('title', 'Zonas')
@section('css')
    <style>
        #zonas tbody td:eq(0) {
            text-align: left;
        }
        #zonas tbody td {
            text-align: center;
        }
    </style>
    
    <style type="text/css">
        /*se agranda el modal para poder cargar el map*/
        @media screen and (min-width: 1200px){
            .modal-large>.modal-dialog{
                width: 1200px;
            }
        }
    </style>
@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Zonas</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">
        <div class="card-header text-white bg-secondary" style="display: flex">
            <div class="col-sm-6">
                <h2>Listado de Zonas</h2>
            </div>
            <div class="col-sm-6 text-right">
                @can('zonas.create')
                <a class="btn btn-primary" href="javascript:void(0)" id="crearNuevoZona">+ Agregar</a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table id="zonas" class="table table-striped table-bordered table-hover data-table" style="width:100%">
                        <thead>
                            <tr class="table-active">
                                <th style="text-align: center;">Id</th>
                                <th style="text-align: center;">Nombre</th>
                                <th style="text-align: center;">Latitud</th>
                                <th style="text-align: center;">Longitud</th>
                                <th style="text-align: center;">Ciudad</th>
                                <th style="text-align: center;">Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal de registro y edición-->
    <div class="modal fade" id="agregar_zona_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content">
              
                <div class="modal-header">
                    <h4 class="modal-title col-11 text-center" id="modelHeading"></h4>  {{--titulo del modal --}}
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form id="agregar_zona_formulario" name="agregar_zona_formulario">
                    <input type="hidden" name="zona_id" id="zona_id"> {{--id del registro oculto para la edicion --}}
                    <div class="modal-body">
                        @csrf
                        @include('admin.zona._form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="btnGuardarZona" value="create">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')
  
    <script type="text/javascript">
    
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        
            var table = $('.data-table').DataTable({
                responsive:true,
                autoWidth: false,
                    "language": {
                        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Cargando...</span> ',
                        "lengthMenu": "Mostrar "+
                                            `<select class="custom-select custom-select-sm form-control form-control-sm">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="-1">All</option>
                                            </select>` +
                                            " registros por página",
                        "zeroRecords": "No se encontró ningún registro.",
                        "info": "Mostrando la página  _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registro disponible.",
                        "infoFiltered": "(filtrado de _MAX_ registros totales)",
                        "search": "Buscar:",
                        "paginate":{
                            "next": "Siguiente",
                            "previous" : "Anterior"
                        }
                    },
                processing: true,
                serverSide: true,
                ajax: "{{ route('zonas.index') }}",
                columns: [
                    {data: 'id'    , name: 'id'},
                    {data: 'nombre', name: 'nombre'},
                    {data: 'latitud', name: 'latitud'},
                    {data: 'longitud', name: 'longitud'},
                    {data: 'ciudad', name: 'ciudad'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],        
            
            });
    
            //Boton de crear nueva persona
            $('#crearNuevoZona').click(function () {
                $('#btnGuardarZona').val("create-zona");
                $('#zona_id').val('');
                $('#agregar_zona_formulario').trigger("reset");
                $('#modelHeading').html("Nueva Zona").css( "font-weight","bold" );
                $('#agregar_zona_modal').modal('show');
                
                $('.error-nombre').html('');
                $('.error-latitud').html('');
                $('.error-longitud').html('');
                $('.error-ciudad').html('');
   
            });
    
            // Boton de editar persona
            $('body').on('click', '.editarZona', function () {
                var zona_id = $(this).data('id');
                $.get("{{ route('zonas.index') }}" +'/' + zona_id +'/edit', function (data) {
                    $('#modelHeading').html("Editar Zona").css( "font-weight","bold" );
                    $('#btnGuardarZona').val("edit-zona");
                    $('#agregar_zona_modal').modal('show');
                    
                    $('#zona_id').val(data.id);
                    $('#nombre').val(data.nombre);
                    $('#latitud').val(data.latitud);
                    $('#longitud').val(data.longitud);
                    $('#ciudad').val(data.ciudad);

                    $('.error-nombre').html('');
                    $('.error-latitud').html('');
                    $('.error-longitud').html('');
                    $('.error-ciudad').html('');
                })
            });
    
            //Boton de guardar persona
            $('#btnGuardarZona').click(function (e) {
                e.preventDefault();
                $(this).html('Guardar');
    
                $.ajax({
                    data: $('#agregar_zona_formulario').serialize(),
                    url: "{{ route('zonas.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#agregar_zona_formulario').trigger("reset");
                        $('#agregar_zona_modal').modal('hide');
    
                        var operacion = $('#btnGuardarZona').val();
    
                        if( operacion == 'edit-zona'){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'La zona ha sido actualizada correctamente',
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }else{
                            Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: data.success,
                            showConfirmButton: false,
                            timer: 1500,
                        });
    
                        }
                        table.draw();
                    },
                    
                    error: function (data) {
                        console.log('Error:', data);
                        console.log('MENSAJE', data.responseJSON.errors);
                        if(data.responseJSON.errors.nombre != null)
                        {
                            $('.error-nombre').html(data.responseJSON.errors.nombre[0]);
                        }
                        if(data.responseJSON.errors.latitud != null)
                        {
                            $('.error-latitud').html(data.responseJSON.errors.latitud[0]);
                        }
                        if(data.responseJSON.errors.longitud != null)
                        {
                            $('.error-longitud').html(data.responseJSON.errors.longitud[0]);
                        }
                        if(data.responseJSON.errors.ciudad != null)
                        {
                            $('.error-ciudad').html(data.responseJSON.errors.ciudad[0]);
                        }
                       
    
                    }
                });
            });
    

    
            $('body').on('click', '.eliminarZona', function () {
                var zona_id = $(this).data("id");
                Swal.fire({
                        title: 'Estas seguro?',
                        text: "¡No podrás revertir esta operación!!!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: '¡Sí, continuar!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                type: "DELETE",
                                url: "{{ route('zonas.store') }}"+'/'+zona_id,
                                success: function (data) {
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: data.success,
                                        showConfirmButton: false,
                                        timer: 1500,
                                    }
    
                                    );
                                    table.draw();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
    
    
                        }
                    });
    
                });
    
            });
            
    </script>
    <script type="text/javascript">
        function initMap() {
            var mapProp= {
                center:new google.maps.LatLng(-25.241668211110042,-57.51411437988281),
                zoom:14,
            };
            var map = new google.maps.Map(document.getElementById("map"),mapProp);
            var infoWindow = new google.maps.InfoWindow;
    
            var marker = new google.maps.Marker({
                position: google.maps.LatLng(-25.241668211110042,-57.51411437988281), 
                map: map,
                draggable: true,
            });
    
            google.maps.event.addListener(map, 'click', function(event) {
                $("input[type='text'][name='latitud']").val(event.latLng.lat());
                $("input[type='text'][name='longitud']").val(event.latLng.lng());
                marker.setPosition(event.latLng);
    
            });
    
            google.maps.event.addListener(marker, 'drag', function(event) {
                $("input[type='text'][name='latitud']").val(event.latLng.lat());
                $("input[type='text'][name='longitud']").val(event.latLng.lng());
            });
    
            $(document).on('keypress','#latitud',function(e){
                if(e.which == 13) {
                    if($(this).val() !== ''){
                        $('#longitud').focus();
                    }
                }   
            });
    
            $(document).on('keyup','#longitud',function(e){
                if($(this).val() !== ''){
                    marker.setPosition(new google.maps.LatLng($("#latitud").val(), $("#longitud").val()));
                    newLocation($("#latitud").val(),$("#longitud").val());
                }
            });
    
            function newLocation(newLat,newLng) {
                map.setCenter({
                    lat : parseFloat(newLat),
                    lng : parseFloat(newLng)
                });
            }
    
            // $(document).on('select2:select','#barrio_id',function(){
            //     var request = {
            //         query: $('#barrio_id > option:selected').text()+', '+$('#ciudad_id > option:selected').text()+', '+$('#departamento_id > option:selected').text(),
            //         fields: ['name', 'geometry'],
            //     };
    
            //     service = new google.maps.places.PlacesService(map);
    
            //     service.findPlaceFromQuery(request, function(results, status) {
            //         if (status === google.maps.places.PlacesServiceStatus.OK) {
            //             for (var i = 0; i < results.length; i++) {
            //                 marker.setPosition(results[i].geometry.location);
            //             }
            //             map.setCenter(results[0].geometry.location);
            //         }
            //     });
            // })
        }
    </script>
    
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAf5MejWxz8Z42K9qKDTtvuseVxQdGC0FQ&libraries=places&callback=initMap&language=es">
    </script>
   
@stop
