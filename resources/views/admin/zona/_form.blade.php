<div class="form-row">

    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="nombre" id="nombre" class="form-control" placeholder=""
                    aria-describedby="helpId"/>
            </div>
            <small class="form-text text-danger error-nombre"></small>     

        </div>
    </div>
     
    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="ciudad">Ciudad</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="ciudad" id="ciudad" class="form-control" placeholder=""aria-describedby="helpId"/>
            </div>
            <small class="form-text text-danger error-ciudad"></small>     

        </div>
    </div>

</div>

<div class="clearfix"></div>

<div class="form-row">

    <label>Ubicación. <small><i class="fto-help-circled mar_r4 fs_lg"></i> Hace clic sobre el mapa para definir la ubicacion exacta.</small></label>
    <div id="map" style="width:100%;height:270px"></div>

    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="latitud">Latitud</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-map menu-icon"></i></span>
                </div>
                <input type="text" name="latitud" id="latitud" class="form-control" placeholder="" aria-describedby="helpId">
            </div>
            <small class="form-text text-danger error-latitud"></small>     

        </div>
    </div>

    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="longitud">Longitud</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-map menu-icon"></i></span>
                </div>
                <input type="text" name="longitud" id="longitud" class="form-control" placeholder="" aria-describedby="helpId">
            </div>
            <small class="form-text text-danger error-longitud"></small>     

        </div>
    </div>

</div>
<div class="clearfix"></div>
