@extends('adminlte::page')
@section('title','Registro de Usuario')
@section('css')
@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Registrar Usuario</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Configuración</a></li>
                    <li class="breadcrumb-item"><a href="{{route('users.index')}}">Usuarios</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Registrar Usuarios</li>
                </ol>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-sm-12">
                @if ($errors->any())
                    <x-adminlte-alert class="text-uppercase" theme="danger" title="Atención!!" dismissable>
                        <ul>
                            @foreach ( $errors->all() as $error )
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </x-adminlte-alert>
                @endif
            </div>

        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Nuevo Usuario</h3>
                    </div>

                    {!! Form::open(['route'=>'users.store', 'method'=>'POST', 'enctype'=>"multipart/form-data"]) !!}
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="name">Nombre</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-user menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="name" id="name"  class="form-control" placeholder="" aria-describedby="helpId">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="email">Correo electrónico</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa fa-address-book menu-icon"></i></span>
                                            </div>
                                            <input type="email" name="email" id="email"  class="form-control" placeholder="" aria-describedby="helpId">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="password">Contraseña</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-eye-slash menu-icon"></i></span>
                                            </div>
                                            <input type="password" name="password" id="password"  class="form-control" placeholder="" aria-describedby="helpId">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <div class="card-body">
                                            <label for="foto">Imágen del usuario</label>
                                            <div class="input-group">
                                                <input type="file" class="form-control" name="foto" id="foto" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            @include('admin.user._form')
                        </div>
                        <div class="card-footer text-muted">
                            <button type="submit" class="btn btn-primary mr-2">Registrar</button>
                            <a href="{{route('users.index')}}" class="btn btn-light">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
@stop
