@extends('adminlte::page')
@section('title','Gestión de usuarios del sistema')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
@stop
@section('content_header')
@stop
@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">

            <div class="col-sm-12 float-right">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Configuración</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<div class="card">
    <div class="card-header text-white bg-secondary" style="display: flex">
        <div class="col-sm-6">
            <h2>Listado de Usuarios</h2>
        </div>
        <div class="col-sm-6 text-right">
                                        @can('usuarios.create')

            <a href="{{route('users.create')}}" class="nav-link">
                <span class="btn btn-info">+ Crear nuevo Usuario</span>
            </a>
                                                    @endcan

        </div>
    </div>
    <div class="card-body">
        <div class="card">
            <div class="card-body">
                <table id="bancos" class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Imágen</th>
                                    <th>Nombre</th>
                                    <th>Correo electrónico</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <th scope="row">{{$user->id}}</th>
                                    <td><img class="img-thumbnail img-fluid" src="{{ asset('/imagen/usuarios') . '/' . $user->imagen }}" width="150" height="150"  alt=""></td>
                                    <td>{{$user->name}}
                                    </td>
                                    <td>{{$user->email}}</td>
                                    <td style="width: 50px;">
                                        {!! Form::open(['route'=>['users.destroy',$user], 'method'=>'DELETE', 'class' => 'formulario-eliminar']) !!}
                                        @can('usuarios.edit')

                                        <a class="btn btn-warning" href="{{route('users.edit', $user)}}" title="Editar">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        @endcan
                                        @can('usuarios.destroy')

                                        <button class="btn btn-danger" type="submit" title="Eliminar">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                        @endcan

                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@stop
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>

<script>
    $(document).ready(function(){
        $('#bancos').DataTable({
            responsive:true,
            autoWidth: false,
            "language": {
                "lengthMenu": "Mostrar "+
                                    `<select class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="-1">All</option>
                                    </select>` +
                                    " registros por página",
                "zeroRecords": "No se encontró ningún registro.",
                "info": "Mostrando la página  _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registro disponible.",
                "infoFiltered": "(filtrado de _MAX_ registros totales)",
                "search": "Buscar:",
                "paginate":{
                    "next": "Siguiente",
                    "previous" : "Anterior"
                }
            }
        });
    });
</script>

{{-- Recepcion de sessions para los estados de insertar actualizar y eliminar desplegando el sweeatalert --}}
@if (session('guardar') == 'ok')
<script>
    Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'El registro ha sido guardado existosamente.',
            showConfirmButton: false,
            timer: 1500
            })
</script>
@endif
@if (session('actualizar') == 'ok')
<script>
    Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'El registro ha sido actualizado existosamente.',
            showConfirmButton: false,
            timer: 1500
            })
</script>
@endif
{{-- Rececpcion de status del metodo destroy para la confirmacion del alert de eliminado --}}
@if (session('eliminar') == 'ok')
    <script>
        Swal.fire(
                'Eliminado!',
                'El registro ha sido eliminado correctamente.',
                'success'
        )
    </script>
@endif
<script>
     $('.formulario-eliminar').submit(function(e){
        e.preventDefault();

       Swal.fire({
        title: 'Estas seguro?',
        text: "¡No podrás revertir esto!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, bórralo!'
        }).then((result) => {
        if (result.isConfirmed) {

            this.submit();
        }
        })

     });
</script>
@stop
