@extends('adminlte::page')
@section('title','Editar de Usuario')
@section('css')
@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Editar usuario: <i>{{$user->name}}</i></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Archivos</a></li>
                    <li class="breadcrumb-item"><a href="{{route('users.index')}}">Usuarios</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editar Usuario</li>
                </ol>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-sm-12">
                @if ($errors->any())
                    <x-adminlte-alert class="text-uppercase" theme="danger" title="Atención!!" dismissable>
                        <ul>
                            @foreach ( $errors->all() as $error )
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </x-adminlte-alert>
                @endif
            </div>
        </div>
    </div>
</section>

@if (session('info'))
    <div class="alert alert-success">
        <strong>{{ session('info') }}</strong>
    </div>

@endif

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- TITULO DEL CARD -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Editar Usuario</h3>
                    </div>
                    {!! Form::model($user, ['route'=>['users.update',$user], 'method'=>'PUT', 'enctype'=>"multipart/form-data"]) !!}
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="name">Nombre</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-user menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="name" id="name" value="{{$user->name}}" class="form-control" placeholder="" aria-describedby="helpId">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="email">Correo electrónico</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa fa-address-book menu-icon"></i></span>
                                            </div>
                                            <input type="email" name="email" id="email" value="{{$user->email}}" class="form-control" placeholder="" aria-describedby="helpId">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title d-flex">Imágen del usuario</h4>
                                <input type="file" name="picture" id="picture"/>
                            </div>
                            @include('admin.user._form')

                            <div class="clearfix"></div>
                        </div>
                        <div class="card-footer text-muted">
                            <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                            <a href="{{route('users.index')}}" class="btn btn-light">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
@if (session('info') == 'ok')
<script>
    Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'El registro ha sido guardado existosamente.',
            showConfirmButton: false,
            timer: 1500
            })
</script>
@endif


@stop



