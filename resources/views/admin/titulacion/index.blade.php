@extends('adminlte::page')
@section('title', 'Titulaciones')
@section('css')

    <style>
        #titulaciones tbody td:eq(0) {
            text-align: left;
        }
        #titulaciones tbody td {
            text-align: center;
        }
    </style>
@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Titulaciones</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">
        <div class="card-header text-white bg-secondary" style="display: flex">
            <div class="col-sm-6">
                <h2>Listado de Titulaciones</h2>
            </div>
            <div class="col-sm-6 text-right">
                @can('titulaciones.create')
                    <a class="btn btn-info" href="javascript:void(0)" id="crearNuevoTitulacion">+Agregar</a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table id="titulaciones" class="table table-striped table-bordered table-hover data-table" style="width:100%">
                        <thead>
                            <tr class="table-active">
                                <th style="text-align: center;">Id</th>
                                <th style="text-align: center;">Tipo</th>
                                <th style="text-align: center;">Paracaidista</th>
                                <th style="text-align: center;">licencia</th>
                                <th style="text-align: center;">Fecha de aprobación</th>
                                <th style="text-align: center;">Documento</th>
                                <th style="text-align: center;">Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>


     <!-- Modal de registro y edición-->
     <div class="modal fade" id="agregar_titulacion_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content">
              
                <div class="modal-header">
                    <h4 class="modal-title col-11 text-center" id="modelHeading"></h4>  {{--titulo del modal --}}
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form id="agregar_titulacion_formulario" name="agregar_titulacion_formulario">
                    <input type="hidden" name="titulacion_id" id="titulacion_id"> {{--id del registro oculto para la edicion --}}
                    <div class="modal-body">
                        @csrf
                        @include('admin.titulacion._form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="btnGuardarTitulacion" value="create">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')

    <script type="text/javascript">

        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        
            var table = $('.data-table').DataTable({
                responsive:true,
                autoWidth: false,
                    "language": {
                        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Cargando...</span> ',
                        "lengthMenu": "Mostrar "+
                                            `<select class="custom-select custom-select-sm form-control form-control-sm">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="-1">All</option>
                                            </select>` +
                                            " registros por página",
                        "zeroRecords": "No se encontró ningún registro.",
                        "info": "Mostrando la página  _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registro disponible.",
                        "infoFiltered": "(filtrado de _MAX_ registros totales)",
                        "search": "Buscar:",
                        "paginate":{
                            "next": "Siguiente",
                            "previous" : "Anterior"
                        }
                    },
                processing: true,
                serverSide: true,
                ajax: "{{ route('titulaciones.index') }}",
                columns: [
                    {data: 'id'    , name: 'id'},
                    {data: 'tipotitulacion', name: 'tipotitulacion'},
                    {data: 'paracaidista', name: 'paracaidista'},
                    {data: 'licencia', name: 'licencia'},
                    {data: 'fecha_aprobacion', name: 'fecha_aprobacion'},
                    {data: 'documento', name: 'documento'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],        
            
            });

            //Boton de crear nuevo tipo
            $('#crearNuevoTitulacion').click(function () {
                $('#btnGuardarTitulacion').val("create-tipo");
                $('#titulacion_id').val('');
                $('#agregar_titulacion_formulario').trigger("reset");
                $('#modelHeading').html("Nueva titulación").css( "font-weight","bold" );
                $('#agregar_titulacion_modal').modal('show');
                
                $('.error-tipo-id').html('');
                $('.error-paracaidista-id').html('');
                $('.error-licencia').html('');
                $('.error-fecha-aprobacion').html('');
                $('.error-documento').html('');

            });

            // Boton de editar el tipo
            $('body').on('click', '.editarTipo', function () {
                var titulacion_id = $(this).data('id');
                $.get("{{ route('titulaciones.index') }}" +'/' + titulacion_id +'/edit', function (data) {
                    $('#modelHeading').html("Editar Tipo de titulación").css( "font-weight","bold" );
                    $('#btnGuardarTitulacion').val("edit-tipo");
                    $('#agregar_titulacion_modal').modal('show');
                    
                    $('#titulacion_id').val(data.id);
                    $('#tipo_id').val(data.tipo_id);
                    $('#paracaidista_id').val(data.paracaidista_id);
                    $('#licencia').val(data.licencia);
                    $('#fecha_aprobacion').val(data.fecha_aprobacion);
                    $('#documento').val(data.documento);

                    $('.error-tipo-id').html('');
                    $('.error-paracaidista-id').html('');
                    $('.error-licencia').html('');
                    $('.error-fecha-aprobacion').html('');
                    $('.error-documento').html('');

                })
            });

            //Boton de guardar tipo
            $('#btnGuardarTitulacion').click(function (e) {
                e.preventDefault();
                $(this).html('Guardar');

                $.ajax({
                    data: $('#agregar_titulacion_formulario').serialize(),
                    url: "{{ route('titulaciones.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#agregar_titulacion_formulario').trigger("reset");
                        $('#agregar_titulacion_modal').modal('hide');

                        var operacion = $('#btnGuardarTitulacion').val();

                        if( operacion == 'edit-tipo'){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'La titulación ha sido actualizada correctamente',
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }else{
                            Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: data.success,
                            showConfirmButton: false,
                            timer: 1500,
                        });

                        }
                        table.draw();
                    },
                    
                    error: function (data) {
                        console.log('Error:', data);
                        console.log('MENSAJE', data.responseJSON.errors);
                        if(data.responseJSON.errors.tipo_id != null)
                        {
                            $('.error-tipo-id').html(data.responseJSON.errors.tipo_id[0]);
                        }
                        if(data.responseJSON.errors.paracaidista_id != null)
                        {
                            $('.error-paracaidista-id').html(data.responseJSON.errors.paracaidista_id[0]);
                        }
                        if(data.responseJSON.errors.licencia != null)
                        {
                            $('.error-licencia').html(data.responseJSON.errors.licencia[0]);
                        }
                        if(data.responseJSON.errors.fecha_aprobacion != null)
                        {
                            $('.error-fecha-aprobacion').html(data.responseJSON.errors.fecha_aprobacion[0]);
                        }
                        if(data.responseJSON.errors.documento != null)
                        {
                            $('.error-documento').html(data.responseJSON.errors.documento[0]);
                        }

                    }
                });
            });

            $('body').on('click', '.eliminarTipo', function () {
                var titulacion_id = $(this).data("id");
                Swal.fire({
                        title: 'Estas seguro?',
                        text: "¡No podrás revertir esta operación!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: '¡Sí, continuar!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                type: "DELETE",
                                url: "{{ route('titulaciones.store') }}"+'/'+titulacion_id,
                                success: function (data) {
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: data.success,
                                        showConfirmButton: false,
                                        timer: 1500,
                                    }

                                    );
                                    table.draw();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });


                        }
                    });

                });

            });
            
    </script>
@stop
