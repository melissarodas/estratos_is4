<div class="form-row">

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="cedula">Cédula</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="cedula" id="cedula" class="form-control" placeholder="C.I." aria-describedby="helpId">
            </div>
            @if ($errors->has('cedula'))
                <small class="form-text text-danger">{{ $errors->first('cedula')}}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre" aria-describedby="helpId">
            </div>
            @if ($errors->has('nombre'))
                <small class="form-text text-danger">{{ $errors->first('nombre')}}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-4">
        <label for="fecha_nacimiento">Fecha de nacimiento</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3"><i class="fa fa-calendar menu-icon"></i></span>
            </div>
            <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control"  value="<?php echo date("Y-m-d");?>" >
        </div>
    </div>

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="correo">Correo electrónico</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa fa-address-book menu-icon"></i></span>
                </div>
                <input type="email" name="correo" id="correo"  class="form-control" placeholder="Email" aria-describedby="helpId">
            </div>
            @if ($errors->has('correo'))
                <small class="form-text text-danger">{{ $errors->first('correo')}}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="nacionalidad">Nacionalidad</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="nacionalidad" id="nacionalidad" class="form-control" placeholder="Ej. Paraguaya" aria-describedby="helpId">
            </div>
            @if ($errors->has('nacionalidad'))
                <small class="form-text text-danger">{{ $errors->first('nacionalidad')}}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="telefono">Teléfono</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="telefono" id="telefono" class="form-control" placeholder="Nro de Teléfono" aria-describedby="helpId">
            </div>
            @if ($errors->has('telefono'))
                <small class="form-text text-danger">{{ $errors->first('telefono')}}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="direccion">Dirección</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Dirección de la persona" aria-describedby="helpId">
            </div>
            @if ($errors->has('direccion'))
                <small class="form-text text-danger">{{ $errors->first('direccion')}}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="tipo_persona">Tipo de persona</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                {!! Form::select('tipo_persona', ['PERSONA FISICA' => 'PERSONA FISICA', 'PERSONA JURIDICA' => 'PERSONA JURIDICA'],'PERSONA FISICA', ['class' => 'form-control'] ) !!}
            </div>
            @if ($errors->has('tipo_persona'))
                <small class="form-text text-danger">{{ $errors->first('tipo_persona')}}</small>
            @endif
        </div>
    </div>

</div>
<div class="clearfix"></div>
