@extends('adminlte::page')
@section('title', 'Personas')
@section('css')
<<<<<<< HEAD
=======
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96

@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Personas</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">
        <div class="card-header text-white bg-secondary" style="display: flex">
            <div class="col-sm-6">
                <h2>Listado de Personas</h2>
            </div>
            <div class="col-sm-6 text-right">
                @can('personas.create')
                    <a href="{{ route('personas.create') }}" class="nav-link">
                        <span class="btn btn-info">+ Registrar persona</span>
                    </a>
                @endcan
            </div>
        </div>
        <div class="card-body">
        
                    <table id="personas" class="table table-striped table-bordered dt-responsive" style="width:100%">
                        <thead>
                            <tr class="table-active">
                                <th>Id</th>
                                <th style="text-align: center;">Cédula</th>
                                <th style="text-align: center;">Nombre</th>
                                <th style="text-align: center;">Teléfono</th>
                                <th style="text-align: center;">Correo</th>
                                <th style="text-align: center;">Acciones</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($personas as $persona)
                                <tr>
                                    <th scope="row">{{ $persona->id }}</th>
                                    <td style="text-align: center;" >{{ $persona->cedula }}</td>
                                    <td style="text-align: center;" >{{ $persona->nombre }}</td>
                                    <td style="text-align: center;" >{{ $persona->telefono }}</td>
                                    <td style="text-align: center;" >{{ $persona->correo }}</td>
                                    <td style="text-align: center;">
                                        @csrf
                                        {!! Form::open(['route' => ['personas.destroy', $persona], 'method' => 'DELETE', 'class' => 'formulario-eliminar']) !!}
                                        @can('personas.show')
                                            <a class="btn btn-sm btn-info" href="{{ route('personas.show', $persona) }}" title="Ver">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        @endcan
                                        @can('personas.edit')
                                            <a class="btn btn-sm btn-warning" href="{{ route('personas.edit', $persona) }}" title="Editar">
                                                <i class="far fa-edit"></i>
                                            </a>
                                        @endcan
                                        @can('personas.destroy')
                                            <button class="btn btn-sm btn-danger" type="submit" title="Eliminar">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                        @endcan
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            
        </div>
    </div>
@stop
@section('js')
<<<<<<< HEAD

<script type="text/javascript">

    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        var table = $('.data-table').DataTable({
            responsive:true,
            autoWidth: false,
=======
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#personas').DataTable({
                responsive: true,
                autoWidth: false,
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
                "language": {
                    "lengthMenu": "Mostrar " +
                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="-1">All</option>
                                        </select>` +
                        " registros por página",
                    "zeroRecords": "No se encontró ningún registro.",
                    "info": "Mostrando la página  _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registro disponible.",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
        });
    </script>

    <script>
        $('.formulario-eliminar').submit(function(e) {
            e.preventDefault();

            Swal.fire({
                title: 'Estas seguro?',
                text: "¡No podrás revertir esto!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, bórralo!'
            }).then((result) => {
                if (result.isConfirmed) {

                    this.submit();
                }
            })

        });
    </script>
    @if (session('guardar') == 'ok')
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido guardado existosamente.',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    @if (session('actualizar') == 'ok')
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido actualizado existosamente.',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    @if (session('eliminar') == 'ok')
        <script>
            Swal.fire(
                'Eliminado!',
                'El registro ha sido eliminado correctamente.',
                'success'
            )
        </script>
    @endif
    @if (session('eliminar') == 'error')
        <script>
            Swal.fire(
                'Error!',
                'No se puede eliminar el registro por que cuenta con dependencias. Verifique.',
                'error'
            )
        </script>
    @endif

@stop
