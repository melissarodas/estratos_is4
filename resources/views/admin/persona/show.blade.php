@extends('adminlte::page')
@section('title','información de la persona')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">

@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">

            <div class="col-sm-12 float-right">
               
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('personas.index')}}">Personas</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Información</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<div class="card">
    <div class="card-header text-white bg-secondary" style="display: flex">
        <div class="col-sm-6">
            <h3 class="page-title">Información de la persona</h3>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12 pl-lg-5">
                <div class="profile-feed">
                    <div class="d-flex align-items-start profile-feed-item">

                        <div class="form-group col-md-6">
                            <strong><i class="fas fa-address-card mr-1"></i> Numero de cedula</strong>
                            <p class="text-muted">
                                {{$persona->cedula}}
                            </p>
                            <hr>
                            <strong><i class="fab fa-product-hunt mr-1"></i> Nombre</strong>
                            <p class="text-muted">
                                {{$persona->nombre}}
                            </p>
                            <hr>
                            <strong><i class="fas fa-calendar mr-1"></i> Fecha de nacimiento</strong>
                            <p class="text-muted">
                                {{$persona->fecha_nacimiento}}
                            </p>
                            <hr>
                            <strong><i class="fas fa-flag mr-1"></i> Nacionalidad</strong>
                            <p class="text-muted">
                                {{$persona->nacionalidad}}
                            </p>
                            <hr>
                        </div>
                        <div class="form-group col-md-6">
                            <strong>
                                <i class="fas fa-mobile mr-1"></i>
                                Teléfono</strong>
                            <p class="text-muted">
                                {{$persona->telefono}}
                            </p>
                            <hr>
                            <strong><i class="fas fa-envelope mr-1"></i> Correo</strong>
                            <p class="text-muted">
                                {{$persona->correo}}

                            </p>
                            <hr>
                            <strong><i class="fas fa-map-marked-alt mr-1"></i> Dirección</strong>
                            <p class="text-muted">
                                {{$persona->direccion}}
                            </p>
                            <hr>
                            <strong><i class="fas fa-user-friends mr-1"></i> Tipo de persona</strong>
                            <p class="text-muted">
                                {{$persona->tipo_persona}}
                            </p>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer text-muted">
        <a href="{{route('personas.index')}}" class="btn btn-primary float-right">Regresar</a>
    </div>
</div>
@stop
@section('js')  

