@extends('adminlte::page')
@section('title','Editar de Persona')
@section('css')
@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Editar Persona: <i>{{$persona->nombre}}</i></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('personas.index')}}">Personas</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editar Persona</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- TITULO DEL CARD -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Editar Persona</h3>
                    </div>
                    {!! Form::model($persona, ['route'=>['personas.update',$persona], 'method'=>'PUT']) !!}
                        <div class="card-body">

                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="cedula">Cédula</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="cedula" id="cedula" class="form-control" placeholder="" aria-describedby="helpId" value="{{$persona->cedula}}">
                                        </div>
                                        @if ($errors->has('cedula'))
                                            <small class="form-text text-danger">{{ $errors->first('cedula')}}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="nombre">Nombre</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="" aria-describedby="helpId" value="{{$persona->nombre}}" >
                                        </div>
                                        @if ($errors->has('nombre'))
                                            <small class="form-text text-danger">{{ $errors->first('nombre')}}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="fecha_nacimiento">Fecha de nacimiento</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon3"><i class="fa fa-calendar menu-icon"></i></span>
                                        </div>
                                        <input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control"  value="{{$persona->fecha_nacimiento}}" >
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="correo">Correo electrónico</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa fa-address-book menu-icon"></i></span>
                                            </div>
                                            <input type="email" name="correo" id="correo"  class="form-control" placeholder="" aria-describedby="helpId"  value="{{$persona->correo}}">
                                        </div>
                                        @if ($errors->has('correo'))
                                            <small class="form-text text-danger">{{ $errors->first('correo')}}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="nacionalidad">Nacionalidad</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="nacionalidad" id="nacionalidad" class="form-control" placeholder="" aria-describedby="helpId"  value="{{$persona->nacionalidad}}">
                                        </div>
                                        @if ($errors->has('nacionalidad'))
                                            <small class="form-text text-danger">{{ $errors->first('nacionalidad')}}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="telefono">Teléfono</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="telefono" id="telefono" class="form-control" placeholder="" aria-describedby="helpId" value="{{$persona->telefono}}">
                                        </div>
                                        @if ($errors->has('telefono'))
                                            <small class="form-text text-danger">{{ $errors->first('telefono')}}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="direccion">Dirección</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="direccion" id="direccion" class="form-control" placeholder="" aria-describedby="helpId"  value="{{$persona->direccion}}">
                                        </div>
                                        @if ($errors->has('direccion'))
                                            <small class="form-text text-danger">{{ $errors->first('direccion')}}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="tipo_persona">Tipo de persona</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                                            </div>
                                            {!! Form::select('tipo_persona', ['PERSONA FISICA' => 'PERSONA FISICA', 'PERSONA JURIDICA' => 'PERSONA JURIDICA'],$persona->tipo_persona, ['class' => 'form-control'] ) !!}
                                        </div>
                                        @if ($errors->has('tipo_persona'))
                                            <small class="form-text text-danger">{{ $errors->first('tipo_persona')}}</small>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                            <a href="{{route('personas.index')}}" class="btn btn-light">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
@stop
