<div class="form-row">

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="persona_id">Asociar Persona</label>
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                </div>
                <select class="form-control" name="persona_id" id="persona_id">
                    <option value="" disabled selected>Selecccione una persona</option>
                    @foreach ($personas as $persona)
                    <option value="{{$persona->id}}">{{$persona->nombre}}</option>
                    @endforeach
                </select>
            </div>
            @if ($errors->has('persona_id'))
                <small class="form-text text-danger">{{ $errors->first('persona_id')}}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="licencia">Licencia</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="licencia" id="licencia" class="form-control" placeholder=""
                    aria-describedby="helpId"/>
            </div>
            @if ($errors->has('licencia'))
                <small class="form-text text-danger">{{ $errors->first('licencia')}}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="categoria">Categoria</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                </div>
                {!! Form::select('categoria', ['A' => 'A', 'AI' => 'AI', 'B' => 'B', 'C' => 'C'],'A', ['class' => 'form-control'] ) !!}
            </div>
            @if ($errors->has('categoria'))
                <small class="form-text text-danger">{{ $errors->first('categoria')}}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="cant_saltos">Cantidad de saltos</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="cant_saltos" id="cant_saltos" class="form-control"
                    placeholder="" aria-describedby="helpId" />
            </div>
            @if ($errors->has('cant_saltos'))
                <small class="form-text text-danger">{{ $errors->first('cant_saltos')}}</small>
            @endif
        </div>
    </div>

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="tamaño_velamen">Tamaño de velamen</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="tamaño_velamen" id="tamaño_velamen" class="form-control" placeholder=""
                    aria-describedby="helpId"/>
            </div>
            @if ($errors->has('tamaño_velamen'))
                <small class="form-text text-danger">{{ $errors->first('tamaño_velamen')}}</small>
            @endif
        </div>
    </div>
</div>
<div class="clearfix"></div>
