@extends('adminlte::page')
@section('title', 'Paracaidistas')
@section('css')
<<<<<<< HEAD

    <style>
        #paracaidistas tbody td:eq(0) {
            text-align: left;
        }
        #paracaidistas tbody td {
            text-align: center;
        }
    </style>
    @stop
=======
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
@stop
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Paracaidistas</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">
        <div class="card-header text-white bg-secondary" style="display: flex">
            <div class="col-sm-6">
                <h2>Listado de Paracaidistas</h2>
            </div>
            <div class="col-sm-6 text-right">
                @can('paracaidistas.create')
                    <a href="{{ route('paracaidistas.create') }}" class="nav-link">
                        <span class="btn btn-info">+ Registrar Paracaidista</span>
                    </a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table id="paracaidistas" class="table table-hover text-nowrap">
                        <thead>
                            <tr class="table-active">
                                <th style="text-align:center;">Id</th>
                                <th style="text-align:center;">Persona</th>
                                <th style="text-align:center;">Licencia</th>
                                <th style="text-align:center;">Categoria</th>
                                <th style="text-align:center;">Cantidad de saltos</th>
                                <th style="text-align:center;">Tamaño de velamen</th>
                                <th style="text-align:center;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($paracaidistas as $paracaidista)
                                <tr>
                                    <th scope="row">{{ $paracaidista->id }}</th>
                                    @foreach ($personas as $persona)
                                    @if ($paracaidista->persona_id == $persona->id)
                                        <td>{{ $persona->nombre }}</td>
                                    @endif
                                    @endforeach
                                    <td style="text-align:center;">{{ $paracaidista->licencia }}</td>
                                    <td style="text-align:center;">{{ $paracaidista->categoria }}</td>
                                    <td style="text-align:center;">{{ $paracaidista->cant_saltos }}</td>
                                    <td style="text-align:center;">{{ $paracaidista->tamaño_velamen }}</td>

                                    <td  style="text-align:center;">
                                        @csrf
                                        {!! Form::open(['route' => ['paracaidistas.destroy', $paracaidista], 'method' => 'DELETE', 'class' => 'formulario-eliminar']) !!}
                                        @can('paracaidistas.edit')
                                            <a class="btn btn-sm btn-warning" href="{{ route('paracaidistas.edit', $paracaidista) }}" title="Editar">
                                                <i class="far fa-edit"></i>
                                            </a>
                                        @endcan
                                        @can('paracaidistas.destroy')
                                            <button class="btn btn-sm btn-danger" type="submit" title="Eliminar">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                        @endcan
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')

<<<<<<< HEAD
    <script type="text/javascript">
    
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        
            var table = $('.data-table').DataTable({
                responsive:true,
=======
    <script>
        $(document).ready(function() {
            $('#paracaidistas').DataTable({
                responsive: true,
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar " +
                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="-1">All</option>
                                        </select>` +
                        " registros por página",
                    "zeroRecords": "No se encontró ningún registro.",
                    "info": "Mostrando la página  _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registro disponible.",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
        });
    </script>

    {{-- Recepcion de sessions para los estados de insertar actualizar y eliminar desplegando el sweeatalert --}}
    @if (session('guardar') == 'ok')
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido guardado existosamente.',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    @if (session('actualizar') == 'ok')
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido actualizado existosamente.',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    {{-- Rececpcion de status del metodo destroy para la confirmacion del alert de eliminado --}}
    @if (session('eliminar') == 'ok')
        <script>
            Swal.fire(
                'Eliminado!',
                'El registro ha sido eliminado correctamente.',
                'success'
            )
        </script>
    @endif
    <script>
        $('.formulario-eliminar').submit(function(e) {
            e.preventDefault();

            Swal.fire({
                title: 'Estas seguro?',
                text: "¡No podrás revertir esto!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, bórralo!'
            }).then((result) => {
                if (result.isConfirmed) {

                    this.submit();
                }
            })

        });
    </script>
@stop
