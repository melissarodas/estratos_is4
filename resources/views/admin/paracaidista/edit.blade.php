@extends('adminlte::page')
@section('title','Editar de Paracaidista')
@section('css')
@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Editar Paracaidista: <i>{{$paracaidista->nombre}}</i></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('paracaidistas.index')}}">Paracaidistas</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editar Paracaidista</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- TITULO DEL CARD -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Editar Paracaidista</h3>
                    </div>
                    {!! Form::model($paracaidista, ['route'=>['paracaidistas.update',$paracaidista], 'method'=>'PUT']) !!}
                        <div class="card-body">
                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="persona_id">Asociar Persona</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                                            </div>
                                            <select class="form-control" name="persona_id" id="persona_id">
                                                <option value="" disabled selected>Selecccione una persona</option>
                                                @foreach ($personas as $persona)
                                                @if (old('id', $paracaidista->persona->id) == $persona->id)
                                                    <option value="{{ $persona->id }}" selected>
                                                        {{ $persona->nombre }}</option>
                                                @else
                                                    <option value="{{ $persona->id }}">{{ $persona->nombre }}
                                                    </option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="licencia">Licencia</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="licencia" id="licencia" class="form-control" placeholder="" aria-describedby="helpId" value="{{$paracaidista->licencia}}" />
                                        </div>
                                        @if ($errors->has('licencia'))
                                        <small class="form-text text-danger">{{ $errors->first('licencia')}}</small>
                                    @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="categoria">Categoria</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                                            </div>
                                            {!! Form::select('categoria', ['A' => 'A', 'AI' => 'AI', 'B' => 'B', 'C' => 'C'],$paracaidista->categoria, ['class' => 'form-control'] ) !!}
                                        </div>
                                        @if ($errors->has('categoria'))
                                        <small class="form-text text-danger">{{ $errors->first('categoria')}}</small>
                                    @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="cant_saltos">Cantidad de saltos</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="cant_saltos" id="cant_saltos" class="form-control" placeholder="" aria-describedby="helpId" value="{{$paracaidista->cant_saltos}}" />
                                        </div>
                                        @if ($errors->has('cant_saltos'))
                                        <small class="form-text text-danger">{{ $errors->first('cant_saltos')}}</small>
                                    @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="tamaño_velamen">Tamaño de velamen</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="tamaño_velamen" id="tamaño_velamen" class="form-control" placeholder="" aria-describedby="helpId" value="{{$paracaidista->tamaño_velamen}}" />
                                        </div>
                                        @if ($errors->has('tamaño_velamen'))
                                        <small class="form-text text-danger">{{ $errors->first('tamaño_velamen')}}</small>
                                    @endif
                                    </div>
                                </div>


                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                            <a href="{{route('paracaidistas.index')}}" class="btn btn-light">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
@stop
