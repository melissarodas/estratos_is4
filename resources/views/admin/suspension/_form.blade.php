<div class="form-row">

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="motivo">Motivo</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="motivo" id="motivo" class="form-control" placeholder="" aria-describedby="helpId"/>
            </div>
       
            <small class="form-text text-danger error-motivo"></small>     

        </div>
    </div>

    <div class="form-group col-md-4">
        <label for="fecha">Fecha de suspensión</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3"><i class="fa fa-calendar menu-icon"></i></span>
            </div>
            <input type="date" name="fecha" id="fecha" class="form-control"  value="<?php echo date("Y-m-d");?>" >
        </div>
        <small class="form-text text-danger error-fecha"></small>     

    </div>


    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="paracaidista_id">Paracaidista</label>
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                </div>
                <select class="form-control" name="paracaidista_id" id="paracaidista_id">
                    <option value="" disabled selected>Selecccione un paracaidista</option>
                    @foreach ($paracaidistas as $paracaidista)
                    <option value="{{$paracaidista->id}}">{{$paracaidista->persona->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <small class="form-text text-danger error-paracaidista-id"></small>     

        </div>
    </div>

    

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="duracion">Duración en horas</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                </div>
                <input type="integer" name="duracion" id="duracion" class="form-control" placeholder=""
                    aria-describedby="helpId"/>
            </div>
            <small class="form-text text-danger error-duracion"></small>     

        </div>
    </div>



</div>
<div class="clearfix"></div>
