@extends('adminlte::page')
@section('title', 'Suspensiones')
@section('css')
 
<style>
    #suspensiones tbody td:eq(0) {
        text-align: left;
    }
    #suspensiones tbody td {
        text-align: center;
    }
</style>
@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Suspensiones</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">
        <div class="card-header text-white bg-secondary" style="display: flex">
            <div class="col-sm-6">
                <h2>Listado de Suspensiones</h2>
            </div>
            <div class="col-sm-6 text-right">
                @can('suspensiones.create')
                    <a class="btn btn-primary" href="javascript:void(0)" id="crearNuevaSuspension">+ Agregar</a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table id="suspensiones" class="table table-striped table-bordered table-hover data-table" style="width:100%">
                        <thead>
                            <tr class="table-active">
                                <th style="text-align: center;">Id</th>
                                <th style="text-align: center;">Paracaidista</th>
                                <th style="text-align: center;">Motivo</th>
                                <th style="text-align: center;">Fecha</th>
                                <th style="text-align: center;">Duración</th>
                                <th style="text-align: center;">Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

         <!-- Modal de registro y edición-->
         <div class="modal fade" id="agregar_suspension_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg ">
                <div class="modal-content">
                  
                    <div class="modal-header">
                        <h4 class="modal-title col-11 text-center" id="modelHeading"></h4>  {{--titulo del modal --}}
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
    
                    <form id="agregar_suspension_formulario" name="agregar_suspension_formulario">
                        <input type="hidden" name="suspension_id" id="suspension_id"> {{--id del registro oculto para la edicion --}}
                        <div class="modal-body">
                            @csrf
                            @include('admin.suspension._form')
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="btnGuardarSuspension" value="create">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@stop
@section('js')

    <script type="text/javascript">
    
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        
            var table = $('.data-table').DataTable({
                responsive:true,
                autoWidth: false,
                    "language": {
                        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Cargando...</span> ',
                        "lengthMenu": "Mostrar "+
                                            `<select class="custom-select custom-select-sm form-control form-control-sm">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="-1">All</option>
                                            </select>` +
                                            " registros por página",
                        "zeroRecords": "No se encontró ningún registro.",
                        "info": "Mostrando la página  _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registro disponible.",
                        "infoFiltered": "(filtrado de _MAX_ registros totales)",
                        "search": "Buscar:",
                        "paginate":{
                            "next": "Siguiente",
                            "previous" : "Anterior"
                        }
                    },
                processing: true,
                serverSide: true,
                ajax: "{{ route('suspensionParacaidistas.index') }}",
                columns: [
                    {data: 'id'    , name: 'id'},
                    {data: 'paracaidista', name: 'paracaidista'},
                    {data: 'motivo', name: 'motivo'},
                    {data: 'fecha', name: 'fecha'},
                    {data: 'duracion', name: 'duracion'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    
                ],        
            
            });
    
            //Boton de crear nueva persona
            $('#crearNuevaSuspension').click(function () {
                $('#btnGuardarSuspension').val("create-suspension");
                $('#suspension_id').val('');
                $('#agregar_suspension_formulario').trigger("reset");
                $('#modelHeading').html("Nueva Suspensión").css( "font-weight","bold" );
                $('#agregar_suspension_modal').modal('show');
                
                $('.error-paracaidista-id').html('');
                $('.error-motivo').html('');
                $('.error-duracion').html('');
                $('.error-fecha').html('');
    
            });
    
            // Boton de editar persona
            $('body').on('click', '.editarSuspension', function () {
                var suspension_id = $(this).data('id');
                $.get("{{ route('suspensionParacaidistas.index') }}" +'/' + suspension_id +'/edit', function (data) {
                    $('#modelHeading').html("Editar suspensión").css( "font-weight","bold" );
                    $('#btnGuardarSuspension').val("edit-suspension");
                    $('#agregar_suspension_modal').modal('show');
                    
                    $('#suspension_id').val(data.id);
                    $('#paracaidista_id').val(data.paracaidista_id);
                    $('#motivo').val(data.motivo);
                    $('#duracion').val(data.duracion);
                    $('#fecha').val(data.fecha);

                    $('.error-paracaidista-id').html('');
                    $('.error-motivo').html('');
                    $('.error-duracion').html('');
                    $('.error-fecha').html('');
    
                })
            });
    
            //Boton de guardar persona
            $('#btnGuardarSuspension').click(function (e) {
                e.preventDefault();
                $(this).html('Guardar');
    
                $.ajax({
                    data: $('#agregar_suspension_formulario').serialize(),
                    url: "{{ route('suspensionParacaidistas.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#agregar_suspension_formulario').trigger("reset");
                        $('#agregar_suspension_modal').modal('hide');
    
                        var operacion = $('#btnGuardarSuspension').val();
    
                        if( operacion == 'edit-suspension'){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'La suspensión ha sido actualizado correctamente',
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }else{
                            Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: data.success,
                            showConfirmButton: false,
                            timer: 1500,
                        });
    
                        }
                        table.draw();
                    },
                    
                    error: function (data) {
                        console.log('Error:', data);
                        console.log('MENSAJE', data.responseJSON.errors);
                        if(data.responseJSON.errors.paracaidista_id != null)
                        {
                            $('.error-paracaidista-id').html(data.responseJSON.errors.paracaidista_id[0]);
                        }
                        if(data.responseJSON.errors.motivo != null)
                        {
                            $('.error-motivo').html(data.responseJSON.errors.motivo[0]);
                        }
                        if(data.responseJSON.errors.duracion != null)
                        {
                            $('.error-duracion').html(data.responseJSON.errors.duracion[0]);
                        }
                        if(data.responseJSON.errors.fecha != null)
                        {
                            $('.error-fecha').html(data.responseJSON.errors.fecha[0]);
                        }
    
                    }
                });
            });
    
            $('body').on('click', '.eliminarSuspension', function () {
                var suspension_id = $(this).data("id");
                Swal.fire({
                        title: 'Estas seguro?',
                        text: "¡No podrás revertir esta operación!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: '¡Sí, continuar!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                type: "DELETE",
                                url: "{{ route('suspensionParacaidistas.store') }}"+'/'+suspension_id,
                                success: function (data) {
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: data.success,
                                        showConfirmButton: false,
                                        timer: 1500,
                                    }
    
                                    );
                                    table.draw();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
    
    
                        }
                    });
    
                });
    
            });
            
    </script>
@stop
