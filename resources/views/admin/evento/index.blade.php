@extends('adminlte::page')
@section('title', 'Eventos')
@section('css')
<<<<<<< HEAD
 
    <style>
        #eventos tbody td:eq(0) {
            text-align: left;
        }
        #eventos tbody td {
            text-align: center;
        }
    </style>
=======
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Eventos</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">
        <div class="card-header text-white bg-secondary" style="display: flex">
            <div class="col-sm-6">
                <h2>Listado de Eventos</h2>
            </div>
            <div class="col-sm-6 text-right">
                @can('eventos.create')
                    <a href="{{ route('eventos.create') }}" class="nav-link">
                        <span class="btn btn-info">+ Registrar Evento</span>
                    </a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table id="eventos" class="table table-hover text-nowrap">
                        <thead>
                            <tr class="table-active">
                                <th style="text-align: center;">Id</th>
                                <th style="text-align: center;">Nombre del evento</th>
                                <th style="text-align: center;">Fecha</th>
                                <th style="text-align: center;">Organizador</th>
                                <th style="text-align: center;">Lugar</th>
                                {{-- <th style="text-align: center;">RTA</th> --}}
                                <th style="text-align: center;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($eventos as $evento)
                                <tr>
                                    <th scope="row" style="text-align: center;">{{ $evento->id }}</th>
                                    <td style="text-align: center;">{{ $evento->nombre }}</td>
                                    <td style="text-align: center;">{{ $evento->fecha }}</td>

                                    @foreach ($personas as $persona)
                                    @if ($evento->organizador_id == $persona->id)
                                        <td style="text-align: center;">{{ $persona->nombre }}</td>
                                    @endif
                                    @endforeach

                                    @foreach ($zonas as $zona)
                                    @if ($evento->zona_id == $zona->id)
                                        <td style="text-align: center;">{{ $zona->nombre }}</td>
                                    @endif
                                    @endforeach

                                    @foreach ($personas as $persona)
                                    @if ($evento->rta_id == $persona->id)
                                        <td style="text-align: center;">{{ $persona->nombre }}</td>
                                    @endif
                                    @endforeach

                                    <td style="text-align: center;">
                                        @csrf
                                        {!! Form::open(['route' => ['eventos.destroy', $evento], 'method' => 'DELETE', 'class' => 'formulario-eliminar']) !!}
                                        @can('eventos.edit')
                                            <a class="btn btn-sm btn-warning" href="{{ route('eventos.edit', $evento) }}" title="Editar">
                                                <i class="far fa-edit"></i>
                                            </a>
                                        @endcan
                                        @can('eventos.destroy')
                                            <button class="btn btn-sm btn-danger" type="submit" title="Eliminar">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                        @endcan
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')

<<<<<<< HEAD
   <script type="text/javascript">
    
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        
            var table = $('.data-table').DataTable({
                responsive:true,
                autoWidth: false,
                    "language": {
                        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Cargando...</span> ',
                        "lengthMenu": "Mostrar "+
                                            `<select class="custom-select custom-select-sm form-control form-control-sm">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="-1">All</option>
                                            </select>` +
                                            " registros por página",
                        "zeroRecords": "No se encontró ningún registro.",
                        "info": "Mostrando la página  _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registro disponible.",
                        "infoFiltered": "(filtrado de _MAX_ registros totales)",
                        "search": "Buscar:",
                        "paginate":{
                            "next": "Siguiente",
                            "previous" : "Anterior"
                        }
                    },
                processing: true,
                serverSide: true,
                ajax: "{{ route('eventos.index') }}",
                columns: [
                    {data: 'id'    , name: 'id'},
                    {data: 'nombre', name: 'nombre'},
                    {data: 'fecha', name: 'fecha'},
                    {data: 'organizador', name: 'organizador'},
                   // {data: 'rta', name: 'rta'},
                    {data: 'zona', name: 'zona'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],        
            
            });
    
            //Boton de crear nueva persona
            $('#crearNuevoEvento').click(function () {
                $('#btnGuardarEvento').val("create-evento");
                $('#evento_id').val('');
                $('#agregar_evento_formulario').trigger("reset");
                $('#modelHeading').html("Nuevo Evento").css( "font-weight","bold" );
                $('#agregar_evento_modal').modal('show');
                
                $('.error-nombre').html('');
                $('.error-fecha').html('');
                $('.error-organizador-id').html('');
                $('.error-rta-id').html('');
                $('.error-zona-id').html('');
    
            });
    
            // Boton de editar persona
            $('body').on('click', '.editarEvento', function () {
                var evento_id = $(this).data('id');
                $.get("{{ route('eventos.index') }}" +'/' + evento_id +'/edit', function (data) {
                    $('#modelHeading').html("Editar Evento").css( "font-weight","bold" );
                    $('#btnGuardarEvento').val("edit-evento");
                    $('#agregar_evento_modal').modal('show');
                    
                    $('#evento_id').val(data.id);
                    $('#nombre').val(data.nombre);
                    $('#fecha').val(data.fecha);
                    $('#organizador_id').val(data.organizador_id);
                    $('#rta_id').val(data.rta_id);
                    $('#zona_id').val(data.zona_id);
                  
                    $('.error-nombre').html('');
                    $('.error-fecha').html('');
                    $('.error-organizador-id').html('');
                    $('.error-rta-id').html('');
                    $('.error-zona-id').html('');
                })
            });
    
            //Boton de guardar persona
            $('#btnGuardarEvento').click(function (e) {
                e.preventDefault();
                $(this).html('Guardar');
    
                $.ajax({
                    data: $('#agregar_evento_formulario').serialize(),
                    url: "{{ route('eventos.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#agregar_evento_formulario').trigger("reset");
                        $('#agregar_evento_modal').modal('hide');
    
                        var operacion = $('#btnGuardarEvento').val();
    
                        if( operacion == 'edit-evento'){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'El evento ha sido actualizado correctamente',
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }else{
                            Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: data.success,
                            showConfirmButton: false,
                            timer: 1500,
                        });
    
                        }
                        table.draw();
                    },
                    
                    error: function (data) {
                        console.log('Error:', data);
                        console.log('MENSAJE', data.responseJSON.errors);
                        if(data.responseJSON.errors.nombre != null)
                        {
                            $('.error-nombre').html(data.responseJSON.errors.nombre[0]);
                        }
                        if(data.responseJSON.errors.fecha != null)
                        {
                            $('.error-fecha').html(data.responseJSON.errors.fecha[0]);
                        }
                        if(data.responseJSON.errors.organizador_id != null)
                        {
                            $('.error-organizador-id').html(data.responseJSON.errors.organizador_id[0]);
                        }
                        if(data.responseJSON.errors.rta_id != null)
                        {
                            $('.error-rta-id').html(data.responseJSON.errors.rta_id[0]);
                        }
                        if(data.responseJSON.errors.zona_id != null)
                        {
                            $('.error-zona-id').html(data.responseJSON.errors.zona_id[0]);
                        }
                    }
                });
            });
    
            $('body').on('click', '.eliminarEvento', function () {
                var evento_id = $(this).data("id");
                Swal.fire({
                        title: 'Estas seguro?',
                        text: "¡No podrás revertir esta operación!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: '¡Sí, continuar!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                type: "DELETE",
                                url: "{{ route('eventos.store') }}"+'/'+evento_id,
                                success: function (data) {
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: data.success,
                                        showConfirmButton: false,
                                        timer: 1500,
                                    }
    
                                    );
                                    table.draw();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
    
    
                        }
                    });
    
                });
    
=======
    <script>
        $(document).ready(function() {
            $('#eventos').DataTable({
                responsive: true,
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar " +
                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="-1">All</option>
                                        </select>` +
                        " registros por página",
                    "zeroRecords": "No se encontró ningún registro.",
                    "info": "Mostrando la página  _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registro disponible.",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
>>>>>>> 2cef6c7a3e36b3a056dcc9d9ee615f2aa4addf96
            });
        });
    </script>

    {{-- Recepcion de sessions para los estados de insertar actualizar y eliminar desplegando el sweeatalert --}}
    @if (session('guardar') == 'ok')
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido guardado existosamente.',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    @if (session('actualizar') == 'ok')
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido actualizado existosamente.',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    {{-- Rececpcion de status del metodo destroy para la confirmacion del alert de eliminado --}}
    @if (session('eliminar') == 'ok')
        <script>
            Swal.fire(
                'Eliminado!',
                'El registro ha sido eliminado correctamente.',
                'success'
            )
        </script>
    @endif
    @if (session('eliminar') == 'error')
    <script>
        Swal.fire(
            'Error!',
            'No se puede eliminar el registro por que cuenta con dependencias. Verifique.',
            'error'
        )
    </script>
    @endif
    <script>
        $('.formulario-eliminar').submit(function(e) {
            e.preventDefault();

            Swal.fire({
                title: 'Estas seguro?',
                text: "¡No podrás revertir esto!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, bórralo!'
            }).then((result) => {
                if (result.isConfirmed) {

                    this.submit();
                }
            })

        });
    </script>
@stop
