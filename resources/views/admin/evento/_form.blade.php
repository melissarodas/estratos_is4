<div class="form-row">

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="nombre">Nombre del evento</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="nombre" id="nombre" class="form-control" placeholder="" aria-describedby="helpId"/>
            </div>
            <small class="form-text text-danger error-nombre"></small>     
        </div>
    </div>

    <div class="form-group col-md-4">
        <label for="fecha">Fecha del evento</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3"><i class="fa fa-calendar menu-icon"></i></span>
            </div>
            <input type="date" name="fecha" id="fecha" class="form-control"  value="<?php echo date("Y-m-d");?>" >
        </div>
        <small class="form-text text-danger error-fecha"></small>     

    </div>


    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="organizador_id">Organizador</label>
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                </div>
                <select class="form-control" name="organizador_id" id="organizador_id">
                    <option value="" disabled selected>Selecccione una persona</option>
                    @foreach ($personas as $persona)
                    <option value="{{$persona->id}}">{{$persona->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <small class="form-text text-danger error-organizador-id"></small>     
        </div>
    </div>

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="rta_id">RTA</label>
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                </div>
                <select class="form-control" name="rta_id" id="rta_id">
                    <option value="" disabled selected>Selecccione una persona</option>
                    @foreach ($personas as $persona)
                    <option value="{{$persona->id}}">{{$persona->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <small class="form-text text-danger error-rta-id"></small>     
        </div>
    </div>


    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="zona_id">Lugar del evento</label>
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                </div>
                <select class="form-control" name="zona_id" id="zona_id">
                    <option value="" disabled selected>Selecccione una zona</option>
                    @foreach ($zonas as $zona)
                    <option value="{{$zona->id}}">{{$zona->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <small class="form-text text-danger error-zona-id"></small>     
        </div>
    </div>




</div>
<div class="clearfix"></div>
