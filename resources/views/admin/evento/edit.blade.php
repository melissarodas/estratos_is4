@extends('adminlte::page')
@section('title','Editar de Evento')
@section('css')
@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Editar Evento: <i>{{$evento->nombre}}</i></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('eventos.index')}}">Eventos</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editar Evento</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- TITULO DEL CARD -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Editar Evento</h3>
                    </div>
                    {!! Form::model($evento, ['route'=>['eventos.update',$evento], 'method'=>'PUT']) !!}
                        <div class="card-body">

                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="nombre">Nombre del evento</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="nombre" id="nombre" class="form-control" placeholder=""
                                                aria-describedby="helpId" value="{{$evento->nombre}}" />
                                        </div>
                                        @if ($errors->has('nombre'))
                                        <small class="form-text text-danger">{{ $errors->first('nombre')}}</small>
                                    @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="fecha">Fecha del evento</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon3"><i class="fa fa-calendar menu-icon"></i></span>
                                        </div>
                                        <input type="date" name="fecha" id="fecha" class="form-control"  value="{{$evento->fecha}}" >
                                    </div>
                                </div>


                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="organizador_id">Organizador</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                                            </div>
                                            <select class="form-control" name="organizador_id" id="organizador_id">
                                                <option value="" disabled selected>Selecccione una persona</option>
                                                @foreach ($personas as $persona)
                                                @if (old('id', $evento->organizador->id) == $persona->id)
                                                    <option value="{{ $persona->id }}" selected>
                                                        {{ $persona->nombre }}</option>
                                                @else
                                                    <option value="{{ $persona->id }}">{{ $persona->nombre }}
                                                    </option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="rta_id">RTA</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                                            </div>
                                            <select class="form-control" name="rta_id" id="rta_id">
                                                <option value="" disabled selected>Selecccione una persona</option>
                                                @foreach ($personas as $persona)
                                                @if (old('id', $evento->rta->id) == $persona->id)
                                                    <option value="{{ $persona->id }}" selected>
                                                        {{ $persona->nombre }}</option>
                                                @else
                                                    <option value="{{ $persona->id }}">{{ $persona->nombre }}
                                                    </option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="zona_id">Lugar del evento</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                                            </div>
                                            <select class="form-control" name="zona_id" id="zona_id">
                                                <option value="" disabled selected>Selecccione una zona</option>
                                                @foreach ($zonas as $zona)
                                                <option value="{{$zona->id}}">{{$zona->nombre}}</option>
                                                @endforeach

                                                @foreach ($zonas as $zona)
                                                @if (old('id', $evento->zona->id) == $zona->id)
                                                    <option value="{{ $zona->id }}" selected>
                                                        {{ $zona->nombre }}</option>
                                                @else
                                                    <option value="{{ $zona->id }}">{{ $zona->nombre }}
                                                    </option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>




                            </div>




                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                            <a href="{{route('eventos.index')}}" class="btn btn-light">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
@stop
