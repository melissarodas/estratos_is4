@extends('adminlte::page')
@section('title','Editar Tipo de titulación')
@section('css')
@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Editar Tipo</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('pruebas.index')}}">Tipos de Titulaciones</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editar Tipo</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- TITULO DEL CARD -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Editar Tipo</h3>
                    </div>
                    {!! Form::model($titulacionTipo, ['route'=>['pruebas.update', $titulacionTipo], 'method'=>'PUT']) !!}
                        <div class="card-body">
                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="nombre">Nombre</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                                            </div>
                                            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="" aria-describedby="helpId" value="{{$titulacionTipo->nombre}}">
                                        </div>
                                        @if ($errors->has('nombre'))
                                            <small class="form-text text-danger">{{ $errors->first('nombre')}}</small>
                                        @endif
                                    </div>
                                </div>


                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                            <a href="{{route('pruebas.index')}}" class="btn btn-light">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
@stop
