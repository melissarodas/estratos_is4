<div class="form-row">

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="nombre" id="nombre" class="form-control" placeholder="" aria-describedby="helpId" required>
            </div>
            @if ($errors->has('nombre'))
                <small class="form-text text-danger">{{ $errors->first('nombre')}}</small>
            @endif
        </div>
    </div>

    
</div>
<div class="clearfix"></div>
