@extends('adminlte::page')
@section('title','Editar permiso')
@section('css')
@stop
@section('content_header')
@stop
@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edición de permiso</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Configuración</a></li>
                    <li class="breadcrumb-item"><a href="{{route('permissions.index')}}">Permisos</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editar Permiso</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                @if (session('info'))
                      <x-adminlte-alert class="text-uppercase" theme="success" title="ACTUALIZADO!!" dismissable>
                        <div>
                            {{session('info')}}
                        </div>
                    </x-adminlte-alert>
                @endif
                @if ($errors->any())
                <x-adminlte-alert class="text-uppercase" theme="danger" title="Atención!!" dismissable>
                    <ul>
                        @foreach ( $errors->all() as $error )
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-adminlte-alert>
                @endif

                <div class="card card-secondary">
                    <div class="card-header">
                        Editar Permiso: <i> {{$permission->name}}</i>
                    </div>

                    {!! Form::model($permission,['route'=>['permissions.update',$permission], 'method'=>'PUT']) !!}
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div class="form-group">
                                    <label for="name">Permiso</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-keyboard menu-icon"></i></span>
                                        </div>
                                        <input type="text" name="name" id="name" value="{{$permission->name}}" class="form-control" placeholder="" aria-describedby="helpId">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="form-group">
                                    <label for="description">Descripción</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-comment menu-icon"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="description" id="description" value="{{$permission->description}}"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="card-footer text-muted">
                        <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                        <a href="{{route('permissions.index')}}" class="btn btn-light">Cancelar</a>
                    </div>

                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
@stop


