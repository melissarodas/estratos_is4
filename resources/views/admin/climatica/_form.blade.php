<div class="form-row">

    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="altitud">Altitud</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="number" name="altitud" id="altitud" class="form-control" placeholder="Altitud" aria-describedby="helpId">
            </div>
            <small class="form-text text-danger error-altitud"></small>

        </div>
    </div>


    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="temperatura">Temperatura</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="number" name="temperatura" id="temperatura" class="form-control" placeholder="Temperatura" aria-describedby="helpId">
            </div>
            <small class="form-text text-danger error-temperatura"></small>

        </div>
    </div>

    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="direccion_viento">Dirección del viento</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="number" name="direccion_viento" id="direccion_viento" class="form-control" placeholder="Direccion del viento" aria-describedby="helpId">
            </div>
            <small class="form-text text-danger error-direccion-viento"></small>

        </div>
    </div>

    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="velocidad_viento">Velocidad del viento</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="number" name="velocidad_viento" id="velocidad_viento" class="form-control" placeholder="Velocidad del viento" aria-describedby="helpId">
            </div>
            <small class="form-text text-danger error-velocidad-viento"></small>

        </div>
    </div>


    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="decolaje_id">Decolaje</label>
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                </div>
                
                <select class="form-control" name="decolaje_id" id="decolaje_id">
                    <option value="" disabled selected>Selecccione un decolaje</option>
                    @foreach ($decolajes as $decolaje)
                    <option value="{{$decolaje->id}}">{{$decolaje->evento->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <small class="form-text text-danger error-decolaje-id"></small>

        </div>
    </div>
</div>
<div class="clearfix"></div>
