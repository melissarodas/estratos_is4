@extends('adminlte::page')
@section('title', 'Condiciones climaticas')
@section('css')
   
<style>
    #climaticas tbody td:eq(0) {
        text-align: left;
    }
    #climaticas tbody td {
        text-align: center;
    }
</style>
@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Condiciones climaticas</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">
        <div class="card-header text-white bg-secondary" style="display: flex">
            <div class="col-sm-6">
                <h2>Listado de Condiciones climaticas</h2>
            </div>
            <div class="col-sm-6 text-right">
                @can('climaticas.create')
                    <a class="btn btn-primary" href="javascript:void(0)" id="crearNuevaClimatica">+ Agregar</a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table id="climaticas" class="table table-striped table-bordered table-hover data-table" style="width:100%">

                        <thead>
                            <tr class="table-active">
                                <th style="text-align: center;">Id</th>
                                <th style="text-align: center;">Decolaje-Evento</th>
                                <th style="text-align: center;">Altitud</th>
                                <th style="text-align: center;">Temperatura</th>
                                <th style="text-align: center;">Direccion del viento</th>
                                <th style="text-align: center;">Velocidad del viento</th>
                                <th style="text-align: center;">Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal de registro y edición-->
    <div class="modal fade" id="agregar_climatica_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content">
              
                <div class="modal-header">
                    <h4 class="modal-title col-11 text-center" id="modelHeading"></h4>  {{--titulo del modal --}}
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form id="agregar_climatica_formulario" name="agregar_climatica_formulario">
                    <input type="hidden" name="clima_id" id="clima_id"> {{--id del registro oculto para la edicion --}}
                    <div class="modal-body">
                        @csrf
                        @include('admin.climatica._form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="btnGuardarClimatica" value="create">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')
 
    <script type="text/javascript">
    
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        
            var table = $('.data-table').DataTable({
                responsive:true,
                autoWidth: false,
                    "language": {
                        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Cargando...</span> ',
                        "lengthMenu": "Mostrar "+
                                            `<select class="custom-select custom-select-sm form-control form-control-sm">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="-1">All</option>
                                            </select>` +
                                            " registros por página",
                        "zeroRecords": "No se encontró ningún registro.",
                        "info": "Mostrando la página  _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registro disponible.",
                        "infoFiltered": "(filtrado de _MAX_ registros totales)",
                        "search": "Buscar:",
                        "paginate":{
                            "next": "Siguiente",
                            "previous" : "Anterior"
                        }
                    },
                processing: true,
                serverSide: true,
                ajax: "{{ route('climaticas.index') }}",
                columns: [
                    {data: 'id'    , name: 'id'},
                    {data: 'eventodecolaje', name: 'eventodecolaje'},
                    {data: 'altitud', name: 'altitud'},
                    {data: 'temperatura', name: 'temperatura'},
                    {data: 'direccion_viento', name: 'direccion_viento'},
                    {data: 'velocidad_viento', name: 'velocidad_viento'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],        
            
            });
    
            //Boton de crear nueva persona
            $('#crearNuevaClimatica').click(function () {
                $('#btnGuardarClimatica').val("create-climatica");
                $('#clima_id').val('');
                $('#agregar_climatica_formulario').trigger("reset");
                $('#modelHeading').html("Nueva Condición climatica").css( "font-weight","bold" );
                $('#agregar_climatica_modal').modal('show');
                
                $('.error-altitud').html('');
                $('.error-temperatura').html('');
                $('.error-direccion-viento').html('');
                $('.error-velocidad-viento').html('');
                $('.error-decolaje-id').html('');
    
            });
    
            // Boton de editar persona
            $('body').on('click', '.editarClimatica', function () {
                var clima_id = $(this).data('id');
                $.get("{{ route('climaticas.index') }}" +'/' + clima_id +'/edit', function (data) {
                    $('#modelHeading').html("Editar Condición climatica").css( "font-weight","bold" );
                    $('#btnGuardarClimatica').val("edit-climatica");
                    $('#agregar_climatica_modal').modal('show');
                    
                    $('#clima_id').val(data.id);
                    $('#altitud').val(data.altitud);
                    $('#temperatura').val(data.temperatura);
                    $('#direccion_viento').val(data.direccion_viento);
                    $('#velocidad_viento').val(data.velocidad_viento);
                    $('#decolaje_id').val(data.decolaje_id);

                    $('.error-altitud').html('');
                    $('.error-temperatura').html('');
                    $('.error-direccion-viento').html('');
                    $('.error-velocidad-viento').html('');
                    $('.error-decolaje-id').html('');


                })
            });
    
            //Boton de guardar persona
            $('#btnGuardarClimatica').click(function (e) {
                e.preventDefault();
                $(this).html('Guardar');
    
                $.ajax({
                    data: $('#agregar_climatica_formulario').serialize(),
                    url: "{{ route('climaticas.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#agregar_climatica_formulario').trigger("reset");
                        $('#agregar_climatica_modal').modal('hide');
    
                        var operacion = $('#btnGuardarClimatica').val();
    
                        if( operacion == 'edit-climatica'){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'La condicion climatica ha sido actualizada correctamente',
                                showConfirmButton: false,
                                timer: 1500,
                            });
                        }else{
                            Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: data.success,
                            showConfirmButton: false,
                            timer: 1500,
                        });
    
                        }
                        table.draw();
                    },
                    
                    error: function (data) {
                        console.log('Error:', data);
                        console.log('MENSAJE', data.responseJSON.errors);
                        if(data.responseJSON.errors.altitud != null)
                        {
                            $('.error-altitud').html(data.responseJSON.errors.altitud[0]);
                        }
                        if(data.responseJSON.errors.temperatura != null)
                        {
                            $('.error-temperatura').html(data.responseJSON.errors.temperatura[0]);
                        }
                        if(data.responseJSON.errors.direccion_viento != null)
                        {
                            $('.error-direccion-viento').html(data.responseJSON.errors.direccion_viento[0]);
                        }
                        if(data.responseJSON.errors.velocidad_viento != null)
                        {
                            $('.error-velocidad-viento').html(data.responseJSON.errors.velocidad_viento[0]);
                        }
                        if(data.responseJSON.errors.decolaje_id != null)
                        {
                            $('.error-decolaje-id').html(data.responseJSON.errors.decolaje_id[0]);
                        }
                       
    
                    }
                });
            });
    
            $('body').on('click', '.eliminarClimatica', function () {
                var clima_id = $(this).data("id");
                Swal.fire({
                        title: 'Estas seguro?',
                        text: "¡No podrás revertir esta operación!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: '¡Sí, continuar!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                type: "DELETE",
                                url: "{{ route('climaticas.store') }}"+'/'+clima_id,
                                success: function (data) {
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: data.success,
                                        showConfirmButton: false,
                                        timer: 1500,
                                    }
    
                                    );
                                    table.draw();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
    
    
                        }
                    });
    
                });
    
            });
            
    </script>


@stop
