@extends('adminlte::page')
@section('title','Editar de Propietario')
@section('css')
@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Editar Propietario</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('propietarios.index')}}">Propietarios</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editar Propietario</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- TITULO DEL CARD -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Editar Propietario</h3>
                    </div>
                    {!! Form::model($propietariosEscuela, ['route'=>['propietarios.update',$propietariosEscuela], 'method'=>'PUT']) !!}
                        <div class="card-body">

                            <div class="form-row">

                                <div class="form-group col-md-4">
                                    <div class="form-group">
                                        <label for="escuela_id">Escuela</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                                            </div>
                                            <select class="form-control" name="escuela_id" id="escuela_id">
                                                <option value="" disabled selected>Selecccione una escuela</option>
                                                @foreach ($escuelas as $escuela)
                                                @if (old('id', $propietariosEscuela->escuela->id) == $escuela->id)
                                                    <option value="{{ $escuela->id }}" selected> {{ $escuela->licencia }}</option>
                                                @else
                                                    <option value="{{ $escuela->id }}">{{ $escuela->licencia }}
                                                    </option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                            <a href="{{route('propietarios.index')}}" class="btn btn-light">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
@stop
