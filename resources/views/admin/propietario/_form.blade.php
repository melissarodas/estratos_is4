<div class="form-row">

    
    <div class="form-group col-md-12">
        <div class="form-group">
            <label for="escuela_id">Escruela</label>
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-briefcase menu-icon"></i></span>
                </div>
                <select class="form-control" name="escuela_id" id="escuela_id">
                    <option value="" disabled selected>Selecccione una escuela</option>
                    @foreach ($escuelas as $escuela)
                    <option value="{{$escuela->id}}">{{$escuela->licencia}}</option>
                    @endforeach
                </select>
            </div>
            <small class="form-text text-danger error-escuela"></small>     

        </div>
    </div>


</div>
<div class="clearfix"></div>
