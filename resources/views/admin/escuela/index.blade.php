@extends('adminlte::page')
@section('title', 'Escuelas')
@section('css')
 
    <style>
        #escuelas tbody td:eq(0) {
            text-align: left;
        }
        #escuelas tbody td {
            text-align: center;
        }
    </style>
@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Escuelas</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">
        <div class="card-header text-white bg-secondary" style="display: flex">
            <div class="col-sm-6">
                <h2>Listado de Escuelas</h2>
            </div>
            <div class="col-sm-6 text-right">
                @can('escuelas.create')
                <a class="btn btn-primary" href="javascript:void(0)" id="crearNuevaEscuela">+ Agregar</a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table id="escuelas" class="table table-striped table-bordered table-hover data-table" style="width:100%">
                        <thead>
                            <tr class="table-active">
                                <th>Id</th>
                                <th>Licencia</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

     <!-- Modal de registro y edición-->
     <div class="modal fade" id="agregar_escuela_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content">
              
                <div class="modal-header">
                    <h4 class="modal-title col-11 text-center" id="modelHeading"></h4>  {{--titulo del modal --}}
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form id="agregar_escuela_formulario" name="agregar_escuela_formulario">
                    <input type="hidden" name="escuela_id" id="escuela_id"> {{--id del registro oculto para la edicion --}}
                    <div class="modal-body">
                        @csrf
                        @include('admin.escuela._form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="btnGuardarEscuela" value="create">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')

<script type="text/javascript">

    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        var table = $('.data-table').DataTable({
            responsive:true,
            autoWidth: false,
                "language": {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Cargando...</span> ',
                    "lengthMenu": "Mostrar "+
                                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="-1">All</option>
                                        </select>` +
                                        " registros por página",
                    "zeroRecords": "No se encontró ningún registro.",
                    "info": "Mostrando la página  _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registro disponible.",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate":{
                        "next": "Siguiente",
                        "previous" : "Anterior"
                    }
                },
            processing: true,
            serverSide: true,
            ajax: "{{ route('escuelas.index') }}",
            columns: [
                {data: 'id'    , name: 'id'},
                {data: 'licencia', name: 'licencia'},
                {data: 'estado', name: 'estado'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],        
        
        });

        //Boton de crear nueva persona
        $('#crearNuevaEscuela').click(function () {
            $('#btnGuardarEscuela').val("create-escuela");
            $('#escuela_id').val('');
            $('#agregar_escuela_formulario').trigger("reset");
            $('#modelHeading').html("Nueva Escuela").css( "font-weight","bold" );
            $('#agregar_escuela_modal').modal('show');
            
            $('.error-licencia').html('');
            $('.error-estado').html('');
       

        });

        // Boton de editar persona
        $('body').on('click', '.editarEscuela', function () {
            var escuela_id = $(this).data('id');
            $.get("{{ route('escuelas.index') }}" +'/' + escuela_id +'/edit', function (data) {
                $('#modelHeading').html("Editar Escuela").css( "font-weight","bold" );
                $('#btnGuardarEscuela').val("edit-escuela");
                $('#agregar_escuela_modal').modal('show');
                
                $('#escuela_id').val(data.id);
                $('#licencia').val(data.licencia);
                $('#estado').val(data.estado);

                $('.error-licencia').html('');
                $('.error-estado').html('');
            })
        });

        //Boton de guardar persona
        $('#btnGuardarEscuela').click(function (e) {
            e.preventDefault();
            $(this).html('Guardar');

            $.ajax({
                data: $('#agregar_escuela_formulario').serialize(),
                url: "{{ route('escuelas.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#agregar_escuela_formulario').trigger("reset");
                    $('#agregar_escuela_modal').modal('hide');

                    var operacion = $('#btnGuardarEscuela').val();

                    if( operacion == 'edit-escuela'){
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'La Escuela ha sido actualizada correctamente',
                            showConfirmButton: false,
                            timer: 1500,
                        });
                    }else{
                        Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: data.success,
                        showConfirmButton: false,
                        timer: 1500,
                    });

                    }
                    table.draw();
                },
                
                error: function (data) {
                    console.log('Error:', data);
                    console.log('MENSAJE', data.responseJSON.errors);
                    if(data.responseJSON.errors.licencia != null)
                    {
                        $('.error-licencia').html(data.responseJSON.errors.licencia[0]);
                    }
                    if(data.responseJSON.errors.estado != null)
                    {
                        $('.error-estado').html(data.responseJSON.errors.estado[0]);
                    }
                    
                }
            });
        });

        $('body').on('click', '.eliminarEscuela', function () {
            var escuela_id = $(this).data("id");
            Swal.fire({
                    title: 'Estas seguro?',
                    text: "¡No podrás revertir esta operación!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: '¡Sí, continuar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "DELETE",
                            url: "{{ route('escuelas.store') }}"+'/'+escuela_id,
                            success: function (data) {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: data.success,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }

                                );
                                table.draw();
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });


                    }
                });

            });

        });

      
        
</script>


@stop
