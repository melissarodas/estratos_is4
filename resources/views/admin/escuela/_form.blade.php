<div class="form-row">

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="licencia">Licencia</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="number" name="licencia" id="licencia" class="form-control" placeholder="" aria-describedby="helpId" required>
            </div>
            <small class="form-text text-danger error-licencia"></small>     

        </div>
    </div>

    <div class="form-group col-md-4">
        <div class="form-group">
            <label for="estado">Estado</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-keyboard menu-icon"></i></span>
                </div>
                <select name="estado" id="estado" class="form-control" required>
                    <option value="" disabled selected>Seleccione un estado</option>
                    <option value="ACTIVO">ACTIVO</option>
                    <option value="INACTIVO">INACTIVO</option>
                </select>
            </div>
            <small class="form-text text-danger error-estado"></small>     

        </div>
    </div>
    
</div>
<div class="clearfix"></div>
