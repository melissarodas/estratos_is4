@extends('adminlte::page')
@section('title', 'Información sobre el rol')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Configuración</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Roles</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Detalle</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="border-bottom text-center pb-4">
                                <h3>Rol: {{ $role->name }}</h3>
                                <div class="d-flex justify-content-between"></div>
                            </div>
                            <div class="border-bottom py-4">
                                <div class="list-group">
                                    <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Permisos</a>
                                    <a type="button" class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Usuarios</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 pl-lg-5">
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <h4>Permisos asignados al rol</h4>
                                        </div>
                                    </div>
                                    <div class="profile-feed">
                                        <div class="d-flex align-items-start profile-feed-item">
                                            <div class="table-responsive">
                                                <table id="order-listing" class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Nombre</th>
                                                            <th>Descripción</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($role->permissions as $permission)
                                                            <tr>
                                                                <th scope="row">{{ $permission->id }}</th>
                                                                <td>{{ $permission->name }}</td>
                                                                <td>{{ $permission->description }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <h4>Usuarios con el rol</h4>
                                        </div>
                                    </div>
                                    <div class="profile-feed">
                                        <div class="d-flex align-items-start profile-feed-item">
                                            <div class="table-responsive">
                                                <table id="order-listing1" class="table">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 50px;">Id</th>
                                                            <th style="width: 200px;">Nombre</th>
                                                            <th style="width: 200px;">Correo electrónico</th>
                                                            <th style="width: 50px;">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($role->users as $user)
                                                            <tr>
                                                                <th scope="row">{{ $user->id }}</th>
                                                                <td>
                                                                    <a href="{{ route('users.show', $user) }}">{{ $user->name }}</a>
                                                                </td>
                                                                <td>{{ $user->email }}</td>
                                                                <td >
                                                                    {!! Form::open(['route' => ['users.destroy', $user], 'method' => 'DELETE', 'class' => 'formulario-eliminar']) !!}

                                                                        <a class="btn btn-warning" href="{{ route('users.edit', $user) }}" title="Editar">
                                                                            <i class="far fa-edit"></i>
                                                                        </a>

                                                                        <button class="btn btn-danger" type="submit" title="Eliminar">
                                                                            <i class="far fa-trash-alt"></i>
                                                                        </button>

                                                                    {!! Form::close() !!}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-muted">
                    <a href="{{ route('roles.index') }}" class="btn btn-primary float-right">Regresar</a>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#roles').DataTable({
                responsive:true,
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar "+
                                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="-1">All</option>
                                        </select>` +
                                        " registros por página",
                    "zeroRecords": "No se encontró ningún registro.",
                    "info": "Mostrando la página  _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registro disponible.",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate":{
                        "next": "Siguiente",
                        "previous" : "Anterior"
                    }
                }
            });
        });
    </script>

    {{-- Recepcion de sessions para los estados de insertar actualizar y eliminar desplegando el sweeatalert --}}
    @if (session('guardar') == 'ok')
    <script>
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido guardado existosamente.',
                showConfirmButton: false,
                timer: 1500
                })
    </script>
    @endif
    @if (session('actualizar') == 'ok')
    <script>
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido actualizado existosamente.',
                showConfirmButton: false,
                timer: 1500
                })
    </script>
    @endif
    {{-- Rececpcion de status del metodo destroy para la confirmacion del alert de eliminado --}}
    @if (session('eliminar') == 'ok')
        <script>
            Swal.fire(
                    'Eliminado!',
                    'El registro ha sido eliminado correctamente.',
                    'success'
            )
        </script>
    @endif
    <script>
         $('.formulario-eliminar').submit(function(e){
            e.preventDefault();

           Swal.fire({
            title: 'Estas seguro?',
            text: "¡No podrás revertir esto!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, bórralo!'
            }).then((result) => {
            if (result.isConfirmed) {

                this.submit();
            }
            })

         });
    </script>
@stop
