@extends('adminlte::page')
@section('title','Editar rol')
@section('css')
@stop
@section('content_header')
@stop
@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edición de rol</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Configuración</a></li>
                    <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editar rol</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                <x-adminlte-alert class="text-uppercase" theme="danger" title="Atención!!" dismissable>
                    <ul>
                        @foreach ( $errors->all() as $error )
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </x-adminlte-alert>
                @endif

                @if (session('info'))
                    <x-adminlte-alert class="text-uppercase" theme="success" title="Exito!!" dismissable>
                        {{session('info')}}
                    </x-adminlte-alert>
                @endif
                <div class="card card-secondary">
                    <div class="card-header">
                        Editar rol: <i> {{$role->name}}</i>
                    </div>

                    {!! Form::model($role,['route'=>['roles.update',$role], 'method'=>'PUT']) !!}
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-keyboard menu-icon"></i></span>
                                        </div>
                                        <input type="text" name="name" id="name" value="{{$role->name}}" class="form-control" placeholder="" aria-describedby="helpId" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('admin.role._form')

                        <div class="clearfix"></div>

                    </div>
                    <div class="card-footer text-muted">
                        <button type="submit" class="btn btn-primary mr-2">Actualizar</button>
                        <a href="{{route('roles.index')}}" class="btn btn-light">Cancelar</a>
                    </div>

                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('js')
@stop


