<div class="form-row">

    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="modelo">Modelo</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="modelo" id="modelo" class="form-control" placeholder="Modelo del avión" aria-describedby="helpId">
            </div>
            <small class="form-text text-danger error-modelo"></small>     
        </div>
    </div>

    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="marca">Marca</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="marca" id="marca" class="form-control" placeholder="Marca del avión" aria-describedby="helpId">
            </div>
            <small class="form-text text-danger error-marca"></small>     
        </div>
    </div>
    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="matricula">Matricula</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="text" name="matricula" id="matricula" class="form-control" placeholder="Matricula del avión" aria-describedby="helpId">
            </div>
            <small class="form-text text-danger error-matricula"></small>     
        </div>
    </div>
    <div class="form-group col-md-6">
        <div class="form-group">
            <label for="capacidad">Capacidad</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-keyboard menu-icon"></i></span>
                </div>
                <input type="number" name="capacidad" id="capacidad" class="form-control" placeholder="Capacidad del avión" aria-describedby="helpId">
            </div>
            <small class="form-text text-danger error-capacidad"></small>     
        </div>
    </div>


</div>
<div class="clearfix"></div>
