@extends('adminlte::page')
@section('title', 'Aviones')
@section('css')
    <style>
        #avionetas tbody td:eq(0) {
            text-align: left;
        }
        #avionetas tbody td {
            text-align: center;
        }
    </style>



@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Aviones</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">
        <div class="card-header text-white bg-secondary" style="display: flex">
            <div class="col-sm-6">
                <h2>Listado de Aviones</h2>
            </div>
            <div class="col-sm-6 text-right">
                @can('avionetas.create')
                    <a class="btn btn-primary" href="javascript:void(0)" id="crearNuevaAvioneta">+ Agregar</a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table id="avionetas" class="table table-striped table-bordered table-hover data-table" style="width:100%">
                        <thead>
                            <tr class="table-active">
                                <th style="text-align: center;">Id</th>
                                <th style="text-align: center;">Modelo</th>
                                <th style="text-align: center;">Marca</th>
                                <th style="text-align: center;">Matricula</th>
                                <th style="text-align: center;">Capacidad</th>
                                <th style="text-align: center;">Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal de registro y edición-->
    <div class="modal fade" id="agregar_avioneta_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title col-11 text-center" id="modelHeading"></h4>  {{--titulo del modal --}}
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <form id="agregar_avioneta_formulario" name="agregar_avioneta_formulario">
                    <input type="hidden" name="avioneta_id" id="avioneta_id"> {{--id del registro oculto para la edicion --}}
                    <div class="modal-body">
                        @csrf
                        @include('admin.avioneta._form')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="btnGuardarAvioneta" value="create">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



@stop
@section('js')

<script type="text/javascript">

    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var table = $('.data-table').DataTable({
            responsive:true,
            autoWidth: false,
                "language": {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Cargando...</span> ',
                    "lengthMenu": "Mostrar "+
                                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="-1">All</option>
                                        </select>` +
                                        " registros por página",
                    "zeroRecords": "No se encontró ningún registro.",
                    "info": "Mostrando la página  _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registro disponible.",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate":{
                        "next": "Siguiente",
                        "previous" : "Anterior"
                    }
                },
            processing: true,
            serverSide: true,
            ajax: "{{ route('avionetas.index') }}",
            columns: [
                {data: 'id'    , name: 'id'},
                {data: 'modelo', name: 'modelo'},
                {data: 'marca', name: 'marca'},
                {data: 'matricula', name: 'matricula'},
                {data: 'capacidad', name: 'capacidad'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],

        });

        //Boton de crear nueva avioneta
        $('#crearNuevaAvioneta').click(function () {
            $('#btnGuardarAvioneta').val("create-avioneta");
            $('#avioneta_id').val('');
            $('#agregar_avioneta_formulario').trigger("reset");
            $('#modelHeading').html("Nuevo Avión").css( "font-weight","bold" );
            $('#agregar_avioneta_modal').modal('show');

            $('.error-matricula').html('');
            $('.error-marca').html('');
            $('.error-modelo').html('');
            $('.error-capacidad').html('');

        });

        // Boton de editar avioneta
        $('body').on('click', '.editarAvioneta', function () {
            var avioneta_id = $(this).data('id');
            $.get("{{ route('avionetas.index') }}" +'/' + avioneta_id +'/edit', function (data) {
                $('#modelHeading').html("Editar Avioneta").css( "font-weight","bold" );
                $('#btnGuardarAvioneta').val("edit-avioneta");
                $('#agregar_avioneta_modal').modal('show');

                $('#avioneta_id').val(data.id);
                $('#modelo').val(data.modelo);
                $('#marca').val(data.marca);
                $('#matricula').val(data.matricula);
                $('#capacidad').val(data.capacidad);

                $('.error-matricula').html('');
                $('.error-marca').html('');
                $('.error-modelo').html('');
                $('.error-capacidad').html('');

            })
        });

        //Boton de guardar persona
        $('#btnGuardarAvioneta').click(function (e) {
            e.preventDefault();
            $(this).html('Guardar');

            $.ajax({
                data: $('#agregar_avioneta_formulario').serialize(),
                url: "{{ route('avionetas.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#agregar_avioneta_formulario').trigger("reset");
                    $('#agregar_avioneta_modal').modal('hide');

                    var operacion = $('#btnGuardarAvioneta').val();

                    if( operacion == 'edit-avioneta'){
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'La Persona ha sido actualizada correctamente',
                            showConfirmButton: false,
                            timer: 1500,
                        });
                    }else{
                        Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: data.success,
                        showConfirmButton: false,
                        timer: 1500,
                    });

                    }
                    table.draw();
                },

                error: function (data) {
                    console.log('Error:', data);
                    console.log('MENSAJE', data.responseJSON.errors);
                    if(data.responseJSON.errors.modelo != null)
                    {
                        $('.error-modelo').html(data.responseJSON.errors.modelo[0]);
                    }
                    if(data.responseJSON.errors.matricula != null)
                    {
                        $('.error-matricula').html(data.responseJSON.errors.matricula[0]);
                    }
                    if(data.responseJSON.errors.marca != null)
                    {
                        $('.error-marca').html(data.responseJSON.errors.marca[0]);
                    }
                    if(data.responseJSON.errors.capacidad != null)
                    {
                        $('.error-capacidad').html(data.responseJSON.errors.capacidad[0]);
                    }
                }
            });
        });

        $('body').on('click', '.eliminarAvioneta', function () {
            var avioneta_id = $(this).data("id");
            Swal.fire({
                    title: 'Estas seguro?',
                    text: "¡No podrás revertir esta operación!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: '¡Sí, continuar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "DELETE",
                            url: "{{ route('avionetas.store') }}"+'/'+avioneta_id,
                            success: function (data) {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: data.success,
                                    showConfirmButton: false,
                                    timer: 1500,
                                }

                                );
                                table.draw();
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });


                    }
                });

            });

        });


</script>

@stop
