@extends('adminlte::page')
@section('title', 'Detalles del decolaje')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
    <style>
        #logo{
            height: 90px;
        }
        #border-div{
            border-left: 1px solid black;
            border-right: 1px solid black;
            border-bottom: 1px solid black;

        }

    </style>

@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">

        <div class="row mb-2">
            <div class="col-sm-12 float-right">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('decolajes.index') }}">Decolajes</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detalles de decolaje</li>
                </ol>
            </div>
        </div>
    </div>



<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body ">

                {{-- <a href="{{ route('decolajes.pdf', $decolaje) }}" class="btn btn-outline-danger btn-fw float-right"><i class="far fa-file-pdf"></i> Exportar</a> --}}

                <div style=" padding: 50px">

                    <div style="display: block;">
                        <div class="col-md-12 text-center" style="border: 1px solid black;">
                            <img src="/img_system/logo/logo-paracaidismo.jpg"  height="200px"><br>
                            <label class="form-control-label"><strong><h4>DECOLAJE</h4></strong></label>

                        </div>
                   
                    </div>
                    {{-- Fila de fecha de despegue y aterrizaje  --}}
                    <div style="display: flex;">
                        <div class="col-md-6" style="border: 1px solid black;" >
                            <label class="form-control-label">Fecha de despegue: </label>&nbsp;{{date('d/m/Y H:i:s', strtotime($decolaje->fecha_despegue)) }}
                        </div>
                        <div class="col-md-6" style="border: 1px solid black;" >
                            <label class="form-control-label">Fecha de aterrizaje: </label>&nbsp;{{date('d/m/Y H:i:s', strtotime($decolaje->fecha_aterrizaje)) }}
                        </div>
                    </div>
                    {{-- Fila de piloto y avion --}}
                    <div style="display: flex;">
                        <div class="col-md-6" style="border: 1px solid black;" >
                            <label class="form-control-label">Evento: </label>&nbsp;{{ $decolaje->evento->nombre  }}
                        </div>
                        <div class="col-md-6" style="border: 1px solid black;" >
                            <label class="form-control-label">Matricula/Avión: </label>&nbsp;{{ $decolaje->avioneta->modelo}} - {{ $decolaje->avioneta->matricula}}
                        </div>
                    </div>

                    {{-- detalle de pilotos --}}
                    <div class="col-md-12  p-3" style="border: 1px solid black;" >
                        <h4 style="text-align: center"><strong>Pilotos/s</strong></h4>
                        <div class="table-responsive col-md-12">
                            <table id="detallesPilotos" class="table table-hover table-sm table-bordered">
                                <thead class="thead-light">
                                    <tr>
                                        <th style="text-align: center">Id</th>
                                        <th style="text-align: center">Cédula</th>
                                        <th style="text-align: center">Pilotos</th>
                                        <th style="text-align: center">Licencia</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($decolajePilotos as $decolajePiloto)
                                        <tr>
                                            <td style="text-align: center">{{ number_format($decolajePiloto->id) }}</td>
                                            <td style="text-align: center">{{ $decolajePiloto->piloto->persona->cedula }}</td>
                                            <td style="text-align: center">{{ $decolajePiloto->piloto->persona->nombre }}</td>
                                            <td style="text-align: center">{{ $decolajePiloto->piloto->licencia }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
  
                    {{-- detalle de decolaje --}}
                    <div class="col-md-12  p-3" style="border: 1px solid black;" >
                        <h4 style="text-align: center"><strong>Paracaidista/s</strong></h4>
                        <div class="table-responsive col-md-12">
                            <table id="detalles" class="table table-hover table-sm table-bordered">
                                <thead class="thead-light">
                                    <tr>
                                        <th style="text-align: center">Id</th>
                                        <th style="text-align: center">Cédula</th>
                                        <th style="text-align: center">Paracaidistas</th>
                                        <th style="text-align: center">Licencia</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($decolajeDetalles as $decolajeDetalle)
                                        <tr>
                                            <td style="text-align: center">{{ number_format($decolajeDetalle->id) }}</td>
                                            <td style="text-align: center">{{ $decolajeDetalle->paracaidista->persona->cedula }}</td>
                                            <td style="text-align: center">{{ $decolajeDetalle->paracaidista->persona->nombre }}</td>
                                            <td style="text-align: center">{{ $decolajeDetalle->paracaidista->licencia }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
        
                </div>
            </div>{{-- card-body --}}

            <div class="card-footer text-muted">
                <a href="{{ route('decolajes.index') }}" class="btn btn-primary float-right">Regresar</a>
            </div>

        </div>
    </div>
</div>

    </div>

@stop
