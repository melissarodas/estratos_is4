<div class="form-row">

    <div class="form-group col-md-3">
        <label for="fecha_despegue">Fecha de despegue</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3"><i class="fa fa-calendar menu-icon"></i></span>
            </div>
            <input type="date" name="fecha_despegue" id="fecha_despegue" class="form-control"  value="<?php echo date("Y-m-d");?>" >
        </div>
    </div>
    <div class="form-group col-md-3">
        <label for="fecha_aterrizaje">Fecha de aterrizaje</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3"><i class="fa fa-calendar menu-icon"></i></span>
            </div>
            <input type="date" name="fecha_aterrizaje" id="fecha_aterrizaje" class="form-control"  value="<?php echo date("Y-m-d");?>" >
        </div>
    </div>
    <div class="form-group col-md-3">
        <div class="form-group">
            <label for="avioneta_id">Avión</label>
            <div class="input-group">
          
                <select class="js-example-basic-single js-states form-control js-example-responsive" name="avioneta_id" id="avioneta_id">
                    <option value="" disabled selected>Selecccione un Avión</option>
                    @foreach ($pilotos as $piloto)
                    <option value="{{$piloto->id}}">{{$piloto->licencia}}</option>
                    @endforeach
                </select>
            </div>
            @if ($errors->has('avioneta_id'))
                <small class="form-text text-danger">{{ $errors->first('avioneta_id') }}</small>
            @endif
        </div>
    </div>
    <div class="form-group col-md-3">
        <div class="form-group">
            <label for="evento_id">Evento</label>
            <div class="input-group">
                <select class="js-example-basic-single js-states form-control" name="evento_id" id="evento_id">
                    <option value="" disabled selected>Selecccione un Evento</option>
                    @foreach ($eventos as $evento)
                    <option value="{{$evento->id}}">{{$evento->nombre}}</option>
                    @endforeach
                </select>
            </div>
            @if ($errors->has('evento_id'))
                <small class="form-text text-danger">{{ $errors->first('evento_id') }}</small>
            @endif
        </div>
    </div>

</div>

<div class="clearfix"></div>


<div class="form-row" style="width:500px; float:left; background-color: rgba(195, 236, 231, 0.938)">

    <div class="form-group col-md-12">
        <div class="form-group">
            <h4 style="text-align: center"><strong>Piloto/s</strong></h4>
            <div class="input-group">
                <select class="js-example-basic-single js-states form-control js-example-responsive" name="piloto_id" id="piloto_id">
                    <option value="" disabled selected>Selecccione un piloto</option>
                    @foreach ($pilotos as $piloto)
                    <option value="{{$piloto->id}}">{{$piloto->licencia}}</option>
                    @endforeach
                </select><button type="button" id="agregarPiloto" class="btn btn-sm btn-primary">Agregar</button>
            </div>
            @if ($errors->has('piloto_id'))
                <small class="form-text text-danger">{{ $errors->first('piloto_id') }}</small>
            @endif
        </div>
    </div>
    <div class="form-group">
        <div class="table-responsive col-md-12">
            <table id="detallesPiloto" class="table table-striped">
                <thead>
                    <tr>
                        <th>Eliminar</th>
                        <th style="width: 700px">Pilotos</th>
                    </tr>
                </thead>
               
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>


<div class="form-row" style="width:500px; float:right; background-color:  rgb(219, 252, 210)">
    <div class="form-group col-md-12">
        <div class="form-group">
            <h4 style="text-align: center"><strong>Paracaidista/s</strong></h4>
            <div class="input-group">
                <select  class="js-example-basic-single js-states form-control" name="paracaidista_id" id="paracaidista_id">
                    <option value="" disabled selected>Selecccione un paracaidista</option>
                    @foreach ($paracaidistas as $paracaidista)
                        <option value="{{$paracaidista->id}}">{{$paracaidista->licencia}}</option>
                    @endforeach
                </select><button type="button" id="agregar" class="btn btn-sm btn-primary">Agregar</button>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="table-responsive col-md-12">
            <table id="detalles" class="table table-striped">
                <thead>
                    <tr>
                        <th>Eliminar</th>
                        <th style="width: 700px">Paracidista</th>
                    </tr>
                </thead>
            
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

