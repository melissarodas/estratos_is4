@extends('adminlte::page')
@section('title','Decolajes')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css">
@stop
@section('content_header')
@stop
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12 float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Decolajes</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <div class="card">

        <div class="card-header text-white bg-secondary" style="display: flex">

            <div class="col-sm-4">
                <h2>Listado de Decolajes</h2>
            </div>
            @can('decolajes.create')
                <div class="col-sm-8 text-right">
                    <a href="{{ route('decolajes.create') }}" class="btn btn-sm btn-primary float-right m-1"><i class="fas fa-plus"></i>&nbsp;REGISTRAR DECOLAJE</a>
                </div>
            @endcan

        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <table id="decolajes" class="table table-striped table-bordered dt-responsive" style="width:100%">
                        <thead>
                            <tr class="table-active">
                                <th style="text-align:center; vertical-align:middle;">#ID</th>
                                <th style="text-align:center; vertical-align:middle;">Fecha Despegue</th>
                                <th style="text-align:center; vertical-align:middle;">Fecha Aterrizaje</th>
                                {{-- <th style="text-align:center; vertical-align:middle;">Avión</th> --}}
                                {{-- <th style="text-align:center; vertical-align:middle;">Piloto</th> --}}
                                <th style="text-align:center; vertical-align:middle;">Estado</th>
                                <th style="text-align:center; vertical-align:middle; width:90px;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($decolajes as $decolaje )
                                <tr>
                                    <td style="text-align:center; vertical-align:middle;">{{$decolaje->id}}</td>
                                    <td style="text-align:center; vertical-align:middle;">{{date('d/m/Y', strtotime($decolaje->fecha_despegue))}} </td>
                                    <td style="text-align:center; vertical-align:middle;">{{date('d/m/Y', strtotime($decolaje->fecha_aterrizaje))}} </td>

                                    {{-- @foreach ($avionetas as $avioneta )
                                        @if ($decolaje->avioneta_id == $avioneta->id)
                                            <td style="text-align:center; vertical-align:middle;">{{$avioneta->matricula}}</td>
                                        @endif
                                    @endforeach

                                    @foreach ($pilotos as $piloto )
                                        @if ($decolaje->piloto_id == $piloto->id)
                                            <td style="text-align:center; vertical-align:middle;">{{$piloto->licencia}}</td>
                                        @endif
                                    @endforeach --}}

                                    @if ($decolaje->estado == 'APROBADO')
                                        <td style="text-align:center; vertical-align:middle;">
                                            <span class="badge badge-success">APROBADO</span>
                                        </td>
                                    @elseif($decolaje->estado == 'DESAPROBADO')
                                        <td style="text-align:center; vertical-align:middle;">
                                            <span class="badge badge-danger">DESAPROBADO</span>
                                        </td>
                                    @else
                                        <td style="text-align:center; vertical-align:middle;">
                                            <span class="badge badge-warning">PENDIENTE</span>
                                        </td>
                                    @endif


                                    <td style="text-align:center; vertical-align:middle;">
                                        @can('decolajes.show')
                                            <a href="{{route('decolajes.show',$decolaje)}}" class="btn btn-sm btn-info"  title="Ver detalles"><i class="far fa-eye"></i></a>
                                        @endcan
                                        @can('decolajes.aprobar')
                                            @if ($decolaje->estado == 'PENDIENTE')
                                                <a href="{{route('change.status.decolajes', $decolaje)}}" class="btn btn-sm btn-success"><i class="fas fa-check-circle" title="Aprobar" id='confirmar_decolaje'></i></a>
                                            @elseif($decolaje->estado == 'APROBADO')
                                                <a href="{{route('change.status.decolajes', $decolaje)}}" class="btn btn-sm btn-danger"><i class="fas fa-minus-circle" title="Desaprobar"></i></a>
                                            @elseif($decolaje->estado == 'DESAPROBADO')
                                            <a href="{{route('change.status.decolajes', $decolaje)}}" class="btn btn-sm btn-success"><i class="fas fa-check-circle" title="Desaprobar"></i></a>
                                            @endif
                                        @endcan

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#decolajes').DataTable({
                responsive:true,
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar "+
                                        `<select class="custom-select custom-select-sm form-control form-control-sm">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="-1">All</option>
                                        </select>` +
                                        " registros por página",
                    "zeroRecords": "No se encontró ningún registro.",
                    "info": "Mostrando la página  _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registro disponible.",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate":{
                        "next": "Siguiente",
                        "previous" : "Anterior"
                    }
                }
            });
        });
    </script>

    @if (session('guardar') == 'ok')
    <script>
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido guardado existosamente.',
                showConfirmButton: false,
                timer: 1500
                })
    </script>
    @endif
    {{-- @if (session('actualizar') == 'ok')
    <script>
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El registro ha sido actualizado existosamente.',
                showConfirmButton: false,
                timer: 1500
                })
    </script>
    @endif --}}
    {{-- @if (session('eliminar') == 'ok')
        <script>
            Swal.fire(
                    'Eliminado!',
                    'El registro ha sido eliminado correctamente.',
                    'success'
            )
        </script>
    @endif --}}
    @if (session('aprobado') == 'ok')
    <script>
        Swal.fire(
                'Decolaje aprobado!',
                'success'
        )
    </script>
    @elseif (session('desaprobado') == 'ok')
    <script>
        Swal.fire(
                'Decolaje Desaprobado!',
                'success'
        )
    </script>
    @endif

    @if (session('error') == 'ok')
    <script>
        Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'El registro no ha sido guardado. Verifique.',
                showConfirmButton: false,
                timer: 1500
                })
    </script>
    @endif

@stop
