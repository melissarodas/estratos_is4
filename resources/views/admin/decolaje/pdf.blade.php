<!DOCTYPE>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Reporte de venta</title>
<style>
    body {
        /*position: relative;*/
        /*width: 16cm;  */
        /*height: 29.7cm; */
        /*margin: 0 auto; */
        /*color: #555555;*/
        /*background: #FFFFFF; */
        font-family: Arial, sans-serif;
        font-size: 14px;
        /*font-family: SourceSansPro;*/
    }

    #contenedorPrincipal{
        border: 1px solid black;
        margin: 50px auto;
        display: inline-block;
        width: 100%
        height: 100%;
        padding: 15px;


        /* margin-top: 20px;mueve el cuadro haca abajo */
        /* padding:15px;
         */

    }

    #contenedorLogo{
        /* display: inline-block; */
        border: 1px solid black;
        height: 125px;
    }

    #logodiv{
        border: 1px solid black;
        display: inline-block;
        width:457px;
        margin-left: 3px;
        margin-top: 15px;
        height: 120px;
    }
    #timbradodiv{
        border: 1px solid black;
        display: inline-block;
        width:230px;
        margin-top: 15px;
        height: 120px;
    }

    img{
        height: 35px;
        display: block;
        margin-top: 1em;
        margin-left: 8em;
    }

    #clientes {
        width:100%;
        height: 86px;
        margin: 5px auto;
        text-align: justify;
        border: 0.1px solid rgb(0, 0, 0);
        display: inline-block;


    }

    #detallefactura {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;

    }


    #detallefactura thead {
        padding: 20px;
        background:rgb(219, 216, 216);
        text-align: center;
        border-bottom: 1px solid #FFFFFF;
        font-size: 12px;

    }




/*
    #encabezado {
        text-align: center;
        margin-left: 35%;
        margin-right: 35%;
        font-size: 15px;
    }

    #fact {
        float: right;
        margin-top: 2%;
        margin-left: 2%;
        margin-right: 2%;
        font-size: 20px;
        padding-bottom: 3%;
        padding-left: 3%;
        padding-right: 3%;
        border: 2px solid black
        text-align: center;

    }

    section {
        clear: left;
    }

    #cliente {
        text-align: left;
    }

    #facliente thead {
        padding: 20px;
        background: #33AFFF;
        text-align: left;
        border-bottom: 1px solid #FFFFFF;
    }

    #facventador {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
    }

    #facventador thead {
        padding: 20px;
        background: #33AFFF;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;
    }

    #cliente{
        margin-top: 90px;
        border: 1px solid black;

    } */



</style>

<body>


<div id="contenedorPrincipal">

    <div id="contenedorLogo">
        <div id="logodiv">
            <img src="{{ url('/img_system/logo/logo_innova.jpg') }}">
            <p style="text-align:center">
                <b>RUC: </b>{{ $empresa->ruc }} - {{ $empresa->nombre }}<br/>
                <b>DIRECCIÓN: </b>{{ $empresa->direccion }}<br/>
                <b>EMAIL: </b>{{ $empresa->mail }}
            </p>
        </div>
        <div id="timbradodiv">
            <p style="text-align:center">
                <strong>Timbrado N°: {{ $venta->timbrado }}</strong><br/>
                <strong>RUC N°: {{ $empresa->ruc }}</strong><br/>
                <strong style="font-size:20px;">FACTURA</strong><br/>
                <strong>{{ $venta->fiscal }}</strong>
            </p>
        </div>
    </div>{{-- end contenedorLogo --}}

    <div id="clientes">

        {{-- Fila 1 : Fecha:xx-xx-xxxx | Condicion de venta --}}
        <div class="d-flex flex-row">
            <div style="border: 1px solid black;  width:50%; float:left  padding-left: 3%; " >
                <label class="form-control-label">&nbsp;<b>Fecha:</b></label>&nbsp;{{$venta->created_at}}
            </div>
            <div style="border: 1px solid black; width:49.5%; float: right; text-align: center;" >
                <label class="form-control-label">&nbsp;<strong>Condición de venta</strong></label>
            </div>
        </div>

        <br>
        {{-- Fila 2 : RUC / CI :xxxxx-x |  Credito --}}
        <div class="d-flex flex-row">
            <div style="border: 1px solid black;  width:50%; float:left" >
                <label class="form-control-label">&nbsp;<strong>RUC /CI: </strong></label>&nbsp;{{ $venta->cliente->ruc }}
            </div>
            <div style="border: 1px solid black; width:49.5%; float: right; text-align: center;" >
                <label class="form-control-label">&nbsp;{{ $venta->condicion->nombre}}</label>
            </div>
        </div>

        <br>
        {{-- Fila 3 : Nombre o razon social: xxxx --}}
        <div class="d-flex flex-row">
            <div style="border: 1px solid black;  width:100%; float:left" >
                <label class="form-control-label">&nbsp;<strong>Nombre o Razón social: </strong></label>&nbsp;{{ $venta->cliente->nombre }}
            </div>
        </div>

        <br>
        {{-- Fila 4 : Direccion:xxxxx-x |  Telefono:xxx --}}

        <div class="d-flex flex-row">
            <div style="border: 1px solid black; width:50%; float: left;" >
                <label class="form-control-label">&nbsp;<strong>Dirección: </strong></label>&nbsp;{{ $venta->cliente->direccion }}
            </div>
            <div style="border: 1px solid black; width:49.5%; float: right;" >
                <label class="form-control-label">&nbsp;<strong>Teléfono:</strong></label>&nbsp;{{ $venta->cliente->telefono}}
            </div>
            <br>
        </div>
        {{-- Fila 5 : sucursal:xxxxx-x |  Vendedor:xxx --}}
        <div class="d-flex flex-row">
            <div style="border: 1px solid black;  width:50%; float:left" >
                <label class="form-control-label">&nbsp;<strong>Sucursal: </strong></label>&nbsp;{{$venta->sucursal->nombre}}
            </div>
            <div style="border: 1px solid black;  width:49.5%; float:right" >
                <label class="form-control-label">&nbsp;<strong>Cajero: </strong></label>&nbsp;{{$venta->user->name}}
            </div>
        </div>

    </div>{{-- end clientes --}}

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <div id="facturadetalle">
        <section>
            <div>
                <table id="detallefactura">
                    <thead>
                        <tr>
                            <th align="center" style="border: 1px solid black;">CANTIDAD</th>
                            <th align="center" style="border: 1px solid black;">DESCRIPCIÓN</th>
                            <th align="center" style="border: 1px solid black;">PRECIO UNITARIO(Gs.)</th>
                            <th align="center" style="border: 1px solid black;">EXENTAS (Gs.)</th>
                            <th align="center" style="border: 1px solid black;">5% (Gs.)</th>
                            <th align="center" style="border: 1px solid black;">10%(Gs.)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($ventaDetalles as $ventaDetalle)
                        <tr>
                            <td align="center" style="border: 1px solid black; font-size: 10px;">{{$ventaDetalle->cantidad}}</td>
                            <td align="center" style="border: 1px solid black; font-size: 10px;">{{$ventaDetalle->producto->nombre}}</td>
                            <td align="center" style="border: 1px solid black; font-size: 10px;">Gs. {{number_format($ventaDetalle->precio)}}</td>
                            <td align="center" style="border: 1px solid black; font-size: 10px;">Gs. 0</td>
                            <td align="center" style="border: 1px solid black; font-size: 10px;">Gs. 0</td>
                            <td align="center" style="border: 1px solid black; font-size: 10px;">Gs/ {{number_format($ventaDetalle->cantidad*$ventaDetalle->precio )}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="5" style="font-size: 12px;" >
                                <p align="right" style="margin-right: 5%">SUB TOTALES:</p>
                            </th>
                            <td>
                                <p align="right">{{number_format($total)}} Gs.<p>
                            </td>
                        </tr>

                        <tr>
                            <th colspan="5" style="font-size: 12px;">
                                <p align="right"style="margin-right: 5%">TOTAL IVA (10%):</p>
                            </th>
                            <td>
                                <p align="right"> {{number_format($total_general /11)}} Gs.</p>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="5" style="font-size: 12px;">
                                <p align="right" style="margin-right: 5%">TOTAL:</p>
                            </th>
                            <td>
                                <p align="right">{{number_format($total_general)}} Gs.<p>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </section>


    </div>





</div> {{-- end contenedorPrincipal --}}






</body>
</html>
