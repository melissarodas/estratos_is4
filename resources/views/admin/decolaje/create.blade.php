@extends('adminlte::page')
@section('title','Registro de decolaje')
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@stop
@section('content_header')
@stop
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('decolajes.index')}}">Decolaje</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Regristro</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Registro de Decolaje</h3>
                    </div>
                    {!! Form::open(['route'=>'decolajes.store', 'method'=>'POST', 'id' => 'decolajes-form']) !!}
                        <div class="card-body">
                            @include('admin.decolaje._form')
                        </div>
                        <div class="card-footer text-muted">
                            <button type="submit" id="guardar" class="btn btn-primary mr-2">Registrar</button>
                            <a href="{{route('decolajes.index')}}" class="btn btn-secondary">Cancelar</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>


@stop
@section('js')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>

    $(document).ready(function () 
    {
 
        $('.select2').select2();

        $("#agregar").click(function () {
            agregar();
        });

        $("#agregarPiloto").click(function () {
            agregarPiloto();
        });

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $( "#paracaidista_id" ).select2({
            ajax: { 
            url: "{{route('paracaidistas.getParacaidistas')}}",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                _token: CSRF_TOKEN,
                search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                results: response
                };
            },
            cache: true
            }
        });

        $( "#avioneta_id" ).select2({
            ajax: { 
            url: "{{route('avionetas.getAvionetas')}}",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                _token: CSRF_TOKEN,
                search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                results: response
                };
            },
            cache: true
            }
        });

        $( "#piloto_id" ).select2({
            ajax: { 
            url: "{{route('pilotos.getPilotos')}}",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                _token: CSRF_TOKEN,
                search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                results: response
                };
            },
            cache: true
            }
        });

        $( "#evento_id" ).select2({
            ajax: { 
            url: "{{route('eventos.getEventos')}}",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                _token: CSRF_TOKEN,
                search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                results: response
                };
            },
            cache: true
            }
        });


    });

    var cont = 1;
    total = 0;

    $("#guardar").hide();

    $("#paracaidista_id").change(mostrarValores);
    function mostrarValores() {
        datosParacaidista = document.getElementById('paracaidista_id').value;
    }

    function agregar() {
        datosParacaidista = document.getElementById('paracaidista_id').value;

        paracaidista_id = datosParacaidista[0];
        paracaidista = $("#paracaidista_id option:selected").text();

        if (paracaidista_id != "" ) {
            if (paracaidista_id != "" ) {
                total = total + 1;

                var fila = '<tr class="selected" id="fila' + cont + '"><td><button type="button" class="btn btn-danger btn-sm" onclick="eliminar(' + cont + ');"><i class="fa fa-times fa-2x"></i></button></td><td><input type="hidden" name="paracaidista_id[]" value="' + paracaidista_id + '" />' + paracaidista + '</td></tr>';
                cont++;
                limpiar();
                evaluar();
                $('#detalles').append(fila);
            } else {
                Swal.fire({
                    type: 'error',
                    text: 'unspecifc',
                })
            }
        } else {
            Swal.fire({
                type: 'error',
                text: 'Rellene todos los campos del detalle del decolaje.',
            })
        }
    }

    function limpiar() {
        $("#paracaidista_id").val("").trigger('change');
    }

    function evaluar() {
        if (total > 0) {
            $("#guardar").show();
        } else {
            $("#guardar").hide();
        }
    }

    function eliminar(index) {
        total = total - 1;

        $("#fila" + index).remove();
        evaluar();
    }
    // ---------------------------------------------
    var contPiloto = 1;
    totalPiloto = 0;
    $("#piloto_id").change(mostrarValoresPiloto);

    function mostrarValoresPiloto() {
        datosPiloto = document.getElementById('piloto_id').value;
    }

    function agregarPiloto() {
        datosPiloto = document.getElementById('piloto_id').value;

        piloto_id = datosPiloto[0];
        piloto = $("#piloto_id option:selected").text();

        if (piloto_id != "" ) {
            if (piloto_id != "" ) {
                totalPiloto = totalPiloto + 1;

                var fila = '<tr class="selected" id="fila' + contPiloto + '"><td><button type="button" class="btn btn-danger btn-sm" onclick="eliminar(' + contPiloto + ');"><i class="fa fa-times fa-2x"></i></button></td><td><input type="hidden" name="piloto_id[]" value="' + piloto_id + '" />' + piloto + '</td></tr>';
                contPiloto++;
                limpiar();
                evaluar();
                $('#detallesPiloto').append(fila);
            } else {
                Swal.fire({
                    type: 'error',
                    text: 'error unspecific',
                })
            }
        } else {
            Swal.fire({
                type: 'error',
                text: 'Rellene todos los campos del detalle del piloto.',
            })
        }
    }

    function limpiarPiloto() {
        $("#piloto_id").val("").trigger('change');
    }

    function eliminarPiloto(indexPiloto) {
        totalPiloto = totalPiloto - 1;

        $("#fila" + indexPiloto).remove();
    }

</script>

@stop

