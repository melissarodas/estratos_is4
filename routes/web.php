<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

Auth::routes();
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');

Route::get('/home', 'HomeController@index')->name('home');
//*************** ROLES Y PERMISOS ********************//
Route::resource('users', 'UserController')->names('users');
Route::resource('roles', 'RoleController')->names('roles');
Route::resource('permissions', 'PermissionController')->names('permissions');

Route::resource('personas', 'PersonaController')->names('personas');

Route::resource('pilotos', 'PilotoController')->names('pilotos');
Route::post('/pilotos/getPilotos/','PilotoController@getPilotos')->name('pilotos.getPilotos');

Route::resource('paracaidistas', 'ParacaidistaController')->names('paracaidistas');
Route::post('/paracaidistas/getParacaidistas/','ParacaidistaController@getParacaidistas')->name('paracaidistas.getParacaidistas');

Route::resource('zonas', 'ZonaController')->names('zonas');

Route::resource('eventos', 'EventoController')->names('eventos');
Route::post('/eventos/getEventos/','EventoController@getEventos')->name('eventos.getEventos');

Route::resource('avionetas', 'AvionetaController')->names('avionetas');
Route::post('/avionetas/getAvionetas/','AvionetaController@getAvionetas')->name('avionetas.getAvionetas');

Route::resource('escuelas', 'EscuelaController')->names('escuelas');

Route::resource('suspensionParacaidistas', 'SuspensionParacaidistaController')->names('suspensionParacaidistas');
Route::resource('pruebas', 'TitulacionTipoController')->names('pruebas');
Route::resource('propietarios', 'PropietariosEscuelaController')->names('propietarios');
Route::resource('climaticas', 'CondicionesClimaticaController')->names('climaticas');

//************************************* DECOLAJES *****************************************//

Route::resource('decolajes', 'DecolajeController')->names('decolajes');
Route::get('decolajes/change_status/{decolaje}', 'DecolajeController@change_status')->name('change.status.decolajes');
Route::get('decolajes/pdf/{decolaje}', 'DecolajeController@pdf')->name('decolajes.pdf');

Route::get('/clima', function(){
    $location = '3439137';
    $apiKey = '9d38cd5e976eccd05e223a2ce51ef70f';

    $response = Http::get("https://api.openweathermap.org/data/2.5/weather?id={$location}&appid={$apiKey}");
    dd($response->json());

    return view('welcome', ['currentWeather' => $response->json()]);
});




// Route::resource('avionetas', 'AvionetaController')->names('avionetas');

