<?php

namespace Tests\Unit;

use App\Zona;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Collection;

class ZonaTest extends TestCase
{
    public function test_zona_has_many_eventos()
    {
        $zona = new Zona;
        $this->assertTrue(Collection::class, $zona->eventos);
    }
}
